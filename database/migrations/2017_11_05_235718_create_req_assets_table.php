<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReqAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asset_name');
            $table->boolean('status');
            $table->integer('initiative_id')->unsigned();
            $table->foreign('initiative_id')->references('id')->on('initiatives');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('req_assets');
    }
}
