<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapPrivatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_preferences_private', function (Blueprint $table) {
            $table->increments('id');
            $table->float('latitude', 13, 10);
            $table->float('longitude', 13, 10);
            $table->integer('zoom');
            $table->string('map_style');
            $table->uuid('map_id');
            $table->foreign('map_id')->references('id')->on('maps')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_preferences_private');
    }
}
