<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiatives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('initiative_name');
            $table->string('email');
            $table->string('initiative_title');
            $table->mediumText('initiative_resume');
            $table->mediumText('initiative_motivation');
            $table->mediumText('initiative_description');
            $table->date('start_date');
            $table->date('finish_date');
            $table->string('url_web')->nullable();
            $table->string('url_fb')->nullable();
            $table->string('url_tw')->nullable();
            $table->string('url_yt')->nullable();
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('themes');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initiatives');
    }
}
