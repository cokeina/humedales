<?php

use Illuminate\Database\Seeder;
use App\Content;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Content::create([
            'id' => 1,
            'name'        => 'start_image_right',
            'description' => 'Imagen principal inicio derecha',
            'content'     => '1.png',
            'type'        => 0
        ]);
        
        Content::create([
            'id' => 2,
            'name'        => 'start_secundary_image_1',
            'description' => 'Imagen secundaria pantalla inicio (izquierda)',
            'content'     => '2.JPG',
            'type'        => 0
        ]);

        Content::create([
            'id' => 3,
            'name'        => 'start_secundary_image_2',
            'description' => 'Imagen secundaria pantalla inicio (derecha)',
            'content'     => '3.JPG',
            'type'        => 0
        ]);

        Content::create([
            'id' => 4,
            'name'        => 'start_secundary_text_1',
            'description' => 'Texto imagen secundaria (izquierda)',
            'content'     => '<p>La plataforma permite visibilizar a los humedales como parte de una
            infraestructura verde urbana, destacando su relevancia en la gestión del agua, la planificación
            urbana, el espacio público y el control del cambio climático en nuestras ciudades.</p>',
            'type'        => 1
        ]);

        Content::create([
            'id' => 5,
            'name'        => 'start_secundary_text_2',
            'description' => 'Texto imagen secundaria (derecha)',
            'content'     => '<p>Además de generar datos locales abiertos sobre los humedales y su vínculo con la
            ciudad, el proceso de co-construcción del mapa busca poner en marcha acciones colectivas
            para la puesta en valor de la naturaleza urbana.</p>',
            'type'        => 1
        ]);

        Content::create([
            'id' => 6,
            'name'        => 'what_is_text_1',
            'description' => 'Texto principal ¿Que es? ',
            'content'     => 'El Mapa Interactivo de Humedales Urbanos es una plataforma web orientada a la difusión y
            construcción permanente de conocimiento colectivo en torno a los humedales y la naturaleza
            urbana en general, y su relación con otros elementos de la ciudad. El Mapa permite visualizar
            y generar datos abiertos, mediante el uso de distintas categorías y temas de interés. Por otra
            parte, la plataforma incluye una sección de Iniciativas, que busca ser un espacio para
            visibilizar, gestionar y financiar proyectos relacionados con la puesta en valor de naturaleza
            urbana, la solución de problemas socio-ambientales, y la co-construcción de ciudades más
            sustentables',
            'type'        => 1
        ]);

        Content::create([
            'id' => 7,
            'name'        => 'what_is_text_2',
            'description' => 'Texto secundario ¿Que es? ',
            'content'     => '<h4>¿Por qué humedales?</h4>
            <p>Los humedales son áreas verdes y centros de biodiversidad urbana, que limpian las
            aguas, previenen inundaciones, y ayudan a controlar el cambio climático. Conocerlos
            es el primer paso para valorarlos y protegerlos.</p>',
            'type'        => 1
        ]);

        Content::create([
            'id' => 8,
            'name'        => 'what_is_text_3',
            'description' => 'Texto creative commons ',
            'content'     => '<p>El Mapa Interactivo de Humedales Urbanos está registrado bajo una  licencia de Creative Commons, que permite adaptaciones para fines no comerciales, siempre que éstas se compartan de la misma manera (<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">Attribution-NonCommercial-ShareAlike 4.0 International</a>).</p>
                ',
            'type'        => 1
        ]);

        Content::create([
            'id' => 9,
            'name'        => 'what_is_text_4',
            'description' => 'Texto desarrollo abierto ',
            'content'     => '<h4>Desarrollo Abierto</h4>
            <p>Esta plataforma ha sido programada en código abierto. Desde este espíritu de apertura y colaboración, buscamos interactuar con una comunidad de desarrolladores que contribuya al mejoramiento  de este espacio y pueda dar origen a nuevos desarrollos.</p>
            ',
            'type'        => 1
        ]);

        Content::create([
            'id' => 10,
            'name'        => 'what_is_image_1',
            'description' => 'Imagen derecha ¿Que es?',
            'content'     => '10.png',
            'type'        => 0
        ]);

    }
}
