<?php

use Illuminate\Database\Seeder;
use App\Map;
use App\Map_theme;

class MapThemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $map = Map::where('name', 'Public map')->first();

        for ($i=1; $i <= 5 ; $i++) { 
        	Map_theme::create([
        		'map_id' => $map->id,
        		'theme_id' => $i
        	]);
        }
    }
}
