<?php

use Illuminate\Database\Seeder;
use App\Theme;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        	'Humedales y espacios del agua',
        	'Espacio público',
        	'Biodiversidad',
        	'Movilidad y accesibilidad',
        	'Acción colectiva',
        	'Infraestructura verde',
        	'Patrimonio natural y cultural',
        	'Servicios y organizaciones sustentables',
        	'Renovación urbana y ambiental',
        	'Cambio climático'
        ];

        foreach(range(0, count($names) - 1) as $index) {
        	Theme::create([
        		'id' 			=> $index+1,
        		'themes_name' => $names[$index]
        	]);
        }
    }
}
