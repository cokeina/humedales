<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(MapsTableSeeder::class);
        $this->call(MapCatTableSeeder::class);
        $this->call(MapThemeTableSeeder::class);
    }
}
