<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        	'Naturaleza urbana',
        	'Iniciativas y buenas prácticas',
        	'Comunidad',
        	'Problemáticas',
        	'Historia',
        	'Herramientas y datos abiertos',
        	'Proyectos regionales',
        	'Regulación',
        	'Comunicación y difusión',
        	'Educación para la sustentabilidad'
        ];

        foreach(range(0, count($names) - 1) as $index) {
        	Category::create([
        		'id' 			=> $index+1,
        		'category_name' => $names[$index]
        	]);
        }
    }
}
