<?php

use Illuminate\Database\Seeder;
use App\Map;
use App\User;

class MapsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        	'Public map'
        ];

        $user = User::where('email', 'admin@gmail.com')->first();

        foreach(range(0, count($names) - 1) as $index) {
        	Map::create([
        		'id' 			=> $index+1,
        		'name' => $names[$index],
        		'user_id' => $user->id
        	]);
        }
    }
}
