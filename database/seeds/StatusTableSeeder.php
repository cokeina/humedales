<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        	'Incompleta - paso 1',
            'Incompleta - paso 2',
            'Pendiente',
        	'Rechazada',
        	'Publicada',
        	'Terminada'
        ];

        foreach(range(0, count($names) - 1) as $index) {
        	Status::create([
        		'id' 			=> $index+1,
        		'status_name' => $names[$index]
        	]);
        }
    }
}
