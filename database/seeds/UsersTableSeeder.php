<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	User::create([
        		'id' => '3f2b4c00-9f2e-11e7-a944-1b02795e34ea',
        		'name' => 'Jorge Almonacid',
        		'email' => 'admin@gmail.com',
        		'password' => bcrypt('mateito'),
        		'roles_id' => 1
        	]);

            User::create([
                'id' => 'aa70bcb0-a3f2-11e7-b759-bfc0a327c821',
                'name' => 'mapahumedalesurbanos',
                'email' => 'mapahumedalesurbanos@gmail.com',
                'password' => bcrypt('humedales2017'),
                'roles_id' => 2
            ]);

            User::create([
                'id' => 'a9e08b30-aa90-11e7-8178-138b53c1f91e',
                'name' => 'general',
                'email' => 'general@gmail.com',
                'password' => bcrypt('mateito'),
                'roles_id' => 3
            ]);
    }
}
