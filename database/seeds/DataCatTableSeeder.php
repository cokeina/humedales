<?php

use Illuminate\Database\Seeder;
use App\Data;
use App\Data_cat;

class DataCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Data::all();

        foreach ($data as $var) {
            Data_cat::create([
                'data_id'     => $var->id,
                'category_id' => rand(1,5)
            ]);
        }
    }
}
