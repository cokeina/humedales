<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        	'admin',
        	'curador',
        	'general'
        ];

        foreach(range(0, count($names) - 1) as $index) {
        	Rol::create([
        		'id' 			=> $index+1,
        		'rol_name' => $names[$index]
        	]);
        }
    }
}
