<?php

use Illuminate\Database\Seeder;
use App\Map;
use App\Map_cat;

class MapCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $map = Map::where('name', 'Public map')->first();

        for ($i=1; $i <= 5 ; $i++) { 
        	Map_cat::create([
        		'map_id' => $map->id,
        		'category_id' => $i
        	]);
        }
    }
}
