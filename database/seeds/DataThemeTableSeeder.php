<?php

use Illuminate\Database\Seeder;
use App\Data;
use App\Data_theme;


class DataThemeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Data::all();

        foreach ($data as $var) {
            Data_theme::create([
                'data_id'     => $var->id,
                'theme_id' => rand(1,5)
            ]);
        }
    }
}
