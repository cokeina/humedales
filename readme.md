Proyecto desarrollado con :
* Laravel 5.4.*
* Javascript / Jquery
* OpenLayers (para los mapas)
* Bootstrap 4


Instalación :
* Previamente copiar .env.example a .env y dar permisos necesarios para acceder a la BD
* composer install
* php partisan key: generate
* php artisan migrate
* php artisan db:seed

