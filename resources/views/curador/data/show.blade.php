@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
@endsection

@section('title', 'Editar usuario | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container admin">
			
			@include('shared.navbar-curador')	

			<div class="row profile justify-content-md-center">
				<div class="col-8 offset-2">

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif
					@if (session('error'))
		                <div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('error') }}
		                </div>
		            @endif
						
                    <h3 style="margin-bottom:1em">Mostrar dato</h3>
                    <form method="POST">

						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" id="latitude" name="latitude" value="">
						<input type="hidden" id="longitude" name="longitude" value="">
						<input type="hidden" id="status" name="status" value="">

	                    <div class="form-group row">
	                        <label for="name" class="col-2">Titulo: </label>

	                        <div class="col-10"><input readonly type="text" class="form-control" placeholder="{{ $data->title }}" value="{{ $data->title }}"" name="title" "></div>
	                    </div>

	                    <div class="form-group row">
	                        <label for="email" class="col-2">Descripción: </label>
	                        <div class="col-10" ><textarea readonly class="form-control" placeholder="{{ $data->description }}" name="description" value="{{ $data->description }}"></textarea></div>
	                    </div>

						<div class="form-group row">
	                        <label for="email" class="col-2">Categoría: </label>
							<div class="col-10">
								<select class="form-control" readonly name="category" id="catEdit" >
								@foreach ($cat as $c)
									<option value="{{ $c->id }}">{{ $c->category_name }}</option>
								@endforeach
								</select>
							</div>
	                    </div>

						<div class="form-group row">
	                        <label for="email" class="col-2">Tema: </label>
							<div class="col-10">
								<select class="form-control" readonly name="theme" id="themeEdit">
								@foreach ($themes as $theme)
									<option value="{{ $theme->id }}">{{ $theme->themes_name }}</option>
								@endforeach
								</select>
							</div>
	                    </div>

						<div class="form-group row">
						<table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Archivo</th>
								</tr>
							</thead>
							
						</table>
						</div>


						<div id="map" class="map" style="height:500px"></div>

	                    <div class="form-group" style="margin-top:10px">
						<div class="">
						    <button type="button" id="goBack" class="btn btn-primary">Volver</button>
						</div>
					</form>
				</div>
			</div>

		</div>
@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>
<script src="{{ asset('js/modal.js') }}"></script>
<script src="{{ URL::asset('js/mapedit.js')}}"></script>
<script>
    
	var elatitude  = 0;
	var elongitude = 0;

	var table      = '';


	
	function check(asdf) {
    	var id = $(asdf).attr('id');
    	console.log(id);
    	if (id == 'status-0') {
    		$('#status-1').attr('checked', false);
    	} else {
    		$('#status-0').attr('checked', false);
    	}
	}



	$(document).ready(function() {  

		mymap = MapMaker();
	  
	  elongitude  = parseFloat('{{ $point->longitude }}');
	  elatitude   = parseFloat('{{ $point->latitude }}');

	  let category = '{{ $data->category_id }}';
	  let theme    = '{{ $data->theme_id }}';

	  $("#latitude").val(elatitude);
	  $("#longitude").val(elongitude);
	  $("#catEdit").val(category);
	  $("#themeEdit").val(theme);

	  mymap.createOSMap(elongitude,elatitude, 10);

	  mymap.addMarker(elongitude, elatitude,'{{ $data->category_id }}','{{ $data->theme_id }}','{{ $data->id }}','x','y',[]);





		$("#goBack").click(function(){
			window.location.href = '{{ url('curador/data')}}';
		  });
		  
		  table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
				"processing": true,
				"fixedColumns": true,
				"scrollX":        true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {

					data.data_id     = '{{ $data->id }}';

					$.get('{{ url('/data/images' ) }}?' + $.param(data), function(response){
						callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.images
                        });
					});

                },
                "paging": true,
                "columns": [
					{ "data": "id", "visible": false,},
					{ "data": "filename"}
                ]
		});


	});



</script>

@endsection