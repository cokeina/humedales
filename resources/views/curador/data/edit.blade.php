@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
@endsection

@section('title', 'Editar usuario | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container admin">
			
			@include('shared.navbar-curador')	

			<div class="row profile justify-content-md-center">
				<div class="col-8 offset-2">

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif
					@if (session('error'))
		                <div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('error') }}
		                </div>
		            @endif
						
                    <h3 style="margin-bottom:1em">Editar dato</h3>
                    <form method="POST">

						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" id="latitude" name="latitude" value="">
						<input type="hidden" id="longitude" name="longitude" value="">
						<input type="hidden" id="status" name="status" value="">

	                    <div class="form-group row">
	                        <label for="name" class="col-2">Titulo: </label>

	                        <div class="col-10"><input type="text" class="form-control" placeholder="{{ $data->title }}" value="{{ $data->title }}"" name="title" "></div>
	                    </div>

	                    <div class="form-group row">
	                        <label for="email" class="col-2">Descripción: </label>
	                        <div class="col-10" ><textarea class="form-control"  name="description" value="{{ $data->description }}">{{ $data->description }}</textarea></div>
	                    </div>

						<div class="form-group row">
	                        <label for="email" class="col-2">Categoría: </label>
							<div class="col-10">
								<select class="form-control" name="category" id="catEdit" >
								@foreach ($cat as $c)
									<option value="{{ $c->id }}">{{ $c->category_name }}</option>
								@endforeach
								</select>
							</div>
	                    </div>

						<div class="form-group row">
	                        <label for="email" class="col-2">Tema: </label>
							<div class="col-10">
								<select class="form-control" name="theme" id="themeEdit">
								@foreach ($themes as $theme)
									<option value="{{ $theme->id }}">{{ $theme->themes_name }}</option>
								@endforeach
								</select>
							</div>
	                    </div>

						<div class="form-group row">
						<button class="btn btn-primary" style="margin-left:15px" type="button" id="addFile" >Agregar Fotografía</button>
						<table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Archivo</th>
									<th>Acciones</th>
								</tr>
							</thead>
							
						</table>
						</div>

						<!--<div class="form-group row">
	                        <label for="name" class="col-2">Estado: </label>
                            <div class="col-10">
								<select name="status" class="form-control">
									<option value="0" @if($data->status == 0) selected @endif >Ingresado</option>
									<option value="2" @if($data->status == 2) selected @endif>En Revisión</option>
									<option value="1" @if($data->status == 1) selected @endif>Publicada</option>
								</select>
							</div>
	                    </div>-->

						<div id="map" class="map" style="height:500px"></div>

	                    <div class="form-group" style="margin-top:10px">
						<div class="">
						    <button type="button" id="goBack" class="btn btn-primary">Volver</button>

							@if($data->status == 2)
							<a href="{!! action('Curador\CuradorController@approveData',$data->id) !!}"><button type="button" class="btn btn-danger pull-right">Aprobar</button></a>
							@endif
							<!--<button type="submit" class="btn btn-primary pull-right" onclick="sendStatus(1)" style="margin-right:10px">Guardar y Publicar</button>-->
							<button type="submit" class="btn btn-success pull-right" onclick="sendStatus(3)" style="margin-right:10px">Guardar</button>
						</div>
					</form>
				</div>
			</div>

		</div>
@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>
<script src="{{ asset('js/modal.js') }}"></script>
<script src="{{ URL::asset('js/mapedit.js')}}"></script>
<script>
    
	var elatitude  = 0;
	var elongitude = 0;

	var table      = '';

	var rows = [
            [
                {field: 'Archivo', type: 'file', id: 's_file', value: 'file'}
            ]
        ];

    var params = {
        title: 'Agregar archivo',
        rows: rows
	}
	
	var group_id = "modal_group";

    HModal.create(group_id, params);
	
	function check(asdf) {
    	var id = $(asdf).attr('id');
    	console.log(id);
    	if (id == 'status-0') {
    		$('#status-1').attr('checked', false);
    	} else {
    		$('#status-0').attr('checked', false);
    	}
	}
	
	function sendStatus(status)
	{
		$("#status").val(status);
	}

	function del(id) {

            swal({
                    title: "Eliminar Archivo",
                    text: "¿Esta seguro de realizar esta acción?'",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "SI",
                    cancelButtonText: "NO"
            }).then(function () {
                    
                var url  = '{{ url('/data/image/delete')  }}';
                url += '/'+id; 

                $.ajax({
                    url:url,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){ 

                        switch(response.code)
                        {
                            case 200:
                                table.ajax.reload();
                            break;

                            case 403:
                                swal('Error',response.data.message,'error');   
                            break;
                            
                            default :
                            swal('Error','Ha ocurrido un error en la operación','error');   
                            break;

                        }
                        
                    }
                });   
                    
                    /*HMApi.delete('{{  url('/files')  }}/'+id,function(data) {
                        
                        

                        
                    });*/
                });
            
    }


	$(document).ready(function() {  

		mymap = MapMaker();
	  
	  elongitude  = parseFloat('{{ $point->longitude }}');
	  elatitude   = parseFloat('{{ $point->latitude }}');

	  let category = '{{ $data->category_id }}';
	  let theme    = '{{ $data->theme_id }}';

	  $("#latitude").val(elatitude);
	  $("#longitude").val(elongitude);
	  $("#catEdit").val(category);
	  $("#themeEdit").val(theme);

	  mymap.createOSMap(elongitude,elatitude, 10);

	  mymap.addMarker(elongitude, elatitude,'{{ $data->category_id }}','{{ $data->theme_id }}','{{ $data->id }}','x','y',[]);

	  $("#catEdit").change(function(){

			mymap.deleteMarkerById('{{ $data->id }}');
			mymap.addMarker($("#longitude").val(), $("#latitude").val(),$("#catEdit").val(),$("#themeEdit").val(),'{{ $data->id }}','x','y',[]);
			//console.log("probando estooo :D"+this.value);
	  });

	  $("#themeEdit").change(function(){

			mymap.deleteMarkerById('{{ $data->id }}');
			mymap.addMarker($("#longitude").val(), $("#latitude").val(),$("#catEdit").val(),$("#themeEdit").val(),'{{ $data->id }}','x','y',[]);
			//console.log("probando estooo :D"+this.value);
	  });

		$("#goBack").click(function(){
			window.location.href = '{{ url('curador/data')}}';
		  });
		  
		  table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
				"processing": true,
				"fixedColumns": true,
				"scrollX":        true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {

					data.data_id     = '{{ $data->id }}';

					$.get('{{ url('/data/images' ) }}?' + $.param(data), function(response){
						callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.images
                        });
					});

                },
                "paging": true,
                "columns": [
					{ "data": "id", "visible": false,},
					{ "data": "filename"},
					{ "data": "id",
                        class:"text-center",
                        render:function(data,type,full,meta)
                        {
                            var del      = "<button  type='button' class='btn btn-danger btn-xs' onclick=del("+data+");><i class='fa fa-xs fa-trash fa-fw' ></i></button";

                            return  "<div class='btn-group'>"+del+"</div>";
                        }
                    }
                ]
		});
		
		$('#addFile').click(function () {
                
                $("#"+group_id+" div.form-group").removeClass("has-error");
                $(".modal-body input").val("");
                $('#' +group_id).modal('show');
		});
		
		$('#' + group_id + "_create").click(function(){

                var frm = new FormData();

                var file =  $('#s_file')[0].files[0] ;
                if (file == undefined) file = '';
                
				frm.append('file',file);
				frm.append('data_id','{{ $data->id }}');

                $.ajax({
                    url:'{{ url('/data/image/upload') }}',
                    data:frm,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){
                       


                         if(response.code == 200)
                         {
                            swal("Agregar archivo", "Información ingresada de forma exitosa", "success");
                            table.ajax.reload();
                            $('#' +group_id).modal('toggle');
                         }

                         if(response.code == 201)
                         {
                            errors = [];
                            
                            for (x in response.data.errors)
                            {
                                errors.push('<li>'+response.data.errors[x].join(",")+'</li>');
                            }

                            //console.log(errors);

                            swal("Error",errors.join(" "), "error");
                            


                         }

                    },
                    failure:function(response)
                    {

                    }
                });
        });

	});



</script>

@endsection