@extends('base')

@section('title', 'Usuarios | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-curador')	

			<div class="row profile">
				<div class="col-12 admin-users">
					<h3>Usuarios</h3>

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif

		            @if($invite != 0)

					<table class="table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Email</th>
								<th>Fecha creación</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
								<tr>
									<td>{!! $user->name !!}</td>
									<td>{!! $user->email !!}</td>
									<td>{!! $user->created_at->format('d-m-Y') !!}</td>
									<td style="text-align: center">
										<a href="{!! action('Curador\CuradorController@editUser', $user->id) !!}" data-toggle="tooltip" title="Editar usuario"><i class="fa fa-pencil-square" style="color:green" aria-hidden="true"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>

					@else
					<p>No tienes el permiso necesario</p>
					@endif
				</div>
			</div>

		</div>

@endsection