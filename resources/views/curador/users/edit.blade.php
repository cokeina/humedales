@extends('base')

@section('title', 'Editar usuario | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container admin">
			
			@include('shared.navbar-curador')	

			<div class="row profile justify-content-md-center">
				<div class="col-8 offset-2">

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif
						
                    <h3 style="margin-bottom:1em">Editar usuario</h3>
                    <form method="POST">

						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

	                    <div class="form-group row">
	                        <label for="name" class="col-2 col-form-label">Nombre: </label>

	                        <div class="col-10"><input type="text" class="form-control" value="{{ $user->name }}" disabled></div>
	                    </div>

	                    <div class="form-group row">
	                        <label for="email" class="col-2 col-form-label">Email: </label>
	                        <div class="col-10"><input class="form-control" value="{{ $user->email }}" disabled></div>
	                    </div>

						<h4 style="margin-bottom:1em; margin-top:1em">Asignar permisos sobre mapas</h4>


						@if ((count($maps) == 0))
							<p style="font-size: .8em;">No se han creado mapas aun</p>
						@else
						<table class="table">
							<thead>
								<tr>
									<th>Mapa</th>
									<th>Ver</th>
								</tr>
							</thead>
							<tbody>
								@foreach($maps as $map)
									<tr>
										<td>{!! $map->name !!}</td>
										<td style="text-align: center">
											<input class="form-check-input" name="see[]" type="checkbox" value="{!! $map->id !!}$1"
											@if ($map->see != null)
												checked
											@endif
											>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						@endif

	                    <div class="form-group">
	                        <div class="col-lg-10 col-lg-offset-2">
	                            <button type="submit" class="btn btn-primary">Guardar</button>
	                        </div>
	                    </div>
					</form>
				</div>
			</div>

		</div>

@endsection