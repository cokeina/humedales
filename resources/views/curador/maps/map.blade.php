@extends('base')
@section('title', 'Mapa interactivo de humedales urbanos')


@section('content')
	
  <div class="row no-gutters">
    <div class="col-10" id="map-container">
      <div id="map" class="map"><div id="popup"></div></div>

      <div class="typeahead__container" style="width:250px!important;position: absolute; top: 10px; right: 10px; padding: 3px;background:rgba(255,255,255,0.4);border-radius:5px;">
          <div class="typeahead__field">

          <span class="typeahead__query">
              <input class="js-typeahead-input"
                         name="q"
                         type="search"
                         autocomplete="off"/>
          </span>
              <span class="typeahead__button">
              <button onclick="search()">
                  <span class="typeahead__search-icon"></span>
              </button>
          </span>

          </div>
      </div>

      <div class="geolocation__container" style="position: absolute; left: 10px; bottom: 10px;background:rgba(255,255,255,0.4);padding: 3px;">
        <button onclick="setUserPosition()" style="padding: 6px;background:rgba(0,60,136,.5); border:none; border-radius: 2px;">
            <i class="fa fa-crosshairs" aria-hidden="true" style="color:white"></i>
        </button>
      </div>


    </div>
    <div class="col-2" id="cat-themes">
      
      <p class="title">{!! $map->name !!}</p>

      <button class="btn" type="button" data-toggle="modal" onclick="setAddData()">Agregar dato</button>

      <p class="title">Categorías</p>
      @foreach ($categories as $category)
        <div class="checkbox">
          <input type="checkbox" checked id="{{ str_replace(' ','_',$category->category_name) }}" onclick="changeMarker('{{ str_replace(' ','_',$category->category_name) }}', 'category', {{ $category->id }})">
            <img src="{{asset('images/icons-map/category/'.$category->id.'.png')}}">
            <label for="checkboxes-0">{{ $category->category_name }}</label>
        </div>
      @endforeach


      <div class="divider"></div>
  
      <p class="title">Temas</p>

      @foreach ($themes as $theme)
        <div class="checkbox" id="theme">
          <input type="checkbox" id="{{ str_replace(' ','_',$theme->themes_name) }}" onclick="changeMarker('{{ str_replace(' ','_',$theme->themes_name) }}','theme', {{ $theme->id }})" checked>
          <img src="{{asset('images/icons-map/theme/'.$theme->id.'.png')}}">
          <label for="checkboxes-0">{{ $theme->themes_name }}</label>
        </div>
      @endforeach
    </div>
  </div>


  <script src="{{ URL::asset('js/map.js')}}"></script>
  <script>
    $(document).ready(function() {  
      mymap = MapMaker();
      var latitude  = parseFloat({!! json_encode($map_preferences->latitude) !!});
      var longitude = parseFloat({!! json_encode($map_preferences->longitude) !!});
      var zoom      = parseInt({!! json_encode($map_preferences->zoom) !!});
      var style     = {!! json_encode($map_preferences->map_style) !!};

      mymap.createOSMap(longitude, latitude, zoom, style);

      var getUrl = window.location;
      var url = getUrl.protocol + "//" + getUrl.host  + getUrl.pathname + "/data";
      $.get(url, function (data){
        for(var i = 0; i < data.length; i++) {
          if (data[i].images[0] == null) {
            data[i].images[0] = {
              'image_file': null,
              'image_ext': null
            }
          }
          if (data[i].type == 'point') {
            mymap.addMarker(data[i].points[0].longitude, data[i].points[0].latitude, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images[0].image_file, data[i].images[0].image_ext);
          } else {

            mymap.addPolygon(data[i].points, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images[0].image_file, data[i].images[0].image_ext)
          }
        }
      })
    });

    function setAddData() {
    	$('.modal').attr('id', 'exampleModal');
    	swal({
    		title: '<p>Selecciona el tipo de dato a agregar</p>',
    		html:
				'<form><input type="checkbox" id="point" onclick="checkboxes(this)"> Punto<br><input type="checkbox" id="polygon" onclick="checkboxes(this)"> Polígono<br></form>'+
					'<p>Para agregar un <strong>punto</strong>, clickea sobre el mapa</p>'+
					'<p>Para agregar un <strong>poligono</strong>, primero doble click sobre mapa y luego manten presionada la tecla shift',
			showCancelButton: true,
			cancelButtonText: 'Cancelar'

    	}).then(function () {
    		var type = localStorage.getItem('type');
    		if (type == 'point') {
    			mymap.addPoint();
    		} else {
    			mymap.addDraw();
    		}
    	});
    }

    function checkboxes(asdf) {
    	var id = $(asdf).attr('id');
    	if (id == 'point') {
    		localStorage.setItem('type', 'point');
    		$('#polygon').attr('checked', false);
    	} else {
    		localStorage.setItem('type', 'polygon');
    		$('#point').attr('checked', false);
    	}
    }

    function search(){
      var names = {!! json_encode($names) !!};
      var points = {!! json_encode($points) !!};
      var search = $('.typeahead').val();
      var finded = names.indexOf(search);
      if (finded > -1) {
        lonlat = points[finded];
        mymap.setCenter(lonlat, 15);
        var search = $('.typeahead').val('');
      }
    }
  </script>

	@include('shared.add-data')


@endsection