@extends('base')

@section('title', 'Mi perfil | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-curador')	

			<div class="row my-maps">
				<h3 class="title">Mis mapas</h3>

		        @if (session('status'))
		           <div class="col-12 alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
		                {{ session('status') }}
		            </div>
		        @endif

		        @foreach ($errors->all() as $error)
		            <div class="col-12 alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
						</button>
		            	{{ $error }}
		        	</div>
		        @endforeach

			        <div class="col-4">
								<p><strong>Nombre:</strong> Mapa Público</p>
					</div>
					<div class="col-8" style="text-align: right">
						<!--	<a href="/curador/my-maps/personalize">Personalizar <i class="fa fa-pencil" aria-hidden="true"></i></a>-->
						<button class="btn" ><a href="/curador/public-map">Ver mapa <i class="fa fa-map" aria-hidden="true"></i></a></button>
					</div>

					@foreach ($user_maps as $map)
						<div class="col-4">
							<p><strong>Nombre:</strong> {{ $map->name }}</p>
						</div>
						<div class="col-4">
							<p><strong>Fecha creación:</strong> {!! $map->created_at->format('d-m-Y') !!} </p>
						</div>
						<div class="col-4" style="text-align: center"><a href="{!! action('Curador\MapsController@privateMap', $map->id) !!}">Ver mapa</a></div>
					@endforeach
			</div>

		</div>

@endsection