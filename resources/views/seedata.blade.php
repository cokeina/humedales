<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('/css/ol.css') }}" type="text/css">
<script src="{{asset('/js/ol.js')}}" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<div id="map" class="map" style="width:100%"></div>

<script src="{{ URL::asset('js/mapedit.js')}}"></script>
<script>
    
    let lng  = parseFloat(localStorage.getItem('lngLocate'));
	let lat  = parseFloat(localStorage.getItem('latLocate'));
    let type = '{{ $data->type }}'; 

    mymap = MapMaker();

    mymap.createOSMap(lng,lat, 10);

    let points = {!! $data->points !!};
    
    if(type == "point")
    {
        mymap.addMarker(lng, lat,'{{ $data->category_id }}','{{ $data->theme_id }}','{{ $data->id }}','x','y',[]);
    }
    else
    {
        mymap.addPolygon(points,'{{ $data->category_id }}','{{ $data->theme_id }}','{{ $data->id }}','x','y',[]);
    }
    



</script>