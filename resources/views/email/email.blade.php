
Hola, <br><br>

Hemos recibido una solicitud asociada a este correo para registrarte como usuario del Mapa Interactivo de Humedales Urbanos. Por favor, pincha este <a href='{{url("/verifyemail/".$email_token)}}'>enlace</a> para verificar tu correo.
<br>
¡Gracias por sumarte a este red! Con tu ayuda avanzamos en la co-creación de conocimiento e iniciativas que ponen nuestros humedales y naturaleza urbana en en el centro para lograr ciudades más sustentables.
<br><br>
Equipo Humedales Urbanos