@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.min.css') }}">
@endsection


@section('content')
    <div class="container">
	<div class="form-section">
        <div class="row profile" style="border-radius: 25px;">
            <div class="col-12 admin-users">
                <h3>Contacto</h3>
				<hr>
				@foreach ($errors->all() as $error)
					<p class="alert alert-danger">{{ $error }}</p>
				@endforeach
				@if(session()->has('message'))
					<div class="alert alert-success">
						{{ session()->get('message') }}
					</div>
				@endif
				<form method="post" onsubmit="changeButton()">
				<input type="hidden" name="_token" value="{!! csrf_token() !!}">

					<div class = "row">
						<div class="form-group col-lg-6">
						<label for="name">Nombre:</label>
						<input type="text" class="form-control" id="name" name="name" required placeholder="Ingrese nombre"  value="{{ old('name') }}">
						</div>

						<div class="form-group col-lg-6">
						<label for="name">Email:</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Ingrese email" required value="{{ old('email') }}">
						</div>
					</div>
					<div class ="row">
						<div class="form-group col-lg-12">
						<label for="name">Mensaje:</label>
						<textarea  rows="5" class="form-control" name="message" required placeholder="Ingrese mensaje" name="password"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-lg-2" >
							<button class="btn" type="submit" id="sendButton" style="background:#638F41;color:white;width:100%">Enviar</button>
						</div>
					</div>
					

					

				</form>
            </div>
        </div>
	</div>
    </div>

@endsection

@section('js')
<script src="{{ asset('plugins/datatables/dataTables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>

<script>

    var table = null;

    $(document).ready(function() {
    });

	function changeButton()
	{
		$("#sendButton").attr("disabled",true);
		$("#sendButton").html("Enviando...");
	}


</script>
@endsection