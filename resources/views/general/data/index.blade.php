@extends('base')
@extends('shared/modals')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
<style>
	.profile
	{
		width:100%!important;
	}
	.dataTables_info
	{
		font-size:0.9rem!important;
	}

	.btn-sm
	{
		font-size: 11px;
	}

	th { font-size: 12px; }
	td { font-size: 11px; }

</style>

@endsection

@section('title', 'Datos de la plataforma | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-logged')	

			<div class="row profile">
				<div class="col-12 admin-users">
					<h3>Mis Datos</h3>

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif
                    @if (session('error'))
		                <div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('error') }}
		                </div>
		            @endif

					<div class="row col-lg-12" style="margin-top:40px">
					  <div class="col-lg-3">
							<div class="form-group">
								<label class="label-control">Categoría</label>
								<select id="cmbCategory" class="form-control">
									<option value='0'>Todos</option>
									@foreach ($category as $cat)
										<option value='{{ $cat->id }}'>{{ $cat->category_name }}</option>
									@endforeach
								</select>
							</div>

					  </div>
						<div class="col-lg-3">
						    <div class="form-group">
								<label class="label-control">Tema</label>
								<select id="cmbTheme" class="form-control">
									<option value='0'>Todos</option>
									@foreach ($theme as $t)
										<option value='{{ $t->id }}'>{{ $t->themes_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-3">
						    <div class="form-group">
								<label class="label-control">Estado</label>
								<select id="cmbStatus" class="form-control">
									<option value='99'>Todos</option>
									<option value='0'>Guardado</option>
									<option value='2'>En Revisión</option>
									<option value='1'>Publicado</option>
									<option value='3'>Rechazado</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
						    <div class="form-group">
								<label class="label-control">Mapa</label>
								<select id="cmbMap" class="form-control">
								<option value='0'>Todos</option>
									@foreach ($maps as $map)
										<option value='{{ $map->id }}'>{{ $map->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row col-lg-12">
						<div class="col-lg-3">
							<div class="form-group">
								<label class="label-control">Título</label>
								<input type="text" class="form-control" id="txtTitle">
							</div>

					  	</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label class="label-control">Fecha Mapeo</label>
								<input type="date" class="form-control" id="dtStartDate">
							</div>

					  	</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label class="label-control">Fecha Publicación</label>
								<input type="date" class="form-control" id="dtPubDate">
							</div>

					  	</div>
						<div class="col-lg-3">
						</div>
					</div>

					<div class="row col-lg-12" style="margin-top:20px">
						<div class="col-lg-12">
						
						<button class="btn btn-primary pull-right" onclick="legend()" style="margin-left:10px;height:38px"><i class="fa fa-info"> </i></button>
						<button class="btn btn-primary pull-right" onclick="search()" style="margin-left:10px">Buscar</button>
						<button class="btn btn-primary pull-right" onclick="cleanSearch()"  style="margin-left:10px" >Limpiar</button>
						<button class="btn btn-primary pull-right" onclick="multiClone()" >Clonar</button>
						</div>
					</div>

					<div class="row col-lg-12" style="margin-top:20px">
					<table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr>
							    <th><input type="checkbox" onchange='handleChange(this);'></th>
								<th>#</th>
							    <th>Mapa</th>
								<th>Titulo</th>
								<th>Categoría</th>
								<th>Tema</th>
								<th>Estado</th>
								<th>Acciones</th>
								<th>Fecha Mapeo</th>
								<th>Fecha Aprobación</th>
								
							</tr>
						</thead>
						
					</table>
					</div>

				</div>
			</div>

		</div>

@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>

<script> 

var table = null;

$(document).ready(function() {
    
	$("#rejectCause").change(function(ths,val){

		if($("#rejectCause").val() == "4")
		{
			$("#textCause").prop("disabled",false);
		}
		else
		{
			$("#textCause").prop("disabled",true);
			$("#textCause").val("");
		}

	});

	$("#saveText").click(function(){

		$("#saveText").prop("disabled",true);

		var frm = new FormData();
		frm.append('cause',$("#rejectCause option:selected").text());
		frm.append('other',$("#textCause").val());

		$.ajax({
				url:'{{ url('/general/data/reject') }}/'+$("#rejectH").val(),
				data:frm,
				cache: false,
				processData: false,
				contentType: false,
				type: 'POST',
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success:function(response){
					$("#saveText").prop("disabled",false);
					if(response.code == 200)
					{
						$("#modalReject").modal("toggle");
						swal('Dato rechazado','Operación realizada con éxito','success');
						table.ajax.reload();
					}
					else
					{
						swal('Error',response.message,'error');
					}
				},
				failure:function(response)
				{
					$("#saveText").prop("disabled",false);
					swal('Error','Error en la operación');
				}
		});

		});
	
	
	if(localStorage.general_data_filter_category)
		{
			
			$("#cmbCategory").val(localStorage.general_data_filter_category);
			localStorage.removeItem("general_data_filter_category");
		}

		if(localStorage.general_data_filter_theme)
		{
			
			$("#cmbTheme").val(localStorage.general_data_filter_theme);
			localStorage.removeItem("general_data_filter_theme");
		}

		if(localStorage.general_data_filter_status)
		{
			
			$("#cmbStatus").val(localStorage.general_data_filter_status);
			localStorage.removeItem("general_data_filter_status");
		}

		if(localStorage.general_data_filter_map)
		{
			
			$("#cmbMap").val(localStorage.general_data_filter_map);
			localStorage.removeItem("general_data_filter_map");
		}

		if(localStorage.general_data_filter_text)
		{
			
			$("#txtTitle").val(localStorage.general_data_filter_text);
			localStorage.removeItem("general_data_filter_text");
		}

		if(localStorage.general_data_filter_sdate)
		{
			
			$("#dtStartDate").val(localStorage.general_data_filter_sdate);
			localStorage.removeItem("general_data_filter_sdate");
		}

		if(localStorage.general_data_filter_pdate)
		{
			
			$("#dtPubDate").val(localStorage.general_data_filter_pdate);
			localStorage.removeItem("general_data_filter_pdate");
		}
	
	
		table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
				"processing": true,
				"fixedColumns": true,
				"iDeferLoading": 0,
				"scrollX":        true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {

					data.status   = $("#cmbStatus").val();
					data.category = $("#cmbCategory").val();
					data.theme    = $("#cmbTheme").val();
                    data.map        = $("#cmbMap").val();
					data.title      = $("#txtTitle").val();
					data.created    = $("#dtStartDate").val();
					data.pub        = $("#dtPubDate").val();

					if(data.created != '')
					{
						data.offset     = moment(data.created).utcOffset();
					}

					if(data.pub != '')
					{
						data.offsetp     = moment(data.pub).utcOffset();
					}

					data.type     = 'general';

					$.get('{{ url('/general/data/grid' ) }}?' + $.param(data),function(response){
												
						callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.datas
						});
						
						if(localStorage.general_data_page)
						{
							table.page(parseInt(localStorage.general_data_page)).draw('page');
							localStorage.removeItem("general_data_page");
						}

					});

                },
                "paging": true,
				"order": [[ 1, 'asc' ]],
                "columns": [
                    { "data": "id","orderable":false, "render":function(a,b,c){

					return "<input type='checkbox'  name='rows' value='"+c.id+"'  >";

					}},
					{ "data": "id", "visible": false,},
					{ "data": "map"},
					{ "data": "title"},
					{ "data": "cat"},
					{ "data": "theme"},
					{ "data": "id",
                        "class":"text-center",
						render:function(data,type,full,meta)
						{
							if(full.status == 1)
							{
								return '<button type="button" class="btn btn-sm btn-success">Publicado</button>';
							}
							if(full.status == 0)
							{
								return '<button type="button" class="btn btn-sm btn-info">Guardado</button>';
							}
							if(full.status == 2)
							{
								return '<button type="button" class="btn btn-sm btn-primary">En Revisión</button>';
							}
							if(full.status == 3)
							{
								return '<button type="button" class="btn btn-sm btn-danger">Rechazado</button>';
							}
								
						}
					},
					{ "data": "status",
					   class:"text-center",
					   render:function(data,type,full,meta)
					   {
						   var content = '';

							/*if(full.status == 1)
							{
								var public = 0;
								if(full.map === 'Public map') public = 1;
								
								content += '<button class="btn" style="background-color: transparent;" onclick="locate(\''+full.id+'\',\''+full.latitude+'\',\''+full.longitude+'\')" data-toggle="tooltip" title="Ver dato"><i class="fa fa-search" style="color:green" aria-hidden="true"></i></button>  ';
							}*/

							if(full.value == 2)
							{
								//INDEPENDIENTE DEL ESTADO , PUEDE EDITAR 
								content    += '<button class="btn" style="background-color: transparent;" ><a onclick="redirect('+full.id+')"  data-toggle="tooltip" title="Ver/Editar dato"><i class="fa fa-search" style="color:green" aria-hidden="true"></i></a> </button> ';
							}
							else
							{
                                if(full.status == 1 || full.status == 2)
								{
									content    += '<button class="btn" style="background-color: transparent;" ><a onclick="locate(\''+full.id+'\',\''+full.latitude+'\',\''+full.longitude+'\')" data-toggle="tooltip" title="Ver/Editar dato"><i class="fa fa-search" style="color:green" aria-hidden="true"></i></a> </button> ';
								}
								else
								{
									content    += '<button class="btn" style="background-color: transparent;" ><a onclick="redirect('+full.id+')"  data-toggle="tooltip" title="Ver/Editar dato"><i class="fa fa-search" style="color:green" aria-hidden="true"></i></a> </button> ';
								}
							}

							if(full.value == 2 &&  (full.status == 1 || full.status == 2))
							{
								content 	  += '<button class="btn btn-sm" style="background-color: transparent;" onclick="rejectData('+full.id+')"  data-toggle="tooltip" title="Rechazar dato"><i class="fa fa-window-close" style="color:red" aria-hidden="true"></i> </button>';
							}
							else content 	  += '<button class="btn" style="background-color: transparent;" onclick="dropData('+full.id+')" ><i class="fa fa-window-close" style="color:red" aria-hidden="true"></i> </button>';
						   
						   //
							
						   
						   
						   return content;				
					   }		
					},
					{ "data": "created_at",
                       render:function(data)
                       {
                            var localTime  = moment.utc(data).toDate();
                            return moment(localTime).format('DD-MM-YYYY HH:mm:ss');
                       }
					},
					{ "data": "approved_at",
						render:function(data)
						{
							 if(data !== null)
							 {
								 var localTime  = moment.utc(data).toDate();
								 return moment(localTime).format('DD-MM-YYYY HH:mm:ss');
							 }
							 else return 'S/F';
							 
						}
                    }
					
					
                ]
        });
        
    
    });

function locate(id,latitude,longitude)
{
	
	localStorage.setItem('general_data_page',table.page());
	localStorage.setItem('general_data_filter_category',$("#cmbCategory").val());
	localStorage.setItem('general_data_filter_theme',$("#cmbTheme").val());
	localStorage.setItem('general_data_filter_status',$("#cmbStatus").val());
	localStorage.setItem('general_data_filter_map',$("#cmbMap").val());
	localStorage.setItem('general_data_filter_text',$("#txtTitle").val());
	localStorage.setItem('general_data_filter_sdate',$("#dtStartDate").val());
	localStorage.setItem('general_data_filter_pdate',$("#dtPubDate").val());
	
	window.location.href = '{{ url("/general/data/show") }}/'+id;
	
	/*let url = '';

	localStorage.setItem('latLocate',latitude);
	localStorage.setItem('lngLocate',longitude);

	var left = (screen.width/2)-(500/2);
    var top = (screen.height/2)-(500/2);

	var myWindow = window.open("{{ url('/seedata')}}/"+id,"_blank","width=500,height=500,left="+left);

	return false;*/
	
	
	/*let url = '';

	if(isPublic == 1)
	{
		url = '{{ url("/general/public-map") }}';
	}
	else
	{
		url = '{{ url("/general/private-map") }}'+'/'+map_id;
	}

	localStorage.setItem('latLocate',latitude);
	localStorage.setItem('lngLocate',longitude);

	window.open(url,'_blank');*/

}

function redirect(fid)
{
	localStorage.setItem('general_data_page',table.page());
	localStorage.setItem('general_data_filter_category',$("#cmbCategory").val());
	localStorage.setItem('general_data_filter_theme',$("#cmbTheme").val());
	localStorage.setItem('general_data_filter_status',$("#cmbStatus").val());
	localStorage.setItem('general_data_filter_map',$("#cmbMap").val());
	localStorage.setItem('general_data_filter_text',$("#txtTitle").val());
	localStorage.setItem('general_data_filter_sdate',$("#dtStartDate").val());
	localStorage.setItem('general_data_filter_pdate',$("#dtPubDate").val());

	
	window.location.href = "{!! action('General\MapsController@editData','') !!}/"+fid;
}

function legend()
{
	$("#modalLegend").modal("show");
}


function search()
{
	table.ajax.reload();
}

function cleanSearch()
{
	$("#cmbStatus").val(99);
	$("#cmbCategory").val(0);
	$("#cmbTheme").val(0);
	$("#cmbMap").val(0);
	$("#txtTitle").val("");
	$("#dtStartDate").val("");
	$("#dtPubDate").val("");

	table.ajax.reload();
}

function dropData(id)
{
	swal({
	title: 'Eliminar dato',
	text: "Con esta acción se eliminará definitivamente el dato seleccionado",
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	cancelButtonText:'Cancelar',
	confirmButtonText: 'Aceptar'
	}).then((result) => {
	
		location.href = "{!! action('General\MapsController@destroyData','') !!}/"+id;

	}).catch((error)=>{

	});
}


function multiClone(id)
{
	swal({
	title: 'Copiar datos en mapa público',
	text: "Con esta acción se crearán nuevos datos en el mapa público con la información seleccionada en estado en revisión",
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	cancelButtonText:'Cancelar',
	confirmButtonText: 'Aceptar'
	}).then((result) => {
	
		var ids = [];

		$("input[name='rows']").each(function(){
			if(this.checked)
			{
				ids.push(this.value);
			}
		});

		if(ids.length == 0)
		{
			swal('Clonar datos','Debe seleccionar al menos un dato','warning');
		}
		else
		{
			
			//realizamos una llamada ajax con los elementos del arreglo
			$.ajax({
				url:'{{ url('/general/data/multiclone') }}/?ids='+ids.join(','),
				cache: false,
                processData: false,
                contentType: false,
                type: 'GET',
				dataType:'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success:function(response)
				{
					if(response.success)
					{
						swal('Clonación de datos','Operación realizada con éxito','success');
					}
					else
					{
						swal('Clonación de datos',response.message,'error');
					}
				}

			});
		}

		//location.href = "{{ url('/admin/data/clone/') }}/"+id;

	}).catch((error)=>{

	});
}

function handleChange(check)
{
	if(check.checked == true)
	{
		$("input[name='rows']").prop('checked', true);
	}
	else $("input[name='rows']").prop('checked', false);
}

function rejectData(id)
{
	$("#textCause").val("");
	$("#textCause").prop("disabled",true);
	$("#rejectCause").val("1");
	$("#rejectH").val(id);
	
	$("#modalReject").modal("show");
	
}

</script>

@endsection