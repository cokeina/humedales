@extends('base')

@section('title', 'Mi perfil | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-logged')	

	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<div class="row my-maps">
				<h3 class="title">Mis mapas</h3>

			        <div class="col-4">
						<p><strong>Nombre:</strong> Mapa Público</p>
					</div>
					<div class="col-8" style="text-align: right">
						<button class="btn"><a href="/general/public-map">Ver mapa <i class="fa fa-map" aria-hidden="true"></i></a></button>
					</div>


					@foreach ($user_maps as $maps)
						<div class="col-12">
							<a href="/general/private-map/{{ $maps->id }}">
								<p style="margin-top: .5em">{{ $maps->name }}</p>
							</a>
						</div>
					@endforeach

			</div>

		</div>

@endsection