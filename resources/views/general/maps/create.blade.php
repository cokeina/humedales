@extends('base')

@section('title', 'Crea un mapa')

@section('content')

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear un mapa</p>

				<form method="post">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<div class="form-group">
							<label for="name">Nombre del mapa(*):</label>
							<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
						</div>

						<div class="form-group">
							<label for="name">Categorías(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="1">
										<img src="{{asset('images/cat1.png')}}">
										<label for="checkboxes-0">
										Naturaleza urbana
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="2">
										<img src="{{asset('images/cat2.png')}}">
										<label for="checkboxes-0">
										Iniciativas y buenas prácticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="3">
										<img src="{{asset('images/cat3.png')}}">
										<label for="checkboxes-0">
										Comunidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="4">
										<img src="{{asset('images/cat4.png')}}">
										<label for="checkboxes-0">
										Problemáticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="5">
										<img src="{{asset('images/cat5.png')}}">
										<label for="checkboxes-0">
										Historia
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="6">
										<img src="{{asset('images/cat6.png')}}">
										<label for="checkboxes-0">
										Herramientas y datos abiertos
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="7">
										<img src="{{asset('images/cat7.png')}}">
										<label for="checkboxes-0">
										Proyectos regionales
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="8">
										<img src="{{asset('images/cat8.png')}}">
										<label for="checkboxes-0">
										Regulación
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="9">
										<img src="{{asset('images/cat9.png')}}">
										<label for="checkboxes-0">
										Comunicación y difusión
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="10">
										<img src="{{asset('images/cat10.png')}}">
										<label for="checkboxes-0">							
										Educación para la sustentabilidad
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group" id="themes">
							<label for="name">Temas(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="1">
										<img src="{{asset('images/21.png')}}">
										<label for="checkboxes-0">
										Humedales y espacios del agua
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="2">
										<img src="{{asset('images/20.png')}}">
										<label for="checkboxes-0">
										Espacio público
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="3">
										<img src="{{asset('images/19.png')}}">
										<label for="checkboxes-0">
										Biodiversidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="4">
										<img src="{{asset('images/18.png')}}">
										<label for="checkboxes-0">
										Movilidad y accesibilidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="5">
										<img src="{{asset('images/17.png')}}">
										<label for="checkboxes-0">
										Acción Colectiva
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="6">
										<img src="{{asset('images/16.png')}}">
										<label for="checkboxes-0">
										Infraestructura verde
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="7">
										<img src="{{asset('images/15.png')}}">
										<label for="checkboxes-0">
										Patrimonio natural y cultural
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="8">
										<img src="{{asset('images/14.png')}}">
										<label for="checkboxes-0">
										Organizaciones sustentables
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="9">
										<img src="{{asset('images/13.png')}}">
										<label for="checkboxes-0">
										Renovación urbana y ambiental
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="10">
										<img src="{{asset('images/12.png')}}">
										<label for="checkboxes-0">
										Cambio climático
										</label>
									</div>
								</div>
							</div>
						</div>

					<div class="form-group" id="submit-button">
						<button class="btn">
							Crear
						</button>
					</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

@endsection