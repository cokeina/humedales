@extends('base')

@section('title', 'Mi perfil | Mapa interactivo de humedales urbanos')

@section('content')

		@include('shared.navbar-logged')	
		
		<div class="container" id="initiatives">

		@if (count($initiatives) == 0)
			<div class="row" id="no-initiatives">
				<div class="col-12">
					<p>¡Aún no tienes iniciativas creadas, crea una!</p>
					<a href="/initiatives/create/first-step"><button class="btn btn-primary">Crear iniciativa</button></a>
				</div>
			</div>
		@endif

		@foreach ($initiatives as $initiative)

			<div class="row" id="initiative-detail">
				<div class="col-sm-4" id="image-initiative">

					<img src="data:{{ $initiative->image_ext }};charset=utf-8;base64,{{ $initiative->image }}">

				</div>
				<div class="col-sm-4">
					
					<p id="title">Nombre de iniciativa:</p>
					<p id="data">{{ $initiative->initiative_title }}</p>
					<p id="title">Fecha de inicio:</p>
					<p id="data">{{ $initiative->start_date }}</p>	
					<p id="title">Fecha de término:</p>
					<p id="data">{{ $initiative->finish_date }}</p>

				</div>
				<div class="col-sm-2">
					
					<p id="title">Optimo:</p>
					<p id="data">$ {{ $initiative->optimum }}</p>
					<p id="title">Recaudado:</p>
					<p id="data">$ {{ $initiative->actual }}</p>	
					<p id="title">Estado:</p>
					<p id="data"
					@if ($initiative->status_id == 1 || $initiative->status_id == 2)
						style="color: #D38728"
					@elseif ($initiative->status_id == 3)
						style="color: #1E77A9"
					@elseif ($initiative->status_id == 4)
						style="color: red"
					@elseif ($initiative->status_id == 5)
						style="color: green"
					@elseif ($initiative->status_id == 5)
						style="color: grey"
					@endif
					>{{ $initiative->status }}</p>

				</div>
				<div class="col-sm-2">
					
					@if ($initiative->status_id == 1 || $initiative->status_id == 2)
						<a href="/initiatives/create/first-step"><button type="button" class="btn btn-primary" style="background:#D38728;border-color:#D38728;width:100%">Completar</button></a>
					@elseif ($initiative->status_id == 3)
						<a href="/initiatives/detail/{{ $initiative->id }}"><button type="button" class="btn btn-primary" style="width:100%;background:#DADF4A;border-color:#DADF4A">Ver inicitiva</button></a>
					@elseif ($initiative->status_id == 4)

						<a href="/initiatives/detail/{{ $initiative->id }}">
							
						<button type="button" class="btn btn-primary" style="width:100%;background:#DADF4A;border-color:#DADF4A;margin-bottom: 2em;">Ver inicitiva</button>

						</a>

						<button type="button" class="btn btn-danger" data-toggle="modal" style="width:100%" data-target="#{{ $initiative->id }}">Ver motivo</button>


						<!-- Modal -->
						<div class="modal fade" id="{{ $initiative->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Motivo de rechazo</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          									<span aria-hidden="true">&times;</span>
        								</button>
									</div>
									<div class="modal-body">
										<p>{{ $initiative->rejection_description }}</p>
									</div>
								</div>
							</div>
						</div>


					@elseif ($initiative->status_id == 5)
						<button type="button" class="btn btn-primary" style="width:100%;background:#DADF4A;border-color:#DADF4A">Ver inicitiva</button>
					@elseif ($initiative->status_id == 5)
						<a href="/initatives/{{ $initiative->id }}"><button type="button" class="btn btn-secondary">Ver inicitiva</button></a>
					@endif

				</div>
			</div>

		@endforeach

		@if (count($initiatives) != 0)
			<div class="row" id="create-initiative">
				<div class="col-12">
					<p>Si no tienes iniciativas incompletas, puedes crear una nueva, en caso contrario deberás temrinar la iniciativa pendiente</p>
					<a href="/initiatives/create/first-step"><button class="btn btn-primary">Crear iniciativa</button></a>
				</div>
			</div>
		@endif

		</div>

@endsection