@extends('base')

@section('title', 'Usuarios | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-admin')	

			<div class="row profile">
				<div class="col-12 admin-users">
					<h3>Usuarios</h3>

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif

					<table class="table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Email</th>
								<th>Rol</th>
								<th>Fecha creación</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
								<tr>
									<td>{!! $user->name !!}</td>
									<td>{!! $user->email !!}</td>
									<td>{!! $user->roles_id !!}</td>
									<td>{!! $user->created_at->format('d-m-Y') !!}</td>
									<td style="text-align: center">
									    <button class="btn" style="background-color: transparent;" ><a href="{!! action('Admin\AdminController@editUser', $user->id) !!}" data-toggle="tooltip" title="Editar usuario"><i class="fa fa-pencil-square" style="color:green" aria-hidden="true"></i></a></button>
										<button class="btn" style="background-color: transparent;" onclick="dropData('{{ $user->id }}')" ><i class="fa fa-window-close" style="color:red" aria-hidden="true"></i> </button>

									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>

<script>

function dropData(id)
{
	swal({
	title: 'Eliminar usuario',
	text: "Con esta acción se eliminará definitivamente al usuario seleccionado",
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	cancelButtonText:'Cancelar',
	confirmButtonText: 'Aceptar'
	}).then((result) => {
	
		location.href = "{!! action('Admin\AdminController@destroyUser', '') !!}/"+id;

	}).catch((error)=>{

	});
}

</script>

@endsection