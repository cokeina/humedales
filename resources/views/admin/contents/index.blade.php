@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/summernote-bs4.css') }}">
<style>
	.profile
	{
		width:100%!important;
	}

	.dataTables_info
	{
		font-size:0.9rem!important;
	}

</style>

@endsection

@section('title', 'Datos de la plataforma | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-admin')	

			<div class="row profile">
				<div class="col-12 admin-users">
					<h3>Contenido de la plataforma</h3>

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif


					<div class="row col-lg-12" style="margin-top:20px">
					<table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th>Descripción</th>
								<th>Acciones</th>
							</tr>
						</thead>
						
					</table>
					</div>
				</div>
			</div>

		</div>


<div class="modal fade" id="modaltext">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modificar texto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  	  <input type="hidden" id="hid">	
		  <textarea id="summernote" name="editordata" rows="10"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" id="saveText" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalImage">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modificar imagen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  	  <input type="hidden" id="hidi">	
		  <input type="file" class="form-control" id="fileimage">
      </div>
      <div class="modal-footer">
        <button type="button" id="saveImage" class="btn btn-primary">Guardar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('js/summernote-bs4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>


<script> 

var table = null;

$(document).ready(function() {
    
	    $('#summernote').summernote({
			tabsize: 2,
			height: 300
		});

		$('.note-editable').css('margin-top','40px');

        table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
				"processing": true,
				"fixedColumns": true,
				"scrollX":        true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {


					$.get('{{ url('/admin/contents/grid' ) }}?' + $.param(data), function(response){
						callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.contents
                        });
					});

                },
                "paging": true,
                "columns": [
                    { "data": "id", "visible": false,},
                    { "data": "description"},
                    { "data": "id",class:"text-center", render:function(a,b,c,d){
                        return '<button class="btn btn-primary" onclick="showModal('+c.type+','+c.id+')">Editar</button>'
                    }}
					
                ]
        });

		$("#saveText").click(function(){

			var params = new FormData();
			params.append('id',$("#hid").val());
			params.append('content',$('#summernote').summernote('code'));
			params.append('type',1);

			$.ajax({
                    url:'{{ url('/admin/contents') }}',
                    data:params,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){

                         if(response.code == 200)
                         {
                            swal("Editar Texto", "Contenido modificado de forma exitosa", "success");
                            table.ajax.reload();
                            $('#modaltext').modal('toggle');
                         }

                         if(response.code == 201)
                         {
                            errors = [];
                            
                            for (x in response.data.errors)
                            {
                                errors.push('<li>'+response.data.errors[x].join(",")+'</li>');
                            }

                            console.log(errors);

                            swal("Error",errors.join(" "), "error");
                            


                         }

                    },
                    failure:function(response)
                    {

                    }
            });

		});

		$("#saveImage").click(function(){

			var frm = new FormData();

            var file =  $('#fileimage')[0].files[0] ;
            if (file == undefined) file = '';
                
            frm.append('id',$("#hidi").val());
            frm.append('file',file);
			frm.append('type',0);

			$.ajax({
                    url:'{{ url('/admin/contents') }}',
                    data:frm,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){
                       
                         if(response.code == 200)
                         {
                            swal("Modificar Imagen", "Contenido modificado de forma exitosa", "success");
                            $('#modalImage').modal('toggle');
                         }

                         if(response.code == 201)
                         {
                            errors = [];
                            
                            for (x in response.data.errors)
                            {
                                errors.push('<li>'+response.data.errors[x].join(",")+'</li>');
                            }

                            console.log(errors);

                            swal("Error",errors.join(" "), "error");
                            


                         }

                    },
                    failure:function(response)
                    {

                    }
                });

		});
        
    
    });



function showModal(type,id)
{
	if(type == 0)
	{
		$("#fileimage").val('');
		$("#hidi").val(id);
		$("#modalImage").modal("show");
	}
	else
	{
		$.get('{{ url('/admin/contents/' ) }}/' + id, function(response){
		
			if(response.code == 200)
			{
				$('#summernote').summernote('code', response.data.content);
				$("#hid").val(id);
				$('#modaltext').modal('show');
			}

			//
			

		});
	}
}


</script>

@endsection