@extends('base')

@section('title', 'Rechazar iniciativa | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container admin">
			
			@include('shared.navbar-admin')	

			<div class="row profile justify-content-md-center">
				<div class="col-8 offset-2">

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif

			        @foreach ($errors->all() as $error)
			            <p class="alert alert-danger">{{ $error }}</p>
			        @endforeach
						
                    <h3 style="margin-bottom:1em">Rechazar iniciativa</h3>
                    <form method="POST">

						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" name="initiative" value="{{ $initiative_id }}">

	                    <div class="form-group row">
							<div class="col-12">
								<label for="name">Motivo de rechazo: </label>

	                       		<textarea class="form-control" name="motive" rows="3"></textarea>
							</div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-lg-12" style="text-align: right">
	                            <button type="submit" class="btn btn-danger">Rechazar</button>
	                        </div>
	                    </div>
					</form>
				</div>
			</div>

		</div>

@endsection