@extends('base')

@section('title', 'Iniciativas | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-admin')	

			<div class="row profile">
				<div class="col-12 admin-users">
					<h3>Iniciativas</h3>

		            @if (session('status'))
		                <div class="alert alert-success">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
		                    {{ session('status') }}
		                </div>
		            @endif

					<table class="table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Creador</th>
								<th>Fecha creación</th>
								<th style="text-align:center;">Ver</th>
								<th style="text-align:center;">Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($initiatives as $initiative)
								<tr>
									<td>{!! $initiative->initiative_title !!}</td>
									<td>{!! $initiative->initiative_name !!}</td>
									<td>{!! $initiative->created_at->format('d-m-Y') !!}</td>
									<td style="text-align: center;">
										<a href="/initiatives/detail/{{ $initiative->id }}">
											<button style="background: #DADF4A;border-color: #DADF4A" class="btn btn-primary">Ver iniciativa</button>
										</a>
									</td>
									<td style="text-align: center">
										<a href="{!! action('Admin\AdminController@acceptInitiative', $initiative->id) !!}" data-toggle="tooltip" title="Aceptar"><button type="button" class="btn btn-primary">Aceptar</button></a>
										<a href="/admin/initiatives/reject/{{ $initiative->id }}" data-toggle="tooltip" title="Rechazar"><button type="button" class="btn btn-danger">Rechazar</button></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>

@endsection