@extends('base')
@section('title', 'Mapa interactivo de humedales urbanos')

@section('js')
<script src="{{ asset('js/api.js') }}"></script>
@endsection 
@section('content')

<style>
      .rotate-north {
        top: 70px;
        left: .5em;
      }
      .ol-touch .rotate-north {
        top: 80px;
      }
      .set-style1 {
        top: 100px;
        left: .5em;
      }
      .ol-touch .set-style1 {
        top: 110px;
      }
      .set-style2 {
        top: 130px;
        left: .5em;
      }
      .ol-touch .set-style2 {
        top: 140px;
      }
      .set-style3 {
        top: 160px;
        left: .5em;
      }
      .ol-touch .set-style3 {
        top: 170px;
      }
      .set-exportkml {
        top: 70;
        left: .5em;
      }
      .ol-touch .set-exportkml {
        top: 80;
      }
      .set-example {
        top: 100;
        left: .5em;
      }
      .ol-touch .set-example {
        top: 110;
      }
      
    </style>

	
  <div class="row info">
		<div class="container">
			<div class="col-12">
				<img src="{{asset('images/infografia1.png')}}" width="100%">
			</div>			
		</div>
	</div>
	<div class="row no-gutters">
		<div class="col-10" id="map-container">
			<div id="map" class="map"><div id="popup"></div></div>

      <div class="typeahead__container" style="width:250px!important;position: absolute; top: 10px; right: 10px; padding: 3px;background:rgba(255,255,255,0.4);border-radius:5px;">
          <div class="typeahead__field">

          <span class="typeahead__query">
              <input class="js-typeahead-input"
                         name="q"
                         type="search"
                         autocomplete="off"/>
          </span>
              <span class="typeahead__button">
              <button onclick="search()">
                  <span class="typeahead__search-icon"></span>
              </button>
          </span>

          </div>
      </div>

      <div class="geolocation__container" style="position: absolute; left: 10px; bottom: 10px;background:rgba(255,255,255,0.4);padding: 3px;">
        <button onclick="setUserPosition()" style="padding: 6px;background:rgba(0,60,136,.5); border:none; border-radius: 2px;">
            <i class="fa fa-crosshairs" aria-hidden="true" style="color:white"></i>
        </button>
      </div>

		</div>
		<div class="col-2" id="cat-themes">
			
			<p class="title">Mapa Público</p>

			<button class="btn" type="button" data-toggle="modal" onclick="setAddData()">Agregar dato</button>

			<p class="title">Categorías</p>
			@foreach ($categories as $category)
				<div class="checkbox">
					<input type="checkbox" checked id="category_{{ $category->id }}" onclick="changeMarker('category_{{ $category->id }}', 'category', {{ $category->id }})">
		  			<img src="{{asset('images/icons-map/category/'.$category->id.'.png')}}">
		  			<label for="checkboxes-0">{{ $category->category_name }}</label>
				</div>
			@endforeach


			<div class="divider"></div>
	
			<p class="title">Temas</p>

			@foreach ($themes as $theme)
				<div class="checkbox" id="theme">
					<input type="checkbox" id="theme_{{ $theme->id }}" onclick="changeMarker('theme_{{ $theme->id }}','theme', {{ $theme->id }})" checked>
					<img src="{{asset('images/icons-map/theme/'.$theme->id.'.png')}}">
					<label for="checkboxes-0">{{ $theme->themes_name }}</label>
				</div>
			@endforeach
		</div>
	</div>

  <script>
     var clist = [@foreach($clist as $c) '{{ $c }}', @endforeach];
     var tlist = [@foreach($tlist as $t) '{{ $t }}', @endforeach];
  </script>
  <script src="{{ URL::asset('js/map.js')}}"></script>
  <script src="{{ URL::asset('js/initMap.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.typeahead.js') }}"></script>
  <script>
    var names = {!! json_encode($names) !!};
    var points = {!! json_encode($points) !!};
    $('.js-typeahead-input').typeahead({
        minLength: 1,
        order: "asc",
        group: false,
        maxItemPerGroup: 3,
        hint: true,
        source: {
            data: names
        },
        debug: false
    });

    function search() {
      var search = $('.js-typeahead-input').val();
      var finded = names.indexOf(search);
      if (finded > -1) {
        lonlat = points[finded];
        mymap.setCenter(lonlat, 15);
        $('.js-typeahead-input').val('');
        $('.typeahead__container').removeClass('cancel');
      }
    }

    function setUserPosition() {
      if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition)
      }
    }

    function showPosition(position) {
      var lonlat = [position.coords.latitude, position.coords.longitude];
      mymap.setCenter(lonlat, 15);
    }

    function setAddData() {
    	$('.modal').attr('id', 'exampleModal');
    	swal({
    		title: '<p>Selecciona el tipo de dato a agregar</p>',
    		html:
				'<form><input type="checkbox" id="point" onclick="checkboxes(this)"> Punto<br><input type="checkbox" id="polygon" onclick="checkboxes(this)"> Polígono<br><input type="checkbox" id="lines" onclick="checkboxes(this)"> Linea<br></form>'+
					'<p>Para agregar un <strong>punto</strong>, clickea sobre el mapa</p>'+
          '<p>Para agregar un <strong>poligono</strong>, primero doble click sobre mapa y luego manten presionada la tecla shift</p>'+
          '<p>Para agregar una <strong>línea</strong>, realice doble click sobre el mapa</p>',
			showCancelButton: true,
			cancelButtonText: 'Cancelar'

    	}).then(function () {
    		var type = localStorage.getItem('type');
        
        if(keyPoint != null)
        {
          ol.Observable.unByKey(keyPoint);
        }

        if(keyPolygon != null)
        {
          ol.Observable.unByKey(keyPolygon);
        }

        if(keyLines != null)
        {
          ol.Observable.unByKey(keyLines);
        }
        
        
        if (type == 'point') {
    			mymap.addPoint();
        }
        if (type == 'polygon')
        {
          mymap.addDraw();
        }
        if (type == 'lines')
        {
          mymap.addLines();
        }  


    	});
    }

    function checkboxes(asdf) {
    	var id = $(asdf).attr('id');
    	if (id == 'point') {
    		localStorage.setItem('type', 'point');
        $('#polygon').attr('checked', false);
        $('#lines').attr('checked', false);
      } 
      if (id == 'polygon') {
    		localStorage.setItem('type', 'polygon');
        $('#point').attr('checked', false);
        $('#lines').attr('checked', false);
      } 
      if (id == 'lines') {
    		localStorage.setItem('type', 'lines');
        $('#point').attr('checked', false);
        $('#polygon').attr('checked', false);
      } 
    }

  </script>

<div class="four-section">
		<div class="container">
			<h4>Elige las características con las cuales se desplegará el mapa principal cada vez que ingreses tu usuario y contraseña</h4>
			<a href="/personalize-map"><button class="btn">PERSONALIZAR</button></a>
		</div>
	</div>

  @include('shared.add-data')


@endsection