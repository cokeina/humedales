@extends('base')

@section('title', 'Crea un mapa')

@section('content')

<style>

.ol-control button{
	width:1.375em !important;
	background-color: rgba(0,60,136,.5) !important;
}
</style>

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear un mapa</p>

				<form method="post" id="createForm" role="form">
					
					<!--<fieldset>-->
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" id="clatitude" name="latitude"  value="{{ old('latitude') }}">
						<input type="hidden" id="clongitude" name="longitude" value="{{ old('longitude') }}">
						<input type="hidden" id="czoom" name="zoom"      value="{{ old('zoom') }}">
						

						<div class="form-group">
							<label for="name">Nombre del mapa(*):</label>
							<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
						</div>
						<div class="form-group">
							<label for="name">Mapa Base:</label>
							<select class="form-control" id="styleCmb" name="style" >
									  <option value="basic" @if(old('style') == "basic") selected @endif>Básico</option>
									  <option value="AerialWithLabels" @if(old('style') == "AerialWithLabels") selected @endif>Satelital</option>
									  <option value="RoadOnDemand" @if(old('style') == "RoadOnDemand") selected @endif>Relieves</option>
									</select>
						</div>

						<!--<p>Los valores de personalización pueden quedar en blanco, de ser así, usará valores por defecto</p>
						<div class="form-group">
							<label for="name">¿Donde debe estar centrado?(*):</label>
							<div class="row">
								<div class="col-6">
									<input type="number" class="form-control" id="latitude" name="latitude" value="{{ old('latitude') }}" placeholder="latitude" step="0.00000000001">
								</div>
								<div class="col-6">
									<input type="number" class="form-control" id="longitude" name="longitude" value="{{ old('longitude') }}" placeholder="longitude" step="0.00000000001">
								</div>
							</div>
						</div>



						<div class="form-group">
							<div class="row">
								<div class="col-5">
									<label for="style">¿Qué estilo debe tener?</label>
								</div>
								<div class="col-7">
									<select class="form-control" name="style">
									  <option value="basic" @if(old('style') == "basic") selected @endif>Básico</option>
									  <option value="collinsBart" @if(old('style') == "collinsBart") selected @endif>Básico 2</option>
									  <option value="AerialWithLabels" @if(old('style') == "AerialWithLabels") selected @endif>Satelital</option>
									  <option value="RoadOnDemand" @if(old('style') == "RoadOnDemand") selected @endif>Relieves</option>
									</select>
								</div>
							</div>
						</div>-->

						
						<div id="map" class="map" style="height:300px"></div>
						

						<!--<div class="form-group">
							<div class="row">
								<div class="col-5">
									<label for="style">¿Qué zoom debe tener?</label>
								</div>
								<div class="col-7">
									<select class="form-control" name="zoom">
									  <option value="1" @if(old('zoom') == "1") selected @endif>1</option>
									  <option value="2" @if(old('zoom') == "2") selected @endif>2</option>
									  <option value="3" @if(old('zoom') == "3") selected @endif>3</option>
									  <option value="4" @if(old('zoom') == "4") selected @endif>4</option>
									  <option value="5" @if(old('zoom') == "5") selected @endif>5</option>
									  <option value="6" @if(old('zoom') == "6") selected @endif>6</option>
									  <option value="7" @if(old('zoom') == "7") selected @endif>7</option>
									  <option value="8" @if(old('zoom') == "8") selected @endif>8</option>
									  <option value="9" @if(old('zoom') == "9") selected @endif>9</option>
									  <option value="10" @if(old('zoom') == "10") selected @endif>10</option>
									  <option value="11" @if(old('zoom') == "11") selected @endif>11</option>
									  <option value="12" @if(old('zoom') == "12") selected @endif>12</option>
									  <option value="13" @if(old('zoom') == "13") selected @endif>13</option>
									  <option value="14" @if(old('zoom') == "14") selected @endif>14</option>
									  <option value="15" @if(old('zoom') == "15") selected @endif>15</option>
									  <option value="16" @if(old('zoom') == "16") selected @endif>16</option>
									  <option value="17" @if(old('zoom') == "17") selected @endif>17</option>
									  <option value="18" @if(old('zoom') == "18") selected @endif>18</option>
									  <option value="19" @if(old('zoom') == "19") selected @endif>19</option>
									</select>
								</div>
							</div>
						</div>-->

						<div class="form-group">
							<label for="name">Categorías(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="1" @if(is_array(old('categories')) && in_array(1,old('categories'))) checked @endif >
										<img src="{{asset('images/cat1.png')}}">
										<label for="checkboxes-0">
										Naturaleza urbana
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="2" @if(is_array(old('categories')) && in_array(2,old('categories'))) checked @endif >
										<img src="{{asset('images/cat2.png')}}">
										<label for="checkboxes-0">
										Iniciativas y buenas prácticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="3" @if(is_array(old('categories')) && in_array(3,old('categories'))) checked @endif >
										<img src="{{asset('images/cat3.png')}}">
										<label for="checkboxes-0">
										Comunidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="4" @if(is_array(old('categories')) && in_array(4,old('categories'))) checked @endif >
										<img src="{{asset('images/cat4.png')}}">
										<label for="checkboxes-0">
										Problemáticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="5" @if(is_array(old('categories')) && in_array(5,old('categories'))) checked @endif >
										<img src="{{asset('images/cat5.png')}}">
										<label for="checkboxes-0">
										Historia
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="6" @if(is_array(old('categories')) && in_array(6,old('categories'))) checked @endif >
										<img src="{{asset('images/cat6.png')}}">
										<label for="checkboxes-0">
										Herramientas y datos abiertos
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="7" @if(is_array(old('categories')) && in_array(7,old('categories'))) checked @endif >
										<img src="{{asset('images/cat7.png')}}">
										<label for="checkboxes-0">
										Proyectos regionales
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="8" @if(is_array(old('categories')) && in_array(8,old('categories'))) checked @endif >
										<img src="{{asset('images/cat8.png')}}">
										<label for="checkboxes-0">
										Regulación
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="9" @if(is_array(old('categories')) && in_array(9,old('categories'))) checked @endif >
										<img src="{{asset('images/cat9.png')}}">
										<label for="checkboxes-0">
										Comunicación y difusión
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="10" @if(is_array(old('categories')) && in_array(10,old('categories'))) checked @endif >
										<img src="{{asset('images/cat10.png')}}">
										<label for="checkboxes-0">							
										Educación para la sustentabilidad
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group" id="themes">
							<label for="name">Temas(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="1" @if(is_array(old('themes')) && in_array(1,old('themes'))) checked @endif >
										<img src="{{asset('images/21.png')}}">
										<label for="checkboxes-0">
										Humedales y espacios del agua
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="2" @if(is_array(old('themes')) && in_array(2,old('themes'))) checked @endif >
										<img src="{{asset('images/20.png')}}">
										<label for="checkboxes-0">
										Espacio público
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="3" @if(is_array(old('themes')) && in_array(3,old('themes'))) checked @endif >
										<img src="{{asset('images/19.png')}}">
										<label for="checkboxes-0">
										Biodiversidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="4" @if(is_array(old('themes')) && in_array(4,old('themes'))) checked @endif >
										<img src="{{asset('images/18.png')}}">
										<label for="checkboxes-0">
										Movilidad y accesibilidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="5" @if(is_array(old('themes')) && in_array(5,old('themes'))) checked @endif >
										<img src="{{asset('images/17.png')}}">
										<label for="checkboxes-0">
										Acción Colectiva
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="6" @if(is_array(old('themes')) && in_array(6,old('themes'))) checked @endif >
										<img src="{{asset('images/16.png')}}">
										<label for="checkboxes-0">
										Infraestructura verde
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="7" @if(is_array(old('themes')) && in_array(7,old('themes'))) checked @endif >
										<img src="{{asset('images/15.png')}}">
										<label for="checkboxes-0">
										Patrimonio natural y cultural
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="8" @if(is_array(old('themes')) && in_array(8,old('themes'))) checked @endif >
										<img src="{{asset('images/14.png')}}">
										<label for="checkboxes-0">
										Organizaciones sustentables
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="9" @if(is_array(old('themes')) && in_array(9,old('themes'))) checked @endif >
										<img src="{{asset('images/13.png')}}">
										<label for="checkboxes-0">
										Renovación urbana y ambiental
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="10" @if(is_array(old('themes')) && in_array(10,old('themes'))) checked @endif >
										<img src="{{asset('images/12.png')}}">
										<label for="checkboxes-0">
										Cambio climático
										</label>
									</div>
								</div>
							</div>
						</div>
					
					<div class="form-group">
							<label for="name">Incluir datos de mapas públicos contenidos en las categorías / temas seleccionados :</label>
							<select name="clone" class="form-control">
								<option value="0">NO</option>
								<option value="1">SI</option>
							</select>
					</div>

					<div class="form-group" id="submit-button">
						<button class="btn">
							Crear
						</button>
					</div>

					<!--</fieldset>-->

				</form>

		</div>

	</div>

</div>

<script src="{{ URL::asset('js/mapedit.js')}}"></script>

<script>

	$(document).ready(function() { 
		
		let lat  = '';
		let lng  = '';
		let zoom = '';
		let style = 'basic';
		
		mymap = MapMaker();

		if (($("#clatitude").val() == ''))
		{
			lat = -41.3214705;
		}
		else lat = $("#clatitude").val();

		if (($("#clongitude").val() == ''))
		{
			lng = -73.0139758;
		}
		else lng = $("#clongitude").val();

		if (($("#czoom").val() == ''))
		{
			zoom = 10;
		}
		else zoom = $("#czoom").val();

        style = $("#styleCmb").val();

		mymap.createOSMap(parseFloat(lng),parseFloat(lat),zoom,style);

		$("#styleCmb").change(function(val){

			mymap.setLayer($("#styleCmb").val());

		});

		$("#createForm").submit(function(){

			let coordinates = mymap.getCenter();
		
			$("#czoom").val(mymap.getZoom());

			$("#clongitude").val(coordinates[0]);
			$("#clatitude").val(coordinates[1]);  

			return true;
		});

	});
	
</script>

@endsection

