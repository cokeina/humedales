@extends('base')

@section('title', 'Mapas creados | Mapa interactivo de humedales urbanos')

@section('content')
		
			
	@include('shared.navbar-admin')	

<div class="wrapper-form" style="background:transparent;padding-top: 1em;">
	
	<div class="container">

		<div class="form-section initiatives-section">
		
		        @if (session('status'))
		           <div class="alert alert-success">
		                {{ session('status') }}
		            </div>
		        @endif

		        @foreach ($errors->all() as $error)
		            <div class="alert alert-danger">
		            	{{ $error }}
		        	</div>
		        @endforeach

			<h3 class="title" style="margin-bottom: 1em;padding-bottom: .5em;">Editar mapa</h3>

				<form method="post">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<div class="form-group">
							<label for="name">Nombre del mapa:</label>
							<input type="text" class="form-control" id="title" name="title" value="{{ $map->name }}">
						</div>

						<!--<p>Los valores de personalización pueden quedar en blanco, de ser así, usará valores por defecto</p>
						<div class="form-group">
							<label for="name">¿Donde debe estar centrado?(*):</label>
							<div class="row">
								<div class="col-6">
									<input type="number" class="form-control" id="latitude" name="latitude" value="{{ old('latitude') }}" placeholder="latitude" step="0.00000000001">
								</div>
								<div class="col-6">
									<input type="number" class="form-control" id="longitude" name="longitude" value="{{ old('longitude') }}" placeholder="longitude" step="0.00000000001">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-5">
									<label for="style">¿Qué estilo debe tener?</label>
								</div>
								<div class="col-7">
									<select class="form-control" name="style">
									  <option value="basic">Básico</option>
									  <option value="collinsBart">Básico 2</option>
									  <option value="AerialWithLabels">Satelital</option>
									  <option value="RoadOnDemand">Relieves</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-5">
									<label for="style">¿Qué zoom debe tener?</label>
								</div>
								<div class="col-7">
									<select class="form-control" name="zoom">
									  <option value="1">1</option>
									  <option value="2">2</option>
									  <option value="3">3</option>
									  <option value="4">4</option>
									  <option value="5">5</option>
									  <option value="6">6</option>
									  <option value="7">7</option>
									  <option value="8">8</option>
									  <option value="9">9</option>
									  <option value="10">10</option>
									  <option value="11">11</option>
									  <option value="12">12</option>
									  <option value="13">13</option>
									  <option value="14">14</option>
									  <option value="15">15</option>
									  <option value="16">16</option>
									  <option value="17">17</option>
									  <option value="18">18</option>
									  <option value="19">19</option>
									</select>
								</div>
							</div>
						</div>-->

						<div class="form-group">
							<label for="name">Categorías:</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="1"
											@if (in_array(1, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat1.png')}}">
										<label for="checkboxes-0">
										Naturaleza urbana
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="2"
											@if (in_array(2, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat2.png')}}">
										<label for="checkboxes-0">
										Iniciativas y buenas prácticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="3"
											@if (in_array(3, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat3.png')}}">
										<label for="checkboxes-0">
										Comunidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="4"
											@if (in_array(4, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat4.png')}}">
										<label for="checkboxes-0">
										Problemáticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="5"
											@if (in_array(5, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat5.png')}}">
										<label for="checkboxes-0">
										Historia
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="6"
											@if (in_array(6, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat6.png')}}">
										<label for="checkboxes-0">
										Herramientas y datos abiertos
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="7"
											@if (in_array(7, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat7.png')}}">
										<label for="checkboxes-0">
										Proyectos regionales
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="8"
											@if (in_array(8, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat8.png')}}">
										<label for="checkboxes-0">
										Regulación
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="9"
											@if (in_array(9, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat9.png')}}">
										<label for="checkboxes-0">
										Comunicación y difusión
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="10"
											@if (in_array(10, $category))
											checked
											@endif 
										>
										<img src="{{asset('images/cat10.png')}}">
										<label for="checkboxes-0">							
										Educación para la sustentabilidad
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group" id="themes">
							<label for="name">Temas:</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="1"
											@if (in_array(1, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/21.png')}}">
										<label for="checkboxes-0">
										Humedales y espacios del agua
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="2"
											@if (in_array(2, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/20.png')}}">
										<label for="checkboxes-0">
										Espacio público
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="3"
											@if (in_array(3, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/19.png')}}">
										<label for="checkboxes-0">
										Biodiversidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="4"
											@if (in_array(4, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/18.png')}}">
										<label for="checkboxes-0">
										Movilidad y accesibilidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="5"
											@if (in_array(5, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/17.png')}}">
										<label for="checkboxes-0">
										Acción Colectiva
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="6"
											@if (in_array(6, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/16.png')}}">
										<label for="checkboxes-0">
										Infraestructura verde
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="7"
											@if (in_array(7, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/15.png')}}">
										<label for="checkboxes-0">
										Patrimonio natural y cultural
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="8"
											@if (in_array(8, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/14.png')}}">
										<label for="checkboxes-0">
										Organizaciones sustentables
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="9"
											@if (in_array(9, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/13.png')}}">
										<label for="checkboxes-0">
										Renovación urbana y ambiental
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="10"
											@if (in_array(10, $theme))
											checked
											@endif 
										>
										<img src="{{asset('images/12.png')}}">
										<label for="checkboxes-0">
										Cambio climático
										</label>
									</div>
								</div>
							</div>
						</div>

					<div class="form-group" id="submit-button">
						<button class="btn">
							Editar
						</button>
					</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

		

@endsection