@extends('base')

@section('title', 'Mapas creados | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container admin">
			
			@include('shared.navbar-admin')	

			<div class="row my-maps">
				<h3 class="title" style="margin-bottom: 1em;padding-bottom: .5em;">Mapas creados</h3>

		        @if (session('status'))
		           <div class="col-12 alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
		                {{ session('status') }}
		            </div>
		        @endif

		        @foreach ($errors->all() as $error)
		            <div class="col-12 alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
						</button>
		            	{{ $error }}
		        	</div>
		        @endforeach
		        		<div class="col-4">
							<p><strong>Nombre:</strong> Mapa Público</p>
						</div>
						<div class="col-8" style="text-align: right">
							<!--<a href="/admin/my-maps/personalize">Personalizar <i class="fa fa-pencil" aria-hidden="true"></i></a>-->
							<button class="btn "><a href="/admin/public-map">Ver mapa <i class="fa fa-map" aria-hidden="true"></i></a></button>
						</div>

					@foreach ($maps as $map)
						<div class="col-4">
							<p><strong>Nombre:</strong> {{ $map->name }}</p>
						</div>
						<div class="col-4">
							<p><strong>Fecha creación:</strong> {!! $map->created_at->format('d-m-Y') !!} </p>
						</div>
						<div class="col-2" style="text-align: center"><a href="{!! action('Admin\AdminController@privateMap', $map->id) !!}">Ver mapa</a></div>
						<div class="col-1" style="text-align: center">
							<a href="{!! action('Admin\AdminController@editMap', $map->id) !!}" data-toggle="tooltip" title="Editar mapa"><i class="fa fa-pencil-square" style="color:green" aria-hidden="true"></i></a>
						</div>
						<div class="col-1" style="text-align: right">
							<a href="{!! action('Admin\AdminController@destroyMap', $map->id) !!}"
							data-toggle="tooltip" title="Eliminar mapa"><i class="fa fa-window-close" style="color:red" aria-hidden="true"></i></a>
						</div>
					@endforeach
			</div>

			<div class="row create">
				<div class="col-12">
					<a href="/admin/maps/create">
						<button class="btn">Crear mapa</button>
					</a>
				</div>
			</div>

		</div>

@endsection