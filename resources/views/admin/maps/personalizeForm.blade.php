@extends('base')

@section('title', 'Personalizar mapa público')

@section('content')

<style>

.ol-control button{
	width:1.375em !important;
	background-color: rgba(0,60,136,.5) !important;
}
</style>

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Personalizar Mapa Público</p>

				<form method="post" id="editForm">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<input type="hidden" id="clatitude" name="latitude"  value="{{ old('latitude') }}">
						<input type="hidden" id="clongitude" name="longitude" value="{{ old('longitude') }}">
						<input type="hidden" id="czoom" name="zoom"      value="{{ old('zoom') }}">

						<div class="form-group">
							<label for="name">Mapa Base:</label>
							<select class="form-control" id="styleCmb" name="style" >
									  <option value="basic" @if(old('style') == "basic") selected @endif>Básico</option>
									  <option value="AerialWithLabels" @if(old('style') == "AerialWithLabels") selected @endif>Satelital</option>
									  <option value="RoadOnDemand" @if(old('style') == "RoadOnDemand") selected @endif>Relieves</option>
									</select>
						</div>

						<div id="map" class="map" style="height:300px"></div>

						<div class="form-group" style="margin-top:10px">
							<label for="name">Categorías:</label>
							<div class="row">
								@foreach ($categories as $key => $value)
									<div class="col-6">
										<div class="checkbox">
											<input type="checkbox" name="categories[]" id="categories" value="{{ $key }}"
												@if (array_key_exists ($key, $mcps))
												checked
												@endif 
											>
											<img src="{{asset('images')}}/cat{{ $key }}.png">
											<label for="checkboxes-0">
											{{ $value }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>

						<div class="form-group" style="margin-top:10px">
							<label for="name">Temas:</label>
							<div class="row">
								@foreach ($themes as $key => $value)
									<div class="col-6">
									    <input type="checkbox" name="themes[]" id="themes" value="{{ $key }}"
												@if (array_key_exists ($key, $mtps))
												checked
												@endif 
										>
										<img width="20px" src="{{asset('images/icons-map/theme/'.$key.'.png')}}">
										<label for="checkboxes-0">
										{{ $value }}
										</label>
									</div>	
								@endforeach
							</div>
						</div>

						

					<div class="form-group" style="margin-top:10px" id="submit-button">
						<button class="btn">
							Guardar preferencias
						</button>
						<button class="btn" type="button" id="goBack" style="margin-top:10px;background-color:blue">
							Volver
						</button>
					</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

<script src="{{ URL::asset('js/mapedit.js')}}"></script>

<script>

	$(document).ready(function() { 
		
		let lat  = '{{ $pref->latitude }}';
		let lng  = '{{ $pref->longitude }}';
		let zoom = '{{ $pref->zoom }}';
		let style = '{{ $pref->map_style }}';

		$("#styleCmb").val(style);
		
		mymap = MapMaker();

		mymap.createOSMap(parseFloat(lng),parseFloat(lat),zoom,style);

		$("#styleCmb").change(function(val){

			mymap.setLayer($("#styleCmb").val());

		});

		$("#editForm").submit(function(){

			let coordinates = mymap.getCenter();
		
			$("#czoom").val(mymap.getZoom());

			$("#clongitude").val(coordinates[0]);
			$("#clatitude").val(coordinates[1]);  

			return true;
		});

		$("#goBack").click(function(){
			
			location.href = '{{ url("admin/public-map") }}';

		});

	});
	
</script>

@endsection