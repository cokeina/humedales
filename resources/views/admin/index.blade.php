@extends('base')

@section('title', 'Mi perfil | Mapa interactivo de humedales urbanos')

@section('content')
		
		<div class="container">
			
			@include('shared.navbar-admin')	

			<div class="row profile">
				<div class="col-4 avatar-image">
					<img src="{{asset('images/avatar.png')}}">
				</div>
				<div class="col-8">
				<form>
					<div class="form-group row">
						<label for="inputEmail3" class="col-sm-2 col-form-label">Usuario</label>
						<div class="col-sm-10">
						<input type="text" disabled class="form-control" value="{{ Auth::user()->name }}" id="inputEmail3" placeholder="Nombre">
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" value="{{ Auth::user()->email }}" id="inputPassword3" disabled placeholder="Password">
						</div>
					</div>
					</form>
				</div>
				<div class="col-2"></div>
				<div class="col-2"></div>
			</div>

		</div>

@endsection