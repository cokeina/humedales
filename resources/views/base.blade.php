<html>
<head>
	<title> @yield('title') </title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Bootstrap styles -->
	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">

	<script src="https://use.fontawesome.com/e8dc2759e3.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<!-- Custom styles -->
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

	<link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet">

	<link href="{{ asset('/css/jquery.typeahead.css') }}" rel="stylesheet">
	<!-- Map styles -->
	<link rel="stylesheet" href="{{ asset('/css/ol.css') }}" type="text/css">
	@yield('css')	

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="{{asset('/js/ol.js')}}" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>

</head>
<body>
	
	@include('shared.navbar-user')

	@yield('content')

	@include('shared.footer')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/amount.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/reward.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/sweetalert2.js')}}" type="text/javascript"></script>
	<script> 
	   var baseUrl = '{{ url('/')}}';
	</script>
	@yield('js')
</body>
</html>