@extends('base')
@section('title', 'Mapa interactivo de humedales urbanos')


@section('content')
  <div class="row info">
		<div class="container">
			<div class="col-12">
				<img src="{{asset('images/infografia1.png')}}" width="100%">
			</div>			
		</div>
	</div>
	<div class="row no-gutters">
		<div class="col-10" id="map-container">
			<div id="map" class="map"><div id="popup"></div></div>

      <div class="typeahead__container" style="width:250px!important;position: absolute; top: 10px; right: 10px; padding: 3px;background:rgba(255,255,255,0.4);border-radius:5px;">
          <div class="typeahead__field">

          <span class="typeahead__query">
              <input class="js-typeahead-input"
                         name="q"
                         type="search"
                         autocomplete="off"/>
          </span>
              <span class="typeahead__button">
              <button onclick="search()">
                  <span class="typeahead__search-icon"></span>
              </button>
          </span>

          </div>
      </div>

      <div class="geolocation__container" style="position: absolute; left: 10px; bottom: 10px;background:rgba(255,255,255,0.4);padding: 3px;">
        <button onclick="setUserPosition()" style="padding: 6px;background:rgba(0,60,136,.5); border:none; border-radius: 2px;">
            <i class="fa fa-crosshairs" aria-hidden="true" style="color:white"></i>
        </button>
      </div>

		</div>
		<div class="col-2" id="cat-themes">

			<p class="title">Categorías</p>
			@foreach ($categories as $category)
				<div class="checkbox">
					<input type="checkbox" checked id="{{ str_replace(' ','_',$category->category_name) }}" onclick="changeMarker('{{ str_replace(' ','_',$category->category_name) }}', 'category', {{ $category->id }})">
		  			<img src="{{asset('images/icons-map/category/'.$category->id.'.png')}}">
		  			<label for="checkboxes-0">{{ $category->category_name }}</label>
				</div>
			@endforeach


			<div class="divider"></div>
	
			<p class="title">Temas</p>

			@foreach ($themes as $theme)
				<div class="checkbox" id="theme">
					<input type="checkbox" id="{{ str_replace(' ','_',$theme->themes_name) }}" onclick="changeMarker('{{ str_replace(' ','_',$theme->themes_name) }}','theme', {{ $theme->id }})" checked>
					<img src="{{asset('images/icons-map/theme/'.$theme->id.'.png')}}">
					<label for="checkboxes-0">{{ $theme->themes_name }}</label>
				</div>
			@endforeach
		</div>
	</div>

  <script src="{{ URL::asset('js/map.js')}}"></script>
  <script src="{{ URL::asset('js/initMap.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.typeahead.js') }}"></script>
  <script>
    var names = {!! json_encode($names) !!};
    var points = {!! json_encode($points) !!};
    $('.js-typeahead-input').typeahead({
        minLength: 1,
        order: "asc",
        group: false,
        maxItemPerGroup: 3,
        hint: true,
        source: {
            data: names
        },
        debug: false
    });

    function search() {
      var search = $('.js-typeahead-input').val();
      var finded = names.indexOf(search);
      if (finded > -1) {
        lonlat = points[finded];
        mymap.setCenter(lonlat, 15);
        $('.js-typeahead-input').val('');
        $('.typeahead__container').removeClass('cancel');
      }
    }

    function setUserPosition() {
      if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition)
      }
    }

    function showPosition(position) {
      var lonlat = [position.coords.latitude, position.coords.longitude];
      mymap.setCenter(lonlat, 15);
    }
  </script>

	

	


@endsection