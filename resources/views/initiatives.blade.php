@extends('base')
@section('title', 'Iniciativas | Mapa interactivo de humedales urbanos')


@section('content')
	<div class="container" id="initiatives-all">
		<div class="row header d-flex justify-content-between">
			<h2>Iniciativas</h2>

			<div class="right-content">
				<a href="#"><button class="btn btn-prmary">Crear iniciativa</button></a>
				<i class="fa fa-search" aria-hidden="true"></i>
			</div>
		</div>

		<div class="row">
			<div class="col-12" id="info-img">
				<img src="{{asset('images/infog2.png')}}">
			</div>
		</div>


		<h4>Iniciativas publicadas</h4>


		<div id="published-initiatives">


			@if (count($initiatives) != 0)
				
				@foreach ($initiatives as $initiative) 

				<div class="initiative">
					
					<img src="data:{{ $initiative->image_type }};charset=utf-8;base64,{{ $initiative->image }}">
					
					<p class="title">{{ $initiative->initiative_title }}</p>
					<p class="subtitle">Por: {{ $initiative->initiative_name }}</p>

					<div class="amounts">
						<p>$0</p>
						<p id="goal">${{ $initiative->goal }}</p>
					</div>
					<div class="progress">
					  <div class="progress-bar bg-success" role="progressbar" style="width: {{  $initiative->actual }};" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ $initiative->actual }}%</div>
					</div>

					<a href="initiatives/detail/{{ $initiative->id }}">
						<button  class="btn btn-primary">Ver iniciativa</button>
					</a>

				</div>

				@endforeach

			@else

			<div class="row" id="no-initiatives">
				<div class="col-12">
					<p>¡Aún no existen iniciativas creadas, crea una!</p>
					<a href="/initiatives/create/first-step"><button class="btn btn-primary">Crear iniciativa</button></a>
				</div>
			</div>

			@endif


		</div>



	</div>


@endsection
