@extends('base')
@section('title', 'Mapa interactivo de humedales urbanos')


@section('content')

	<div class="main-section">
		
		<div class="row">
		
			<div class="col-lg-6 first-section">

				<div class="container">
					<h3>Mapa de Humedales para Ciudades Sustentables</h3>
					<div class="row" style="margin-bottom: 20px;">

						<div class="col-sm-6 media">
							<img class="d-flex align-self-start mr-3" src="{{asset('images/59.png')}}">
							<div class="media-body">
								<h5 class="mt-0">Genera y comparte datos abiertos</h5>
								<p>Identifica aspectos de interés para los humedales urbanos, y compártelos con otros usuarios</p>
							</div>
						</div>

						<div class="col-sm-6 media">
							<img class="d-flex align-self-start mr-3" src="{{asset('images/57.png')}}">
							<div class="media-body">
								<h5 class="mt-0">Utiliza datos abiertos</h5>
								<p>Difunde y ayuda a la aplicación con datos, en contextos y temas de interés.</p>
							</div>
						</div>

					</div>

					<div class="row" style="margin-bottom: 20px;">

						<div class="col-sm-6 media">
							<img class="d-flex align-self-start mr-3" src="{{asset('images/58.png')}}">
							<div class="media-body">
								<h5 class="mt-0">Aprende sobre los humedales y la naturaleza urbana</h5>
								<p>Co-construye conocimiento colectivo, mediante un mapeo colaborativo</p>
							</div>
						</div>

						<div class="col-sm-6 media">
							<img class="d-flex align-self-start mr-3" src="{{asset('images/56.png')}}">
							<div class="media-body">
								<h5 class="mt-0">Apoya o genera iniciativas colaborativas</h5>
								<p>Encuentra y apoya una iniciativa de interés, o inicia tu propia acción colectiva.</p>
							</div>
						</div>

					</div>
				</div>

			</div>
			<div class="col-lg-6" id="map-image">
				@if (Auth::check())
					@if (Auth::user()->roles_id ==1)
					<a  href="/admin/public-map">
					@endif
					@if (Auth::user()->roles_id ==2)
					<a  href="/curador/public-map">
					@endif
					@if (Auth::user()->roles_id ==3)
					<a  href="/general/public-map">
					@endif   
				@else
				<a href="/map">  
				@endif
				<img src="{{asset('storage/contents/'.$contents['start_image_right']).'?'.time()  }}">
				</a>
			</div> 
		</div>


	</div>

	<div class="second-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<img src="{{asset('storage/contents/'.$contents['start_secundary_image_1']).'?'.time()  }}">
					{!! $contents['start_secundary_text_1'] !!}
				</div>
				<div class="col-sm-6">
				    <img src="{{asset('storage/contents/'.$contents['start_secundary_image_2']).'?'.time() }}">
					{!! $contents['start_secundary_text_2'] !!}
				</div>
			</div>
		</div>
	</div>

	<div class="third-section">
		<div class="container">
			<div class="row">

				<div class="col-3"></div>

				<div class="col-3">
					<img src="{{asset('images/46.png')}}">
					<p class="number">{{ $data }}</p>
					<p class="title">Datos</p>
					<p class="title">Publicados</p>
				</div>
				<!--<div class="col-3">
					<img src="{{asset('images/45.png')}}">
					<p class="number">0</p>
					<p class="title">Ciudades</p>
					<p class="title">Agregadas</p>
				</div>-->
				<div class="col-3">
					<img src="{{asset('images/visitas.png')}}" style="width:64px">
					<p class="number">{{ $visits  }}</p>
					<p class="title">Total</p>
					<p class="title">Visitas</p>
				</div>
				<div class="col-3"></div>
				<!--<div class="col-3">
					<img src="{{asset('images/43.png')}}">
					<p class="number">{{ $themes  }}</p>
					<p class="title">Temas</p>
					<p class="title">Agregados</p>
				</div>-->
			</div>
		</div>
	</div>

	<div class="four-section">
		<div class="container">
			<h4>¿Quieres colaborar o implementar un proyecto en otra región?</h4>
			<a href="/contact"> <button class="btn" style="margin-top: 1em">CONTÁCTANOS</button></a>
		</div>
	</div>

	<div class="five-section">
		<div class="container">
			<h4>Organismos asociados</h4>
			<img src="{{asset('images/logos.jpg')}}">
		</div>
	</div>

@endsection