<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar datos a mapa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>


      <div class="modal-body">

        @if(Auth::user()->roles_id == 3)
          <p><strong>Este marcador será validado por un curador antes de ser publicado</strong></p>
        @endif

        <p class="subtitle-modal">Se agregará un marcador sobre el mapa, en el lugar seleccionado</p>

        <form>
          
          <fieldset>
            
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">

            <div class="form-group">
              <label for="name" style="font-size: 15px">Título (*):</label>
              <input type="text" class="form-control" id="title" name="title" onclick="clearFeedback('feedback-info-title')">
              <div class="feedback-info-title"></div>
            </div>

            <div class="form-group">
              <label for="name" style="font-size: 15px">Descripción:</label>
              <textarea class="form-control" id="description" name="description" onclick="clearFeedback('feedback-info-description')"></textarea>
              <div class="feedback-info-description"></div>
            </div>

            <div class="form-group">
              <label for="name" style="font-size: 15px">Categorías(*):</label>
              @foreach ($categories as $category)
                <div class="radio">
                  <input type="radio" name="categories[]" id="{{ $category->category_name }}" value="{{ $category->id }}" onclick="clearFeedback('feedback-info-category')">
                  <img src="{{asset('images/icons-map/category/'.$category->id.'.png')}}">
                  <label for="checkboxes-0">{{ $category->category_name }}</label>
                </div>
              @endforeach
              <div class="feedback-info-category"></div>
            </div>

            <div class="form-group">
              <label for="name" style="font-size: 15px">Temas(*):</label>
              @foreach ($themes as $theme)
              <div class="radio" id="theme">
                <input type="radio" name="themes[]" id="{{ $theme->theme_name }}" value="{{ $theme->id }}" onclick="clearFeedback('feedback-info-theme')">
                <img src="{{asset('images/icons-map/theme/'.$theme->id.'.png')}}">
                <label for="checkboxes-0">{{ $theme->themes_name }}</label>
              </div>
              @endforeach
              <div class="feedback-info-theme"></div>
            </div>

            <div class="divider"></div>

            <div class="form-group">
                
                <div class="row">
                  
                  <div class="col-6">
                    <label for="photos">Subir fotos (opcional)</label>
                  </div>
                  <div class="col-6" id="button-display">
                    <div class="btn btn-danger" onclick="removePhoto()">-</div>
                    <div class="btn btn-primary" onclick="addPhoto()">+</div>
                  </div>

                </div>

              <div class="input-group" id="photos">
                <input type="file" id="photos0" class="input-file" name="file_photos[]" onclick="clearFeedback('feedback-info-photo')">
                <input type="hidden" id="file_name_photos0" name="filenames_photos[]">
                <input type="hidden" id="file_type_photos0" name="filetypes_photos[]">
                <input type="hidden" id="file_btoa_photos0" name="filebs_photos[]">      
              </div>

              <div id="added-photos"></div>  

              <div class="feedback-info-photo"></div>

            </div>


          </fieldset>

        </form>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Descartar</button>
        <button type="button" class="btn btn-primary save-data">Guardar</button>
        <button type="button" class="btn btn-success publish-data">Guardar y Publicar</button>
      </div>

    </div>
  </div>
</div>


<script>
  function clearFeedback(classSelected) {
    $('.'+classSelected).empty();
  }

  $('.save-data').click(function() {
    var title = $('#title').val();
    var description = $('#description').val();
    var groupCategories = $('input[name="categories[]"]');
    var groupThemes = $('input[name="themes[]"]');
    var array_photo = $('input[name="filebs_photos[]"]');
    var array_photo_ext = $('input[name="filetypes_photos[]"]');

    if (title.length == 0) {
      $('.feedback-info-title').append('<p style="font-size: .8em; color: red">El título es obligatorio</p>');
    }

    /*if (description.length == 0) {
      $('.feedback-info-description').append('<p style="font-size: .8em; color: red">La descripción es obligatoria</p>');
    }*/

    var count = 0;
    categories = [];
    groupCategories.each(function (){
        var checked = $(this).is(':checked');
        if ( checked ) {
          categories[count] = $(this).attr('value');
          count += 1;
        }
    });

    if (count == 0 || count > 1) {
      $('.feedback-info-category').append('<p style="font-size: .8em; color: red">Debes seleccionar una categoría</p>');
    }

    count = 0;
    themes = [];
    groupThemes.each(function (){
        var checked = $(this).is(':checked');
        if ( checked ) {
          themes[count] = $(this).attr('value');
          count += 1;
        }
    });

    if (count == 0 || count > 1) {
      $('.feedback-info-theme').append('<p style="font-size: .8em; color: red">Debes seleccionar un tema</p>');
    }

    /*count      = 0;
    var photos     = [];
    var photos_ext = [];
    array_photo.each(function(){
      var file_data = $(this).attr('value');
      if (file_data != undefined) {
        photos[count] = $(this).attr('value');
        count++;
      } 
    });

    count = 0;
    array_photo_ext.each(function(){
      var file_ext = $(this).attr('value');
      if (file_ext != undefined) {
        photos_ext[count] = $(this).attr('value');
        count++;
      } 
    });*/
    


    data = {
      "_token": "{{ csrf_token() }}",
      'title': title,
      'description': description,
      'categories': categories,
      'themes': themes,
      //'image': photos,
      //'image_ext': photos_ext,
      'type': localStorage.getItem('type')
    }

    var formData = new FormData();

    if (data.type == 'point') {
      data.latitude = localStorage.getItem('lat');
      data.longitude = localStorage.getItem('long');

      formData.append("latitude",data.latitude);
      formData.append("longitude",data.longitude);
    } else {
      data.coordinates = localStorage.getItem('coordinates').split(",");
      formData.append("coordinates",data.coordinates);
    }

    
    formData.append("_token",data._token);
    formData.append("title",data.title);
    formData.append("description",data.description);
    formData.append("categories",data.categories);
    formData.append("themes",data.themes);
    formData.append("type",data.type);
    formData.append("status",0);
    
    $("input[type='file']").map(function(file){
        
        if($("input[type='file']")[file].files[0] != undefined)
        {
           formData.append("file[]",$("input[type='file']")[file].files[0]);
        }
        
    });

    /*$.each($("input[type='file']")[0].files, function(i, file) {
      
    });*/

    
    formData.append('id','{{ $map->id }}');

    

    var getUrl = window.location;

    var query = getUrl.pathname;

    
    if(query.includes("admin/private-map"))
    {
      query = "/admin/private-map/add-data";
    }

    if(query.includes("admin/public-map"))
    {
      query = "/admin/public-map/add-data";
    }

    if(query.includes("general/public-map"))
    {
      query = "/general/public-map/add-data";
    }

    if(query.includes("general/private-map"))
    {
      query = "/general/private-map/add-data";
    }

    if(query.includes("curador/private-map"))
    {
      query = "/curador/private-map/add-data";
    }

    if(query.includes("curador/public-map"))
    {
      query = "/curador/public-map/add-data";
    }

    $(".feedback-info-photo").empty();
    $('.feedback-info-theme').empty();
    $('.feedback-info-category').empty();
    $('.feedback-info-title').empty();
    
    $.ajax({
        url:query,
        data:formData,
        cache: false,
        processData: false,
        contentType: false,
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success:function(data){
          
            if(data.errors == null)
            {
              if (data.type == 'point')
              {
                 mymap.addMarker(data.points[0].longitude, data.points[0].latitude, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }
              if (data.type == 'polygon')
              {
                mymap.addPolygon(data.points, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }
              if (data.type == 'lines')
              {
                mymap.addLine(data.points, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }

              //$(':input').val('');
              //$('input:checkbox').removeAttr('checked');

              // Cierra modal 
              swal(
                'Dato ingresado',
                'Este dato quedará guardado en Mi Perfil/ Datos. Podrás editarlo y publicarlo cuando quieras',
                'info'
              );
              $('#exampleModal').modal('hide');
            }
            else
            {
              for (x in data.errors)
              {
                for (y in data.errors[x])
                {
                    $(".feedback-info-photo").append('<p style="font-size: .8em; color: red">'+ data.errors[x][y]+'</p>');
                }
              }
            }

        }
    });



  });

  $('.publish-data').click(function() {
    var title = $('#title').val();
    var description = $('#description').val();
    var groupCategories = $('input[name="categories[]"]');
    var groupThemes = $('input[name="themes[]"]');
    var array_photo = $('input[name="filebs_photos[]"]');
    var array_photo_ext = $('input[name="filetypes_photos[]"]');

    if (title.length == 0) {
      $('.feedback-info-title').append('<p style="font-size: .8em; color: red">El título es obligatorio</p>');
    }

    /*if (description.length == 0) {
      $('.feedback-info-description').append('<p style="font-size: .8em; color: red">La descripción es obligatoria</p>');
    }*/

    var count = 0;
    categories = [];
    groupCategories.each(function (){
        var checked = $(this).is(':checked');
        if ( checked ) {
          categories[count] = $(this).attr('value');
          count += 1;
        }
    });

    if (count == 0 || count > 1) {
      $('.feedback-info-category').append('<p style="font-size: .8em; color: red">Debes seleccionar una categoría</p>');
    }

    count = 0;
    themes = [];
    groupThemes.each(function (){
        var checked = $(this).is(':checked');
        if ( checked ) {
          themes[count] = $(this).attr('value');
          count += 1;
        }
    });

    if (count == 0 || count > 1) {
      $('.feedback-info-theme').append('<p style="font-size: .8em; color: red">Debes seleccionar un tema</p>');
    }

    /*count      = 0;
    var photos     = [];
    var photos_ext = [];
    array_photo.each(function(){
      var file_data = $(this).attr('value');
      if (file_data != undefined) {
        photos[count] = $(this).attr('value');
        count++;
      } 
    });

    count = 0;
    array_photo_ext.each(function(){
      var file_ext = $(this).attr('value');
      if (file_ext != undefined) {
        photos_ext[count] = $(this).attr('value');
        count++;
      } 
    });*/
    


    data = {
      "_token": "{{ csrf_token() }}",
      'title': title,
      'description': description,
      'categories': categories,
      'themes': themes,
      //'image': photos,
      //'image_ext': photos_ext,
      'type': localStorage.getItem('type')
    }

    var formData = new FormData();

    if (data.type == 'point') {
      data.latitude = localStorage.getItem('lat');
      data.longitude = localStorage.getItem('long');

      formData.append("latitude",data.latitude);
      formData.append("longitude",data.longitude);
    } else {
      data.coordinates = localStorage.getItem('coordinates').split(",");
      formData.append("coordinates",data.coordinates);
    }

    
    formData.append("_token",data._token);
    formData.append("title",data.title);
    formData.append("description",data.description);
    formData.append("categories",data.categories);
    formData.append("themes",data.themes);
    formData.append("type",data.type);
    formData.append("status",2);
    
    $("input[type='file']").map(function(file){
        
        if($("input[type='file']")[file].files[0] != undefined)
        {
           formData.append("file[]",$("input[type='file']")[file].files[0]);
        }
        
    });

    /*$.each($("input[type='file']")[0].files, function(i, file) {
      
    });*/

    
    formData.append('id','{{ $map->id }}');

    

    var getUrl = window.location;

    var query = getUrl.pathname;

    
    if(query.includes("admin/private-map"))
    {
      query = "/admin/private-map/add-data";
    }

    if(query.includes("admin/public-map"))
    {
      query = "/admin/public-map/add-data";
    }

    if(query.includes("general/public-map"))
    {
      query = "/general/public-map/add-data";
    }

    if(query.includes("general/private-map"))
    {
      query = "/general/private-map/add-data";
    }

    if(query.includes("curador/private-map"))
    {
      query = "/curador/private-map/add-data";
    }

    if(query.includes("curador/public-map"))
    {
      query = "/curador/public-map/add-data";
    }

    $(".feedback-info-photo").empty();
    $('.feedback-info-theme').empty();
    $('.feedback-info-category').empty();
    $('.feedback-info-title').empty();
    
    $.ajax({
        url:query,
        data:formData,
        cache: false,
        processData: false,
        contentType: false,
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success:function(data){
          
            if(data.errors == null)
            {
              if (data.type == 'point')
              {
                 mymap.addMarker(data.points[0].longitude, data.points[0].latitude, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }
              else if(data.type == 'polygon') 
              {
                mymap.addPolygon(data.points, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }
              else
              {
                mymap.addLine(data.points, data.category_id, data.theme_id, data.id, data.title, data.description, data.images);
              }

              //$(':input').val('');
              //$('input:checkbox').removeAttr('checked');
              $('#exampleModal').modal('hide');

              if(getUrl.pathname.includes("admin"))
              {
                swal(
                'Dato ingresado',
                'El dato ha sido publicado de forma exitosa',
                'info'
                );
              }
              else
              {
                
                if(getUrl.pathname.includes("public-map"))
                {
                  swal(
                  'Dato ingresado',
                  'Este dato será enviado a validación para ser publicado. Ya no podrás editarlo',
                  'info'
                  );

                }
                else
                {
                  //si es mapa privado y no es administrador, 
                  //si es administrador de mapa debería ver mensaje de publicado
                  if(data.isAdmin)
                  {
                    swal(
                    'Dato ingresado',
                    'El dato ha sido publicado de forma exitosa',
                    'info'
                    );
                  } 
                  else
                  {
                    swal(
                    'Dato ingresado',
                    'Este dato será enviado a validación para ser publicado. Ya no podrás editarlo',
                    'info'
                    );
                  }

                }
                
                
              }

              

              // Cierra modal 
              
            }
            else
            {
              for (x in data.errors)
              {
                for (y in data.errors[x])
                {
                    $(".feedback-info-photo").append('<p style="font-size: .8em; color: red">'+ data.errors[x][y]+'</p>');
                }
              }
            }

        }
    });



  });

</script>