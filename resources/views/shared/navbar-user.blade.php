<div class="sesion-section">
    
  <div class="container">
      
    <div class="row">
      
        <div class="col-md-6 col-sm-4"></div>
        <div class="col-md-6 col-sm-8 sesion-menu">
          <ul class="float-right">
            @if (Auth::check())
            <li class="item-element"><a href="/redirect">Mi perfil</a></li>
            <li class="item-element"><a href="/logout">Cerrar Sesión</a></li>
            @else
            <li class="item-element"><a href="/users/register">Registrarse</a></li>
            <li class="item-element"><a href="/login">Iniciar sesión</a></li>
            @endif
            <!--<li class="item-element"><a href="#">EN</a></li>
            <li class="item-element"><a href="#">ES</a></li>-->
          </ul>
        </div>

    </div>

  </div>

</div>

<nav class="navbar navbar-expand-lg">

  <div class="container">
      <a class="navbar-brand" href="#"><img src="{{asset('images/logo-rounded.png')}}">
      <label style="color:black;font-weight: bold">BETA</label>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
      </button>

      <div class="collapse navbar-collapse justify-content-end" id="navbar">
        <div class="navbar-nav">
          <a class="nav-item nav-link" href="/">
            <img src="{{asset('images/home.png')}}">
            Inicio
          </a>
          <a class="nav-item nav-link" href="/what-is">
            <img src="{{asset('images/53.png')}}">
            ¿Qué es?
          </a>
          @if (Auth::check())
              @if (Auth::user()->roles_id ==1)
              <a class="nav-item nav-link" href="/admin/public-map">
              @endif
              @if (Auth::user()->roles_id ==2)
              <a class="nav-item nav-link" href="/curador/public-map">
              @endif
              @if (Auth::user()->roles_id ==3)
              <a class="nav-item nav-link" href="/general/public-map">
              @endif   
          @else
           <a class="nav-item nav-link" href="/map">  
          @endif
            <img src="{{asset('images/55.png')}}">
            Mapa
          </a>
          <!--<a class="nav-item nav-link" href="/initiatives">
            <img src="{{asset('images/54.png')}}">
            Iniciativas
          </a>-->
          <a class="nav-item nav-link" href="/downloads">
            <img src="{{asset('images/52.png')}}">
            Descargas
          </a>
          <a class="nav-item nav-link" href="/contact">
            <img src="{{asset('images/51.png')}}">
            Contacto
          </a>
          <!--<a href="#" class="nav-item nav-link" id="last-child"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
          <a href="https://www.facebook.com/patagua.cl" target="_blank"  class="nav-item nav-link" id="last-child"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </div>
      </div>

  </div>

</nav>
