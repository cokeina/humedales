      <div class="row profile-menu">
        <div class="col-2 sesion-menu">
              <ul style="padding-left: 0">
                <li class="item-element"><a>Administrador</a></li>
              </ul>
        </div>
        <div class="col-10 sesion-menu">
              <ul class="float-right">
                <li class="item-element"><a href="/admin">Perfil</a></li>
                <li class="item-element"><a href="/admin/users">Usuarios</a></li>
                <li class="item-element"><a href="/admin/contents">Contenidos</a></li>
                <li class="item-element"><a href="/admin/maps">Mapas</a></li>
                <li class="item-element"><a href="/admin/data">Datos</a></li>
                <li class="item-element"><a href="/admin/downloads">Gestor de Descargas</a></li>
                <!--<li class="item-element"><a href="/admin/initiatives">Iniciativas</a></li>-->
                <li class="item-element"><a href="/logout">Cerrar sesión</a></li>
              </ul>
        </div>
      </div>