@extends('base')
@section('title', 'Detalle de punto | Mapa interactivo de humedales urbanos')


@section('content')

	<div class="container" id="point-detail"> 

		<div class="row">
			<div class="col-md-6">

				@if( count($data->image) == 0 )

					<img src="{{asset('images/dummy-image.jpg')}}" id="dummy">
				@elseif ( count($data->image) == 1 )

					<img id="dummy" src="data:{{ $data->image[0]->image_ext }};charset=utf-8;base64,{{ $data->image[0]->image_file }}" alt="">

				@else
				<div id="carouselExampleControls"  class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
					@for ($i = 0; $i < count($data->image); $i++)

						@if ($i == 0)

							<div class="carousel-item active">
      							<img class="d-block w-100" src="data:{{ $data->image[$i]->image_ext }};charset=utf-8;base64,{{ $data->image[$i]->image_file }}">
    						</div>

						@else
							<div class="carousel-item">
      							<img class="d-block w-100" src="data:{{ $data->image[$i]->image_ext }};charset=utf-8;base64,{{ $data->image[$i]->image_file }}">
    						</div>

						@endif


					@endfor
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
				@endif

			</div>
			<div class="col-md-6">
				<h2>{{ $data->title }}</h2>
				<div id="point-info">
					<p>Categoría: {{ $data->category }}</p>
					<p>Tema: {{ $data->theme }}</p>
					<p id="last-child"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $data->created_at->format('d-m-Y') }}</p>
				</div>
				<p>{{ $data->description }}</p>
			</div>
		</div>
	</div>


@endsection