<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<p class="title">Mapa interactivo</p>
				<a href="/what-is" class="item-footer">¿Qué es?</a>
				<a href="/api" class="item-footer">Desarrolladores</a>
			</div>
			<div class="col-sm-2">
				<p class="title">Ámbitos de acción</p>
				<a href="/map" class="item-footer">Mapa colaborativo</a>
				<!--<a href="/initiatives" class="item-footer">Iniciativas colectivas</a>-->
			</div>
			<div class="col-sm-2">
				<p class="title">Sobre nosotros</p>
				<a href="/team" class="item-footer">Equipo</a>
				<a href="/contact" class="item-footer">Contacto</a>
			</div>
			<div class="col-sm-2">
				<p class="title">Términos del servicio</p>
				<a href="#" class="item-footer">Normativa</a>
				<a href="/privacy" class="item-footer">Política de privacidad</a>
			</div>
			<div class="col-sm-4" style="text-align: right;">
				<p class="title" style="margin-bottom: 5px !important">Proyecto desarrollado por</p>
				<a href="http://www.patagua.cl"><img class="logo" style="margin-bottom: 5px !important" src="{{asset('images/patagua.png')}}"></a><br>
				<!--<a href="#" class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
				<a href="https://www.facebook.com/patagua.cl"  target="_blank"  class="icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</footer>