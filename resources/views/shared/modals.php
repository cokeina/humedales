<div class="modal fade" id="modalReject">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Rechazar dato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="rejectH">
        <div class="form-group">
                <label for="rejectCause">Causa </label>
                <select id="rejectCause" class="form-control">
                    <option value="1">Localización Incorrecta</option>
                    <option value="2">Inconsistencia en Categoría o Tema</option>
                    <option value="3">Contenido no adecuado</option>
                    <option value="4">Otro</option>
                </select>
        </div>
        <div class="form-group">
                <textarea class="form-control" rows="5" disabled id="textCause"></textarea>
        </div>        
	  	  
      </div>
      <div class="modal-footer">
        <button type="button" id="saveText" class="btn btn-danger">Rechazar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalLegend">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:blue;color:white">
        <h5 class="modal-title">Leyenda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-group">
         <b>Publicado</b>: Dato aprobado por un Curador y publicado en el mapa. Ya no puedes editarlo.
        </div>
        <div class="form-group">
         <b> En revisión: </b> Dato enviado a revisión para ser publicado. Ya no puedes editarlo.
        </div>
        <div class="form-group">
         <b>Guardado:</b> Dato guardado en tu sesión de usuario. Puedes editarlo o enviarlo a publicación cuando quieras.
        </div>
        <div class="form-group">
         <b>Rechazado:</b> Dato rechazado por un Curador. Puedes editarlo y volver a enviarlo a publicación.
        </div>  
	  	  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>