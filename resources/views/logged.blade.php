<html>
<head>
	<title> @yield('title') </title>
	
	<!-- Bootstrap styles -->
	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">

	<script src="https://use.fontawesome.com/e8dc2759e3.js"></script>

	<!-- Custom styles -->
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

</head>
<body>
	
	@include('shared.navbar-logged')


	@yield('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('/js/bootstrap.js')}}" type="text/javascript"></script>
</body>
</html>