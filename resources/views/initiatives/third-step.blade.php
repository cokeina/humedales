@extends('base')

@section('title', 'Crea una iniciativa')

@section('content')

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">

			<div class="row bs-wizard" style="border-bottom: 0;margin-bottom: 1em;">
				
				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 1</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Crear iniciativa</div>
				</div>

				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 2</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Definir montos</div>
				</div>

				<div class="col-3 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Paso 3</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Agregar recompensas</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 4</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">¡Iniciativa creada!</div>
				</div>

			</div>
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear una iniciativa</p>
			<p class="subtitle-form">Agrega recompensas, productos, servicios, reconocimientos u otros a los cuales podrán acceder los co-financiadores</p>

				<form method="post">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" name="initiative" value="{!! $initiative_id !!}">


						<div class="form-group" id="reward">
							<div class="row">
								<div class="col-6">
									<label for="name" style="display: block;margin-top: 1em;font-weight: bold">Recompensas:</label>
								</div>
								<div class="col-6" style="text-align: right;">
									<button class="btn" type="button" onclick="addMoreRewards()" style="font-size: .6em;width:100px;border-radius: 4px;"> Agregar más </button>
								</div>
							</div>

							<label for="name">Nombre de recompensa(*):</label>
							<div class="input-group">
								<input type="text" class="input-file" name="reward_name[]">
							</div>

							<label for="name">Descripción(*):</label>
							<div class="input-group">
								<textarea name="reward_description[]" id=""></textarea>
							</div>

							<div class="input-group">
								<label for="name">Monto(*):</label>
								<input type="text" class="input-file" name="reward_amount[]" placeholder='$' style="width: 20%;">
							</div>

							<div class="input-group">
								<input type="file" id="reward" class="input-file" name="file_rewards[]">
					            <input type="hidden" id="file_name_reward" name="filenames_rewards[]">
					            <input type="hidden" id="file_type_reward" name="filetypes_rewards[]">
					            <input type="hidden" id="file_btoa_reward" name="filebs_rewards[]">	
							</div>		

		     			 	<div id="added-inputs"></div>

							<label for="name" style="display: block;margin-top: 1em;font-weight: bold">Producto(opcional):</label>

							<label for="name">Nombre de producto:</label>
							<div class="input-group">
								<input type="text" class="input-file" name="product_name">
							</div>

							<label for="name">Descripción:</label>
							<div class="input-group">
								<textarea name="product_description" id=""></textarea>
							</div>

							<div class="input-group">
								<label for="name">Monto(*):</label>
								<input type="text" class="input-file" name="product_amount" placeholder='$' style="width: 20%;">
							</div>

							<div class="input-group">
								<input type="file" class="input-file" id="products" name="file_products">
					            <input type="hidden" id="file_name_products" name="filenames_products">
					            <input type="hidden" id="file_type_products" name="filetypes_products">
					            <input type="hidden" id="file_btoa_products" name="filebs_products">	
							</div>	


							<label for="name" style="display: block;margin-top: 1em;font-weight: bold">Servicio(opcional):</label>

							<label for="name">Nombre de servicio:</label>
							<div class="input-group">
								<input type="text" class="input-file" name="service_name">
							</div>

							<label for="name">Descripción:</label>
							<div class="input-group">
								<textarea name="service_description" id=""></textarea>
							</div>

							<div class="input-group">
								<label for="name">Monto(*):</label>
								<input type="text" class="input-file" name="service_amount" placeholder='$' style="width: 20%;">
							</div>

							<div class="input-group">
								<input type="file" class="input-file" id="service" name="file_service">
					            <input type="hidden" id="file_name_service" name="filenames_service">
					            <input type="hidden" id="file_name_service" name="filetypes_service">
					            <input type="hidden" id="file_name_service" name="filebs_service">	
							</div>	


							<label for="name" style="display: block;margin-top: 1em;font-weight: bold">Reconocimiento(opcional):</label>

							<label for="name">Nombre de reconocimiento:</label>
							<div class="input-group">
								<input type="text" class="input-file" name="recognition_name">
							</div>

							<label for="name">Descripción:</label>
							<div class="input-group">
								<textarea name="recognition_description" id=""></textarea>
							</div>

							<div class="input-group">
								<label for="name">Monto(*):</label>
								<input type="text" class="input-file" name="recognition_amount" placeholder='$' style="width: 20%;">
							</div>

							<div class="input-group">
								<input type="file" class="input-file" id="recognition" name="file_recognition">
					            <input type="hidden" id="file_name_recognition" name="filenames_recognition">
					            <input type="hidden" id="file_type_recognition" name="filetypes_recognition">
					            <input type="hidden" id="file_btoa_recognition" name="filebs_recognition">	
							</div>	

							<div class="form-group" id="submit-button" style="margin-top:2em;">
								<button class="btn">
									Siguiente paso
								</button>
							</div>


						</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

@endsection