@extends('base')

@section('title', 'Crea una iniciativa')

@section('content')

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">

			<div class="row bs-wizard" style="border-bottom: 0;margin-bottom: 1em;">
				
				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 1</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Crear iniciativa</div>
				</div>

				<div class="col-3 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Paso 2</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Definir montos</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 3</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Agregar recompensas</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 4</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">¡Iniciativa creada!</div>
				</div>

			</div>
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear una iniciativa</p>
			<p class="subtitle-form">Define los montos necesarios para tu iniciativa</p>

				<form method="post">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<input type="hidden" name="initiative" value="{!! $initiative_id !!}">

						<div class="form-group" style="margin-bottom: 1.2em;">
							<div class="row">
								<div class="col-4">
									<label for="name">Monto total requerido(*):</label>
									<input type="number" class="form-control" id="money" placeholder="$" name="money_total">
								</div>
								<div class="col-4">
									<label for="name">Monto óptimo requerido(*):</label>
									<input type="number" class="form-control" id="money" placeholder="$" name="money_opt">
								</div>
								<div class="col-4">
									<label for="name">Monto mínimo requerido(*):</label>
									<input type="number" class="form-control" id="money" placeholder="$" name="money_min">
								</div>
							</div>
						</div>

						<p class="subtitle-form" style="margin-top: 2em;">Detalla los montos requeridos</p>

						<div class="form-group amounts">

							<div class="form-group" style="margin-bottom: 1em;">
								<label for="name">Nombre del gasto(*):</label>
								<input type="text" class="form-control" name="name_amount" id="name_amount">
							</div>



							<div class="row" style="margin-bottom: 1em;">
								<div class="col-6">
									<label for="name">Monto de gasto(*):</label>
								</div>
								<div class="col-6">
									<input type="number" style="text-align:right" class="form-control" placeholder="$" name="money_amount" id="money_amount">
								</div>
							</div>


							<label for="name" style="display: block;margin-top: 1em">Categoría de monto(*):</label>
							<div class="row">
								<div class="col-4">
									<div class="checkbox">
										<input type="checkbox" name="type_amount" value="0" id="type_amount_material" onclick="changeCheckbox('#type_amount_material')">
										<img src="{{asset('images/24.png')}}">
										<label for="checkboxes-0">
										Material
										</label>
									</div>
								</div>
								<div class="col-4">
									<div class="checkbox">
										<input type="checkbox" name="type_amount" value="1" id="type_amount_task" onclick="changeCheckbox('#type_amount_task')">
										<img src="{{asset('images/23.png')}}">
										<label for="checkboxes-0">
										Tarea
										</label>
									</div>
								</div>
								<div class="col-4">
									<div class="checkbox">
										<input type="checkbox" name="type_amount" value="2" id="type_amount_inf" onclick="changeCheckbox('#type_amount_inf')">
										<img src="{{asset('images/22.png')}}">
										<label for="checkboxes-0">
										Infraestructura
										</label>
									</div>
								</div>
							</div>

							<div class="form-group" style="margin-top: 1em;">
								<label for="name">Descripción(*):</label>
								<textarea class="form-control" id="description_amount" name="description_amount" value="{{ old('motivation_amount') }}"></textarea>
							</div>

							<div class="form-group">
							<label for="name" style="display: block;margin-top: 1em">Tipo de monto(*):</label>
								<div class="row">
									<div class="col-6" style="text-align: center;">
										<div class="checkbox">
											<input type="checkbox" name="condition" value="0" id="imp_amount" onclick="changeCheckbox('#imp_amount')">
											<label for="checkboxes-0">Imprescindible</label>
										</div>
									</div>
									<div class="col-6" style="text-align: center;">
										<div class="checkbox">
											<input type="checkbox" name="condition" value="1" id="adi_amount" onclick="changeCheckbox('#adi_amount')">
											<label for="checkboxes-0">Adicional</label>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group" style="margin-bottom: 0;">
								<a class="btn amount-btn">
									Agregar
								</a>
							</div>

							<div id="response"></div>

						</div>


						<div class="form-group" style="margin-top: 1.5em;">
							
							<p class="title-amount">Desglose de montos</p>


							<div class="row">
								<div class="col-6">
									<p class="subtitle-amount"><img src="{{asset('images/24.png')}}">Material</p>
								</div>
								<div class="col-6">
									<p class="subtitle-amount" id="material_total_amount" style="float:right;">$0</p>
								</div>
							</div>

							<div id="material_amount"></div>

							<div class="row">
								<div class="col-6">
									<p class="subtitle-amount"><img src="{{asset('images/23.png')}}">Tarea</p>
								</div>
								<div class="col-6">
									<p class="subtitle-amount" id="task_total_amount" style="float:right;">$0</p>
								</div>
							</div>

							<div id="task_amount"></div>

							<div class="row">
								<div class="col-6">
									<p class="subtitle-amount"><img src="{{asset('images/22.png')}}">Infraestructura</p>
								</div>
								<div class="col-6">
									<p class="subtitle-amount" id="infraestructure_total_amount" style="float:right;">$0</p>
								</div>
							</div>

							<div id="infraestructure_amount"></div>

						</div>

						<p class="subtitle-form" style="margin-top: 2em;">Detalla si existen servicios requeridos</p>
						<div class="col-12" id="servs">
							<label style="font-weight: 400!important;" for="service">Servicios(opcional)</label>
							<div class="input-group" id="servs">
								<input type="text" class="form-control" id="servs" name="servs[]">
					      		<span class="input-group-btn">
					        		<button class="btn" type="button" onclick="addServ()"> + </button>
					      		</span>	
							</div>

							<div id="added-servs"></div>
						</div>

						<p class="subtitle-form" style="margin-top: 2em;">Detalla si existen bienes requeridos</p>
						<div class="form-group">
							<div class="col-12" id="goods">
								<label style="font-weight: 400!important;" for="money">Bienes(opcional)</label>
								<div class="input-group">
									<input type="text" class="form-control" id="good" name="good[]">
						      		<span class="input-group-btn">
						        		<button class="btn" type="button" onclick="addGood()"> + </button>
						      		</span>	
								</div>

								<div id="added-goods"></div>
							</div>
						</div>

						<div class="form-group" id="submit-button" style="margin-top:2em;">
							<button class="btn">
								Siguiente paso
							</button>
						</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

@endsection