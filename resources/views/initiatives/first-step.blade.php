@extends('base')

@section('title', 'Crea una iniciativa')

@section('content')

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">

			<div class="row bs-wizard" style="border-bottom: 0;margin-bottom: 1em;">
				
				<div class="col-3 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Paso 1</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Crear iniciativa</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 2</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Definir montos</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 3</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Agregar recompensas</div>
				</div>

				<div class="col-3 bs-wizard-step disabled">
					<div class="text-center bs-wizard-stepnum">Paso 4</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">¡Iniciativa creada!</div>
				</div>

			</div>
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear una iniciativa</p>
			<p class="subtitle-form">Cuéntanos acerca de tú iniciativa</p>

				<form method="post">
					
					<fieldset>
						
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<div class="form-group">
							<label for="name">Título de la iniciativa(*):</label>
							<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
						</div>

						<div class="form-group">
							<label for="name">Resumen de la iniciativa(*):</label>
							<textarea class="form-control" id="resume" name="resume" value="{{ old('resume') }}"></textarea>
						</div>

						<div class="form-group">
							<label for="name">Motivación y a quién se dirige(*):</label>
							<textarea class="form-control" id="motivation" name="motivation" value="{{ old('motivation') }}"></textarea>
						</div>

						<div class="form-group">
							<label for="name">Descripción de iniciativa(*):</label>
							<textarea class="form-control" id="description" name="description" value="{{ old('description') }}"></textarea>
						</div>

						<div class="form-group">
							<label for="name">Categorías(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="1">
										<img src="{{asset('images/cat1.png')}}">
										<label for="checkboxes-0">
										Naturaleza urbana
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="2">
										<img src="{{asset('images/cat2.png')}}">
										<label for="checkboxes-0">
										Iniciativas y buenas prácticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="3">
										<img src="{{asset('images/cat3.png')}}">
										<label for="checkboxes-0">
										Comunidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="4">
										<img src="{{asset('images/cat4.png')}}">
										<label for="checkboxes-0">
										Problemáticas
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="5">
										<img src="{{asset('images/cat5.png')}}">
										<label for="checkboxes-0">
										Historia
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="6">
										<img src="{{asset('images/cat6.png')}}">
										<label for="checkboxes-0">
										Herramientas y datos abiertos
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="7">
										<img src="{{asset('images/cat7.png')}}">
										<label for="checkboxes-0">
										Proyectos regionales
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="8">
										<img src="{{asset('images/cat8.png')}}">
										<label for="checkboxes-0">
										Regulación
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="9">
										<img src="{{asset('images/cat9.png')}}">
										<label for="checkboxes-0">
										Comunicación y difusión
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="categories[]" id="categories" value="10">
										<img src="{{asset('images/cat10.png')}}">
										<label for="checkboxes-0">							
										Educación para la sustentabilidad
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group" id="themes">
							<label for="name">Temas(*):</label>
							<div class="row">
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="1">
										<img src="{{asset('images/21.png')}}">
										<label for="checkboxes-0">
										Humedales y espacios del agua
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="2">
										<img src="{{asset('images/20.png')}}">
										<label for="checkboxes-0">
										Espacio público
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="3">
										<img src="{{asset('images/19.png')}}">
										<label for="checkboxes-0">
										Biodiversidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="4">
										<img src="{{asset('images/18.png')}}">
										<label for="checkboxes-0">
										Movilidad y accesibilidad
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="5">
										<img src="{{asset('images/17.png')}}">
										<label for="checkboxes-0">
										Acción Colectiva
										</label>
									</div>
								</div>
								<div class="col-6">
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="6">
										<img src="{{asset('images/16.png')}}">
										<label for="checkboxes-0">
										Infraestructura verde
										</label>
										</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="7">
										<img src="{{asset('images/15.png')}}">
										<label for="checkboxes-0">
										Patrimonio natural y cultural
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="8">
										<img src="{{asset('images/14.png')}}">
										<label for="checkboxes-0">
										Organizaciones sustentables
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="9">
										<img src="{{asset('images/13.png')}}">
										<label for="checkboxes-0">
										Renovación urbana y ambiental
										</label>
									</div>
									<div class="checkbox">
										<input type="checkbox" name="themes[]" id="themes" value="10">
										<img src="{{asset('images/12.png')}}">
										<label for="checkboxes-0">
										Cambio climático
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="name">Encargados(*):</label>
							<input type="text" class="form-control" name="managments[]" style="margin-bottom: 1em;">
<!-- 							<input type="text" class="form-control" name="managments[]" style="margin-bottom: 1em;">
							<input type="text" class="form-control" name="managments[]" style="margin-bottom: 1em;"> -->
						</div>

						<div class="form-group">
							<p style="font-weight: 400;font-size: .9em">Plazos:</p>
							<div class="row">
								<div class="col-6">
									<label style="font-weight: 400!important;font-size: .8em;" for="money">Fecha de inicio(*):</label>
									<input type="text" class="form-control" id="datepicker_begin"  name="start_date" value="{{ old('start_date') }}">
								</div>
								<div class="col-6">
									<label style="font-weight: 400!important;font-size: .8em;" for="money">Fecha de termino(*):</label>
									<input type="text" class="form-control" id="datepicker_end" name="end_date" value="{{ old('end_date') }}">
								</div>
							</div>
						</div>

					<div class="form-group">
						<label for="name" style="display: block;">Medios sociales(Opcional):</label>
						<label style="font-weight: 400!important;font-size: .8em;" for="rs">Facebook:</label>
						<input type="text" class="form-control" id="fb-url" name="fb-url" value="{{ old('fb-url') }}">
						<label style="font-weight: 400!important;font-size: .8em;" for="rs">Twitter:</label>
						<input type="text" class="form-control" id="tw-url" name="tw-url" value="{{ old('tw-url') }}">
						<label style="font-weight: 400!important;font-size: .8em;" for="rs">Youtube:</label>
						<input type="text" class="form-control" id="yt-url" name="yt-url" value="{{ old('yt-url') }}">
						<label style="font-weight: 400!important;font-size: .8em;" for="rs">Página web:</label>
						<input type="text" class="form-control" id="web-url" name="web-url" value="{{ old('web-url') }}">
					</div>

					<div class="form-group" id="photos">
						<label for="photos">Subir fotos(*):</label>
						<div class="input-group">
							<input type="file" id="photos0" class="input-file" name="file_photos[]">
		                  	<input type="hidden" id="file_name_photos0" name="filenames_photos[]">
		                  	<input type="hidden" id="file_type_photos0" name="filetypes_photos[]">
		                  	<input type="hidden" id="file_btoa_photos0" name="filebs_photos[]">
					      	<span class="input-group-btn">
					        	<button style="border-radius: 4px;padding: 4px 6px;margin-left: 1em;" class="btn" type="button" onclick="addPhoto()"> + </button>
					        	<button style="border-radius: 4px;background: red;padding: 4px 6px;margin-left: 1em;" class="btn" type="button" onclick="removePhoto()"> - </button>
					      	</span>					
	     			 	</div>

	     			 	<div id="added-photos"></div>
					</div>

					<div class="form-group" id="submit-button">
						<button class="btn">
							Siguiente paso
						</button>
					</div>

					</fieldset>

				</form>

		</div>

	</div>

</div>

@endsection