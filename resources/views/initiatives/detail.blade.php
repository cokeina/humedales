@extends('base')
@section('title', 'Detalle de punto | Mapa interactivo de humedales urbanos')


@section('content')


	<div class="container" id="initiative-show">
			

			<div id="title">
				<h2>{{ $initiative->initiative_title }}</h2>
				<p>Creado por: {{ $initiative->initiative_name }}</p>
			</div>

			<div class="row">
				<div class="col-sm-7" id="initiative_img">


				@if( count($photos) == 0 )

					<img src="{{asset('images/dummy-image.jpg')}}" id="dummy">
				@elseif ( count($photos) == 1 )

					<img id="dummy" src="data:{{ $photos[0]->photo_type }};charset=utf-8;base64,{{ $photos[0]->photo_file }}" alt="">

				@else
				<div id="carouselExampleControls"  class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
					@for ($i = 0; $i < count($photos); $i++)

						@if ($i == 0)

							<div class="carousel-item active">
      							<img class="d-block w-100" src="data:{{ $photos[$i]->photo_type }};charset=utf-8;base64,{{ $photos[$i]->photo_file }}">
    						</div>

						@else
							<div class="carousel-item">
      							<img class="d-block w-100" src="data:{{ $photos[$i]->photo_type }};charset=utf-8;base64,{{ $photos[$i]->photo_file }}">
    						</div>

						@endif


					@endfor
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
				@endif









				</div>
				<div class="col-sm-5" id="initiative_info">

					<div id="obtained">
						<p>Obtenido</p>
						<h2>$ {{ $initiative->actual }}</h2>
					</div>


					<div id="money_details">
						<p id="title_detail">Mínimo</p>
						<p id="amount">$1{{ $initiative->minimum }}</p>

						<p id="title_detail">Optimo</p>
						<p id="amount">${{ $initiative->optimum }}</p>

						<p id="amount">Quedan {{ $initiative->diff->days }} días</p>
					</div>

					<button id="cofinanciate" class="btn btn-primary">COFINANCIAR</button>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-7" id="initiative_detail">
					<div class="category">
						<p id="subtitle">Categoría: </p>
						<p>Naturaleza Urbana</p>
					</div>
					<div class="theme">
						<p id="subtitle">Tema: </p>
						<p>Movilidad y accesibilidad</p>
					</div>
				</div>
				<div class="col-sm-5 d-flex align-items-center justify-content-between" id="social-links"> 
						@if ($initiative->url_fb != null)
							<a href="{{ $initiative->url_fb }}"><img src="{{asset('images/social/fb.png')}}"></a>
						@else
							<a><img src="{{asset('images/social/fb-no.png')}}"></a>
						@endif

						@if ($initiative->url_tw != null)
							<a href="{{ $initiative->url_tw }}"><img src="{{asset('images/social/tw.png')}}"></a>
						@else
							<a><img src="{{asset('images/social/tw-no.png')}}"></a>
						@endif

						@if ($initiative->url_yt != null)
							<a href="{{ $initiative->url_yt }}"><img src="{{asset('images/social/gplus.png')}}"></a>
						@else
							<a><img src="{{asset('images/social/gplus-no.png')}}"></a>
						@endif

						@if ($initiative->url_web != null)
							<a href="{{ $initiative->url_web }}"><img src="{{asset('images/social/link.png')}}"></a>
						@else
							<a><img src="{{asset('images/social/link-no.png')}}"></a>
						@endif
				</div>
			</div>


	</div>

	<div class="about">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-7" id="about_project">
					
					<h3>Sobre este proyecto</h3>

					<p>{{ $initiative->initiative_resume }}</p>

					<div id="accordion" role="tablist">
					  <div class="card">
					    <div class="card-header" role="tab" id="headingOne">
					      <h5 class="mb-0">
					        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          Listado de necesidades
					        </a>
					      </h5>
					    </div>

					    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
					      <div class="card-body">
							

							@if (count($materials) != 0)

								<div id="amount_detail">
									<img src="{{asset('images/24.png')}}">
									<h3>Materiales</h3>
								</div>

								@foreach ($materials as $material)

									<div id="item_detail">
											
										<p style="font-weight:bold;margin-bottom: .5em;">{{ $material->material_name }}</p>
										<p>{{ $material->material_description }}</p>
										<div class="footer_detail d-flex justify-content-between">
											<p>{{ $material->material_importance }}</p>
											<p style="font-weight:bold">${{ $material->material_money }}</p>
										</div>


									</div>		

								@endforeach

							

							@endif


							@if (count($infraestructures) != 0)

							<div id="amount_detail">
								<img src="{{asset('images/22.png')}}">
								<h3>Infraestructura</h3>
							</div>

								@foreach ($infraestructures as $infraestructure)

									<div id="item_detail">
											
										<p style="font-weight:bold;margin-bottom: .5em;">{{ $infraestructure->infraestructure_name }}</p>
										<p>{{ $infraestructure->infraestructure_description }}</p>
										<div class="footer_detail d-flex justify-content-between">
											<p>{{ $infraestructure->infraestructure_importance }}</p>
											<p style="font-weight:bold">${{ $infraestructure->infraestructure_money }}</p>
										</div>


									</div>		

								@endforeach
							
							@endif

							@if (count($tasks) != 0)

							<div id="amount_detail">
								<img src="{{asset('images/23.png')}}">
								<h3>Tareas</h3>
							</div>
	
								@foreach ($tasks as $task)

									<div id="item_detail">
											
										<p style="font-weight:bold;margin-bottom: .5em;">{{ $task->task_name }}</p>
										<p>{{ $task->task_description }}</p>
										<div class="footer_detail d-flex justify-content-between">
											<p>{{ $task->task_importance }}</p>
											<p style="font-weight:bold">${{ $task->task_money }}</p>
										</div>


									</div>		

								@endforeach


							@endif


					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="headingTwo">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					          Descripción de la iniciativa
					        </a>
					      </h5>
					    </div>
					    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
					      <div class="card-body">
					        <p id="initiative_description">{{ $initiative->initiative_description }}</p>
					      </div>
					    </div>
					  </div>

					  <div class="card">
					    <div class="card-header" role="tab" id="headingThree">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					          Motivación de la iniciativa
					        </a>
					      </h5>
					    </div>
					    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
					      <div class="card-body">
					        <p id="initiative_description">{{ $initiative->initiative_motivation }}</p>
					      </div>
					    </div>
					  </div>

					  <div class="card">
					    <div class="card-header" role="tab" id="headingFour">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					          Encargados de la iniciativa
					        </a>
					      </h5>
					    </div>
					    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
					      <div class="card-body">
					        @if (count($managers) != 0)
	
								@foreach($managers as $manager)

									<p id="initiative_description">{{ $manager->manager_name }}</p>

								@endforeach

					        @endif
					      </div>
					    </div>
					  </div>
					</div>



				</div>

				<div class="col-sm-5" id="reward">
					
					<h3>Recompensas</h3>


					@if( count($rewards) != 0)

						@foreach ($rewards as $reward)


							<div class="reward_item">
								
								<h4>{{ $reward->reward_name }}</h4>

								<p>{{ $reward->reward_description }}</p>

								<div class="amount d-flex justify-content-between">
									<p>Monto: ${{ $reward->reward_amount }}</p>
									<i class="fa fa-camera" aria-hidden="true" data-toggle="modal" data-target="#{{ $reward->reward_name }}"></i>
								</div>
								<button class="btn btn-primary">Quiero esta recompensa</button>


								<!-- Modal -->
									<div class="modal fade" id="{{ $reward->reward_name }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									    	<img class="d-block w-100" src="data:{{ $reward->reward_type }};charset=utf-8;base64,{{ $reward->reward_image }}">
									    </div>
									  </div>
									</div>

							</div>

						@endforeach

					@endif

				</div>

			</div>

		</div>


	</div>



@endsection