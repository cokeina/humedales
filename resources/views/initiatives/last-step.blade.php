@extends('base')

@section('title', 'Crea una iniciativa')

@section('content')

<div class="wrapper-form">
	
	<div class="container">

		<div class="form-section initiatives-section">

			<div class="row bs-wizard" style="border-bottom: 0;margin-bottom: 1em;">
				
				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 1</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Crear iniciativa</div>
				</div>

				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 2</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Definir montos</div>
				</div>

				<div class="col-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum">Paso 3</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">Agregar recompensas</div>
				</div>

				<div class="col-3 bs-wizard-step active">
					<div class="text-center bs-wizard-stepnum">Paso 4</div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center">¡Iniciativa creada!</div>
				</div>

			</div>
		
	        @foreach ($errors->all() as $error)
	            <p class="alert alert-danger">{{ $error }}</p>
	        @endforeach

	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

			<p class="title-form">Crear una iniciativa</p>

			<div id="created-initiative">
				

				<img src="{{asset('images/social/party-popper.png')}}">

				<h3>¡Tu iniciativa ha sido creada!</h3>
				<p>Uno de nuestros administradores evaluará tu iniciativa y, de estar todo correcto, esta será publicada</p>

			</div>
		</div>

	</div>

</div>

@endsection