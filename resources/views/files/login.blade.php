@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
@endsection


@section('content')
    <div class="container">
        <div class="row profile">
            <div class="col-12 admin-users">
                <h3>Descargas</h3>
                <p>Para acceder a los archivos disponibles para descarga, debes iniciar sesión</p>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>


@endsection