@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
@endsection


@section('content')
    <div class="container">
        <div class="row profile">
            <div class="col-12 admin-users">
                <h3>Descargas</h3>
                <table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Descripción</th>
                        <th>Archivo</th>
                        <th>Fecha creación</th>
                        <th>Acciones</th>
                        </tr>
                    </thead>
              </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>

<script>

    var table = null;

    $(document).ready(function() {
    
        table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {

                    HMApi.get('{{ url('/files' ) }}?' + $.param(data), function (response) {
                        callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.files
                        });
                    });

                },
                "paging": true,
                "columns": [
                    { "data": "id", "visible": false,},
                    { "data": "description"},
                    { "data": "filename"},
                    { "data": "created_at",
                       render:function(data)
                       {
                            var localTime  = moment.utc(data).toDate();
                            return moment(localTime).format('DD-MM-YYYY HH:mm:ss');
                       }
                    },
                    { "data": "id",
                        class:"text-center",
                        render:function(data,type,full,meta)
                        {
                            var download = "<button  type='button' class='btn btn-success btn-xs' onclick=downloadFile("+data+");><i class='fa fa-xs fa-download fa-fw' ></i></button>";

                            return  "<div class='btn-group'>"+download+"</div>";
                        }
                    }
                ]
        });
        
    
    });

    function downloadFile(id)
    {
        HMApi.download('{{ url('/files/download/') }}/'+id);
    }

</script>
@endsection