@extends('base')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
@endsection


@section('content')
    <div class="container">
            @include('shared.navbar-admin')	
        <div class="row profile">
            <div class="col-12 admin-users">
                <h3>Gestor de Descargas</h3>
                <button type="button" class="btn btn-primary" id="myButton" style="margin-left:15px"><i class="fa fa-plus"></i></button>
                <table id="datas" class="display nowrap table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Descripción</th>
                        <th>Archivo</th>
                        <th>Fecha creación</th>
                        <th>Acciones</th>
                        </tr>
                    </thead>
              </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/api.js') }}"></script>
<script src="{{ asset('js/modal.js') }}"></script>
<script type="text/javascript">
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
</script>

<script>

    var table = null;

    var rows = [
            [
                {field: 'Descripción', type: 'text', id: 's_name', value: 'name'}
            ],
            [
                {field: 'Archivo', type: 'file', id: 's_file', value: 'file'}
            ]
        ];

    var params = {
        title: 'Agregar archivo',
        rows: rows
    }

    fieldValues = [];

    var group_id = "modal_group";

    HModal.create(group_id, params);

    $(document).ready(function() {
    
        table = $('#datas').DataTable({
                "language": {
                    "url": "{{ asset('json/Spanish.json') }}"
                },
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "searching": false,
                "ajax": function (data, callback, settings) {

                    HMApi.get('{{ url('/files' ) }}?' + $.param(data), function (response) {
                        callback({
                            recordsTotal: response.data.recordsTotal,
                            recordsFiltered: response.data.recordsFiltered,
                            data: response.data.files
                        });
                    });

                },
                "paging": true,
                "columns": [
                    { "data": "id", "visible": false,},
                    { "data": "description"},
                    { "data": "filename"},
                    { "data": "created_at",
                       render:function(data)
                       {
                            var localTime  = moment.utc(data).toDate();
                            return moment(localTime).format('DD-MM-YYYY HH:mm:ss');
                       }
                    },
                    { "data": "id",
                        class:"text-center",
                        render:function(data,type,full,meta)
                        {
                            var download = "<button  type='button' class='btn btn-success btn-xs' onclick=downloadFile("+data+");><i class='fa fa-xs fa-download fa-fw' ></i></button>";
                            var del      = "<button  type='button' class='btn btn-danger btn-xs' onclick=del("+data+");><i class='fa fa-xs fa-trash fa-fw' ></i></button";

                            return  "<div class='btn-group'>"+download+" "+del+"</div>";
                        }
                    }
                ]
        });

        (function setFieldValues() {
                fieldValues['name'] = 's_name';
                fieldValues['file'] = 's_file';
                
        })();

        $('#myButton').click(function () {
                
                $("#"+group_id+" div.form-group").removeClass("has-error");
                $(".modal-body input").val("");
                $('#' +group_id).modal('show');
        });

        $('#' + group_id + "_create").click(function(){

                var frm = new FormData();

                var file =  $('#s_file')[0].files[0] ;
                if (file == undefined) file = '';
                
                frm.append('description', $('#s_name').val());
                frm.append('file',file);

                $.ajax({
                    url:'{{ url('/files') }}',
                    data:frm,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){
                       


                         if(response.code == 200)
                         {
                            swal("Agregar archivo", "Información ingresada de forma exitosa", "success");
                            table.ajax.reload();
                            $('#' +group_id).modal('toggle');
                         }

                         if(response.code == 201)
                         {
                            errors = [];
                            
                            for (x in response.data.errors)
                            {
                                errors.push('<li>'+response.data.errors[x].join(",")+'</li>');
                            }

                            console.log(errors);

                            swal("Error",errors.join(" "), "error");
                            


                         }

                    },
                    failure:function(response)
                    {

                    }
                });
        });
        
    
    });

    function downloadFile(id)
    {
        HMApi.download('{{ url('/files/download/') }}/'+id);
    }

    function del(id) {

            swal({
                    title: "Eliminar Archivo",
                    text: "¿Esta seguro de realizar esta acción?'",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "SI",
                    cancelButtonText: "NO"
            }).then(function () {
                    
                var url  = '{{ url('/files')  }}';
                url += '/'+id; 

                $.ajax({
                    url:url,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success:function(response){ 

                        switch(response.code)
                        {
                            case 200:
                                table.ajax.reload();
                            break;

                            case 403:
                                swal('Error',response.data.message,'error');   
                            break;
                            
                            default :
                            swal('Error','Ha ocurrido un error en la operación','error');   
                            break;

                        }
                        
                    }
                });   
                    
                    /*HMApi.delete('{{  url('/files')  }}/'+id,function(data) {
                        
                        

                        
                    });*/
                });
            
    }

</script>
@endsection