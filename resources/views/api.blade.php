@extends('base')

@section('title', 'Política de privacidad | Mapa interactivo de humedales urbanos')

@section('content')

	<div id="api">
		<div class="container">

            <BR>
            <h2>API</h2>

            <ul>
                <li>
                <p><b>Primer paso :</b> realizar petición POST a la siguiente url : {{ url('api/oauth/token') }} </p>
                <p> Los parámetros a requerir son los siguientes : </p>
                <table class="table ">
                    <tr>
                        <td>
                           <b> email </b> 
                        </td>
                        <td>
                            Correo electrónico del usuario
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <b> password </b> 
                        </td>
                        <td>
                            Contraseña del usuario
                        </td>
                    </tr>
                </table>

                </li>

                <li>
                    <p>Se recibirá una respuesta en formato JSON, en el cual se debe rescatar el parámetro <b>access_token</b> , 
                    que es la llave que se utilizará para realizar llamadas a la API (<b>observación : </b> esta llave es temporal, por lo que si luego de un 
                    tiempo sus peticiones arrojan error de caducidad, se debe volver a general un token con la petición anterior). </p>
                </li>

                <li>
                    <p>Luego, en cada petición hacia la API, se debe incluir en el header de la petición : <b>Authorization : Bearer "valor_del_access_token" </b></p>
                </li>

                <li>
                    <p>Lista de peticiones disponibles :</p>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Petición</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Parámetros</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Respuesta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ url('api/user/maps') }}</td>
                                <td>GET</td>
                                <td>Ninguno</td>
                                <td>Obtiene los mapas disponibles del usuario</td>
                                <td>
                                    <ul>
                                        <li><b>id :</b> Identificador del mapa </li>
                                        <li><b>name :</b> Nombre del mapa </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>{{ url('map/id_mapa/points') }}</td>
                                <td>GET</td>
                                <td>Ninguno</td>
                                <td>Lista los datos publicados en un mapa en particular</td>
                                <td>
                                    <ul>
                                        <li><b>id :</b> Identificador del dato </li>
                                        <li><b>title :</b> Título del dato </li>
                                        <li><b>description :</b> Descripción del dato </li>
                                        <li><b>category_name :</b> Categoría </li>
                                        <li><b>themes_name :</b> Tema  </li>
                                        <li><b>type :</b> Este puede ser point (Punto), line (Línea) o polygon (Polígono)  </li>
                                        <li><b>points :</b> Arreglo con las coordenadas asociadas al dato, contienen latitud y longitud  </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>{{ url('api/user/points') }}</td>
                                <td>GET</td>
                                <td>Ninguno</td>
                                <td>Lista los datos ingresados por un usuario</td>
                                <td>
                                    <ul>
                                        <li><b>id :</b> Identificador del dato </li>
                                        <li><b>title :</b> Título del dato </li>
                                        <li><b>description :</b> Descripción del dato </li>
                                        <li><b>map_name :</b> Nombre del mapa asociado al dato </li>
                                        <li><b>category_name :</b> Categoría </li>
                                        <li><b>themes_name :</b> Tema  </li>
                                        <li><b>type :</b> Este puede ser point (Punto), line (Línea) o polygon (Polígono)  </li>
                                        <li><b>statusName :</b> Estado del dato  </li>
                                        <li><b>points :</b> Arreglo con las coordenadas asociadas al dato, contienen latitud y longitud  </li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
                <br>

            </ul>

        </div>
    </div>

@endsection