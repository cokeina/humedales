@extends('base')
@section('title', 'Registro confirmado')

@section('content')

<div class="wrapper-form">
   <div class="container">
        <div class="form-section">

            <p class="title-form">Registro</p>

            <p>El registro se ha realizado de forma exitosa. Se ha enviado un correo para realizar la verificación</p>

        </div>
    </div>
</div>

@endsection