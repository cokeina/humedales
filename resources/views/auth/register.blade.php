@extends('base')
@section('title', 'Iniciar sesión | Mapa interactivo de humedales urbanos')


@section('content')

<div class="wrapper-form">
  
  <div class="container">

    <div class="form-section">

        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif


        <p class="title-form">Registro de usuario</p>


        <div class="form-group">
            <a href="{{ route('social.auth', 'facebook') }}"><button class="btn btn-primary" style="background:#3b5998;border-color:#3b5998">Registrarse con Facebook</button></a>
        </div>

        <form method="post">
  
        <p style="text-align: center;margin-top: 2em;">Regrístrate ingresando tus datos</p>

        <fieldset>

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="form-group">
          <label for="name">Nombre:</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
        </div>

        <div class="form-group">
          <label for="name">Apellido:</label>
          <input type="text" class="form-control" id="name" name="surname" value="{{ old('surname') }}">
        </div>

        <div class="form-group">
          <label for="name">Email:</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
        </div>

        <div class="form-group">
          <label for="name">Contraseña:</label>
          <input type="password" class="form-control" id="password" name="password">
        </div>

        <div class="form-group">
          <label for="name">Confirmar contraseña:</label>
          <input type="password" class="form-control" id="passwordinput" name="password_confirmation">
        </div>

        <!-- Button -->
        <div class="form-group">
            <button class="btn">Registrar</button>
        </div>

        <a href="/login">¿Tienes una cuenta? Inicia sesión</a>

        </fieldset>
        </form>
    </div>

  </div>

</div>


@endsection