@extends('base')
@section('title', 'Restablecer contraseña ')

@section('content')
<div class="wrapper-form">
   <div class="container">
        <div class="form-section">
            
            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            
            @if (session('status'))
                <div class="alert alert-success">
                            {{ session('status') }}
                </div>
            @endif

            <p class="title-form">Restablecer contraseña</p>

            <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                <fieldset>
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="email" class="control-label">Email :</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class="form-group">
                            <button type="submit" class="btn" style="margin-bottom: 1em;">
                                    Enviar
                            </button>
                    </div>
                    

                
                </fieldset>
            </form>

        </div>
   </div>
</div>
@endsection
