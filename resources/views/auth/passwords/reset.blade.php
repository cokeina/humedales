@extends('base')
@section('title', 'Restablecer contraseña ')

@section('content')

<div class="wrapper-form">
   <div class="container">
        <div class="form-section">
            @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            
            @if (session('status'))
                <div class="alert alert-success">
                            {{ session('status') }}
                </div>
            @endif

            <p class="title-form">Restablecer contraseña</p>

            <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                <fieldset>
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label for="email" class="control-label">Usuario</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="password" class="control-label">Contraseña</label>
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="control-label">Confirmar Contraseña</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>

                    <div class="form-group">
                            <button type="submit" class="btn" style="margin-bottom: 1em;">
                                    Restablecer
                            </button>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>
</div>

@endsection