@extends('base')
@section('title', 'Iniciar sesión | Mapa interactivo de humedales urbanos')


@section('content')

<div class="wrapper-form">
  
  <div class="container">

    <div class="form-section">

        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif


        <p class="title-form">Iniciar sesión</p>

        <a href="{{ route('social.auth', 'facebook') }}"><button class="btn btn-primary" style="background:#3b5998;border-color:#3b5998">Ingresar con Facebook</button></a>

        <form method="post">

            <p style="text-align: center;margin-top: 2em;">Inicia sesión ingresando con tus datos</p>
        <fieldset>

        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="form-group">
          <label for="name">Email:</label>
          <input type="email" class="form-control" id="email" required name="email" value="{{ old('email') }}">
        </div>

        <div class="form-group">
          <label for="name">Contraseña:</label>
          <input type="password" class="form-control" id="password" required name="password">
        </div>

        <!-- Button -->
        <div class="form-group">
            <button class="btn" style="margin-bottom: 1em;">Ingresar</button>
        </div>

        <a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
        <a href="/users/register">Crear una cuenta de usuario</a>

        </fieldset>
        </form>
    </div>

  </div>

</div>


@endsection