@extends('base')
@section('title', 'Registro confirmado')

@section('content')

<div class="wrapper-form">
   <div class="container">
        <div class="form-section">

            @if($verified)
            <p class="title-form">Registro confirmado</p>
            <p>El usuario ya fue confirmado en el sistema</p>
            @elseif($token)
            <p class="title-form">Error en el registro</p>
            <p>Token inválido</p>
            @else 
            <p class="title-form">Registro confirmado</p>
                <p>Tu email fue confirmado de manera exitosa. Para ingresar haz click en el siguiente <a href="{{url('/login')}}">enlace</a> </p>
            @endif
        </div>
    </div>
</div>

@endsection