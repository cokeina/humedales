@extends('base')

@section('title', '¿Qué es?')

@section('content')

<div class="first-section-wt-is">
	<div class="container">
		<h3>¿Qué es?</h3>

		<p class="introduction">{!! $contents['what_is_text_1'] !!}</p>
		<div class="row">
			<div class="col-sm-6" id="info">
			{!! $contents['what_is_text_2'] !!}
			</div>
			<div class="col-sm-6">
				<img class="obs" src="{{asset('storage/contents/'.$contents['what_is_image_1']).'?'.time() }}">
			</div>
		</div>
	</div>
</div>

<div class="second-section-wt-is">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="plus">
					<h3>¡Súmate!</h3>
					<p>Buscamos <strong class="magnified">articular a una comunidad</strong> con interés en generar y compartir información sobre los
						humedales, la naturaleza urbana y su importancia en la ciudad.</p>
					<p><strong class="magnified">Tu participación es la base</strong> del conocimiento colectivo que estamos co-construyendo. Este
					mapa lo hacemos todos. La información que generes y compartas, permitirá conformar una
					base de datos abiertos, que pueda utilizarse con igual interés en el mundo público, privado y
					ciudadano.</p>
				</div>
			</div>
			<div class="col-sm-6 ilustration">
				<img src="{{asset('images/ilustra.png')}}">
			</div>
		</div>
	</div>
</div>

<div class="third-section-wt-is">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<img src="{{ asset('images/cc.png') }}">
				{!! $contents['what_is_text_3'] !!}
			</div>
			<div class="col-sm-5 ml-sm-auto" id="info">
				{!! $contents['what_is_text_4'] !!}
			</div>
		</div>
	</div>
</div>


@endsection