@extends('base')

@section('title', 'Política de privacidad | Mapa interactivo de humedales urbanos')

@section('content')

	<div id="privacy">
		<div class="container">
			
			<h2>Política de privacidad</h2>
			<p>La presente Política de Privacidad establece los términos en que el Mapa Interactivo de Humedales
			Urbanos usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su
			sitio web. Esta iniciativa está comprometida con la seguridad de los datos de sus usuarios. Cuando le
			pedimos llenar los campos de información personal con la cual usted crea su nombre de usuario, lo
			hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento.</p>

			<h3>Información que es recogida</h3>

			<p>Nuestro sitio web podrá recoger información personal por ejemplo: Nombre,  información de contacto
			como su dirección de correo electrónico. Asimismo, cuando sea necesario podrá ser requerida
			información específica para validar información o verificar algún dato que haya sido aportado por
			alguno de los usuarios en la plataforma.
			</p>

			<h3>Uso de la información recogida</h3>

			<p>La plataforma del Mapa Interactivo de Humedales Urbanos emplea la información con el fin de
			proporcionar el mejor servicio posible en forma abierta a la comunidad para mantener un registro de
			usuarios y favorecer la protección y cuidado de los humedales.  Es posible que sean enviados correos
			electrónicos periódicamente a través de nuestro sitio para informar respecto de nueva información que
			haya sido publicada, o nuevas iniciativas de proyectos relacionados con los humedales y su protección.
			Estos correos electrónicos serán enviados a la dirección que usted proporcione y podrán ser cancelados
			en cualquier momento.
			El Mapa Interactivo de Humedales Urbanos está altamente comprometido para cumplir con el
			compromiso de mantener su información segura y no utilizarla para otros fines.</p>

			<h3>Enlaces a Terceros</h3>

			<p>Este sitio web podría contener enlaces a otros sitios que pudieran ser de su interés. Una vez que usted
			dé click en estos enlaces y abandone nuestra página, ya no tenemos control sobre el sitio al que es
			redirigido y, por lo tanto, no somos responsables de los términos o privacidad ni de la protección de sus
			datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad, por
			lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con éstas.</p>

			<h3>Control de su información personal</h3>

			<p>En cualquier momento usted puede restringir la recopilación o el uso de la información personal que es
			proporcionada a nuestro sitio web. 
			El Mapa Interactivo de Humedales Urbanos no venderá, cederá ni distribuirá la información personal
			que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.
			El Mapa Interactivo de Humedales Urbanos se reserva el derecho de cambiar los términos de la
			presente Política de Privacidad, previo aviso en nuestro sitio web.</p>

		</div>
	</div>


@endsection