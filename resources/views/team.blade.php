@extends('base')

@section('title', 'Equipo | Mapa interactivo de humedales urbanos')

@section('content')

	<div id="team">
		<div class="container">
			
			<h2>Equipo</h2>

			<div class="row">

				<div class="col-sm-4">
					
					<div class="card" style="width: 18vw;">
					  <img class="card-img-top" src="{{asset('images/dummy-image.jpg')}}">
					  <div class="card-body">
					    <h4 class="card-title">Camila Teutsch</h4>
					    <p class="card-text">Ingeniera en Recursos Naturales</p>
					    <p class="card-text">Master en Gestión Integrada del Agua</p>
					  </div>
					</div>

				</div>

				<div class="col-sm-4">
					
					<div class="card" style="width: 18vw;">
					  <img class="card-img-top" src="{{asset('images/dummy-image.jpg')}}">
					  <div class="card-body">
					    <h4 class="card-title">Francisco Vásquez</h4>
					    <p class="card-text">Ingeniero en Recursos Naturales</p>
					    <p class="card-text">Master en Estudios Urbano-Regionales</p>
					  </div>
					</div>

				</div>

				<div class="col-sm-4">
					
					<div class="card" style="width: 18vw;">
					  <img class="card-img-top" src="{{asset('images/dummy-image.jpg')}}">
					  <div class="card-body">
					    <h4 class="card-title">Fiorenza Marinkovic</h4>
					    <p class="card-text2">Geógrafa</p>
					    <p class="card-text2">Master en Conservación y gestión de Recursos Naturales</p>
					    <p class="card-text2">Master en Comunicación Cartográfica y SIG</p>
					  </div>
					</div>

				</div>

			
			</div>


			<div class="row align-items-center">
				
				<div class="col-12">
					
					<div class="card" style="width: 18vw;">
					  <img class="card-img-top" src="{{asset('images/dummy-image.jpg')}}">
					  <div class="card-body">
					    <h4 class="card-title">Joaquín Aguirre</h4>
					    <p class="card-text">Ingeniero Agrónomo</p>
					  </div>
					</div>

				</div>


			</div>

		</div>
	</div>


@endsection