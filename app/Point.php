<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    
	protected $table = 'points';

    protected $fillable = ['latitude', 'longitude', 'data_id','latitudex','longitudex'];
}
