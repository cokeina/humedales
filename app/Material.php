<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'materials';

    protected $fillable = ['material_name', 'material_description', 'material_money', 'material_importance', 'initiative_id'];
}
