<?php

namespace App\Http\Controllers;


use Auth;
use Response;
use App\Data;
use App\Point;
use App\Map;
use App\Map_preferences;
use App\Map_private_preferences;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MapController extends Controller
{
    
    public function setpreferences(Request $request)
    {
        if(Auth::check())
        {
            
            $user = Auth::user();

            if($request->input("map") == "public")
            {
                $preference = Map_preferences::where('user_id',$user->id)->first();
            }
            else
            {
                $preference = Map_private_preferences::where('user_id',$user->id)->where('map_id',$request->input("mid"))->first();
            }

            

            if($preference == null)
            {
                if($request->input("map") == "private")
                {
                    $preference                         = new Map_private_preferences();
                    $preference->map_id = $request->input("mid");  
                    $preference->fill($request->all());
                    $preference->map_style              = 'basic';
                    $preference->user_id                = $user->id;
                    $preference->save(); 
                }
                else
                {
                    $preference                         = new Map_preferences();
                    $preference->fill($request->all());
                    $preference->map_style              = 'basic';
                    $preference->user_id                = $user->id;
                    $preference->save();
                }
                
                
            }
            else
            {
                switch($request->input('type'))
                {
                    case 'zoom':
                        $preference->zoom = $request->input("zoom");
                        $preference->latitude  = $request->input("latitude");
                        $preference->longitude = $request->input("longitude");

                    break;

                    case 'center':
                    
                        $preference->latitude  = $request->input("latitude");
                        $preference->longitude = $request->input("longitude");
                    
                    break;

                    case 'style':
                    
                        $preference->map_style  = $request->input("style");
                    
                    break;

                    

                }
                $preference->save();
            }

            return response()->json([
                                'success' => true,
                                'code' => 200,
                                'message' => 'ok',
                                'data' => null
                            ], 200);
       

        }
        else
        {
            return response()->json([
                'success' => false,
                'code' => 200,
                'message' => 'ok',
                'data' => null
             ], 200);    
        }
    }

    public function exportKML(Request $request)
    {
        $type = $request->input("type");

        $content  = '<?xml version="1.0" encoding="UTF-8"?>';
        $content .= '<kml xmlns="http://www.opengis.net/kml/2.2">';
        $content .= '<Document>';
        
        switch($type)
        {
            case 'public':
                
                $content .= '<name>Mapa Público</name><Style id="thickLine"><LineStyle><width>2.5</width></LineStyle></Style>';
                $content .= '<Style id="transparent50Poly"><PolyStyle><color>7fffffff</color></PolyStyle></Style>';


                $map = Map::where("name","Public map")->first();

                $data = DB::table("data")->join("map_data","data.id","map_data.data_id");
                $data = $data->where("map_data.map_id",$map->id);
                $data = $data->where("data.status",1);
                $data = $data->select("data.id","data.title","data.description","data.type");
                $data = $data->get();

                foreach ($data as $dt)
                {
                    $content .= '<Placemark>';
                    $content .= '<name>'.$dt->title.'</name>';
                    $content .= '<description>'.$dt->title.'</description>';
                    
                    if($dt->type == "point")
                    {
                        $content .= '<Point>';

                        $point = DB::table("points")->where("data_id",$dt->id)->first();

                        if ($point == null)
                        {
                            $lat = 0;
                            $lng = 0;
                        }
                        else
                        {
                            $lat = $point->latitude;
                            $lng = $point->longitude;
                        }

                        $content .= '<coordinates>'.$lng.','.$lat.',0</coordinates>';

                        $content .= '</Point>';
                    }

                    if($dt->type == "polygon")
                    {
                        $content .= '<Polygon>';

                        $content .= '<outerBoundaryIs><LinearRing><coordinates>';

                        $points = DB::table("points")->where("data_id",$dt->id)->get();

                        foreach ($points as $point)
                        {
                            $content .= $point->longitude.','.$point->latitude.',0 ';
                        }

                        $content .= '</coordinates></LinearRing></outerBoundaryIs>';

                        $content .= '</Polygon>';
                    }


                    $content .= '</Placemark>';
                }

            break;
        }

        $content .= '</Document>';
        $content .= '</kml>';

        //return response()->download($content,'public_map.kml');
        $headers = array(
            'Content-Type' => 'plain/txt',
            'Content-Disposition' => sprintf('attachment; filename="%s"',"public_map.kml"),
            'Content-Length' => sizeof($content),
        );
      
        return Response::make($content, 200, $headers);
    }

    public function export(Request $request)
    {
        $title  = '';
        $first  = true;
        $export = $request->input("file");

        if($export == 'geojson')
        {
            $markers = $request->input("markers");

            $dt = new \stdClass();

            $dt->type     = "FeatureCollection";
            $dt->features = [];

            $datas = DB::table("data")->whereIn('data.id',$markers);
            $datas = $datas->join("categories","data.category_id","categories.id");
            $datas = $datas->join("themes","data.theme_id","themes.id");
            $datas = $datas->select(["data.type","data.title","data.id","themes.themes_name","categories.color"])->get();

            foreach($datas as $data)
            {
                
                if ($first)
                {
                    $query = DB::table('maps')->join('map_data','maps.id','=','map_data.map_id');
                    $query = $query->where('map_data.data_id',$data->id);
                    $query = $query->select(['maps.name'])->first();

                    $title = $query->name;
                }

                $first = false;
                
                
                if($data->type == "point")
                {
                    $point = DB::table("points")->where("data_id",$data->id)->first();
                    
                    if($point !== null)
                    {
                        $mc = "marker-color";
                        
                        $mrk                        = new \stdClass();
                        $mrk->type                  = "Feature";
                        $mrk->properties            = new \stdClass();
                        $mrk->properties->$mc       = $data->color;
                        $mrk->properties->theme     = $data->themes_name;
                        $mrk->geometry              = new \stdClass();
                        $mrk->geometry->type        = "Point";
                        $mrk->geometry->coordinates = [$point->longitude,$point->latitude];

                        $dt->features[] = $mrk;
                    }

                    
                }
                else if($data->type == "polygon")
                {
                    $points = [];
                    
                    $point = DB::table("points")->where("data_id",$data->id)->get();
                    
                    foreach($point as $p)
                    {
                        $points[] = [$p->longitude,$p->latitude];
                    }

                    if(count($points) > 0)
                    {
                        $mrk                        = new \stdClass();
                        $mrk->type                  = "Feature";
                        $mrk->properties            = new \stdClass();
                        $mrk->properties->fill       = $data->color;
                        $mrk->properties->theme     = $data->themes_name;
                        $mrk->geometry              = new \stdClass();
                        $mrk->geometry->type        = "Polygon";
                        $mrk->geometry->coordinates = [$points];

                        $dt->features[] = $mrk;
                    }

                }
                else
                {
                    $points = [];
                    
                    $point = DB::table("points")->where("data_id",$data->id)->get();
                    
                    foreach($point as $p)
                    {
                        $points[] = [$p->longitude,$p->latitude];
                    }

                    if(count($points) > 0)
                    {
                        $mrk                        = new \stdClass();
                        $mrk->type                  = "Feature";
                        $mrk->properties            = new \stdClass();
                        $mrk->properties->fill       = $data->color;
                        $mrk->properties->theme     = $data->themes_name;
                        $mrk->geometry              = new \stdClass();
                        $mrk->geometry->type        = "LineString";
                        $mrk->geometry->coordinates = $points;

                        $dt->features[] = $mrk;
                    }
                }
            }

            $headers = array(
                'Content-Type' => 'plain/txt',
                'Content-Disposition' => sprintf('attachment; filename="%s"',$title.".geojson"),
                'Content-Length' => sizeof(json_encode($dt)),
            );
        
            return Response::make(json_encode($dt), 200, $headers);

        }
        else
        {
            $type    = $request->input("type");
            $markers = $request->input("markers");

            $content  = '<?xml version="1.0" encoding="UTF-8"?>';
            $content .= '<kml xmlns="http://www.opengis.net/kml/2.2">';
            $content .= '<Document>';
            
            switch($type)
            {
                case 'public':
                    
                    

                    $map = Map::where("name","Public map")->first();

                    $data = DB::table("data")->join("map_data","data.id","map_data.data_id");
                    //$data = $data->where("map_data.map_id",$map->id);
                    //$data = $data->where("data.status",1);
                    $data = $data->whereIn('data.id',$markers);
                    $data = $data->select("data.id","data.title","data.description","data.type","map_data.map_id");
                    $data = $data->get();

                    foreach ($data as $dt)
                    {
                        if($first)
                        {
                            $map = Map::where("id",$dt->map_id)->first();
                            $content .= '<name>'.$map->name.'</name><Style id="thickLine"><LineStyle><width>2.5</width></LineStyle></Style>';
                            $content .= '<Style id="transparent50Poly"><PolyStyle><color>7fffffff</color></PolyStyle></Style>';
                            $title    = $map->name;
                        }
                        $first = false;
                        
                        $content .= '<Placemark>';
                        $content .= '<name>'.$dt->title.'</name>';
                        $content .= '<description>'.$dt->title.'</description>';
                        
                        if($dt->type == "point")
                        {
                            $content .= '<Point>';

                            $point = DB::table("points")->where("data_id",$dt->id)->first();

                            if ($point == null)
                            {
                                $lat = 0;
                                $lng = 0;
                            }
                            else
                            {
                                $lat = $point->latitude;
                                $lng = $point->longitude;
                            }

                            $content .= '<coordinates>'.$lng.','.$lat.',0</coordinates>';

                            $content .= '</Point>';
                        }

                        if($dt->type == "polygon")
                        {
                            $content .= '<Polygon>';

                            $content .= '<outerBoundaryIs><LinearRing><coordinates>';

                            $points = DB::table("points")->where("data_id",$dt->id)->get();

                            foreach ($points as $point)
                            {
                                $content .= $point->longitude.','.$point->latitude.',0 ';
                            }

                            $content .= '</coordinates></LinearRing></outerBoundaryIs>';

                            $content .= '</Polygon>';
                        }

                        if($dt->type == "lines")
                        {
                            $content .= '<LineString>';

                            $content .= '<coordinates>';

                            $points = DB::table("points")->where("data_id",$dt->id)->get();

                            foreach ($points as $point)
                            {
                                $content .= $point->longitude.','.$point->latitude.',0 ';
                            }

                            $content .= '</coordinates>';

                            $content .= '</LineString>';
                        }


                        $content .= '</Placemark>';
                    }

                break;
            }

            $content .= '</Document>';
            $content .= '</kml>';

            //return response()->download($content,'public_map.kml');
            $headers = array(
                'Content-Type' => 'plain/txt',
                'Content-Disposition' => sprintf('attachment; filename="%s"',$title.".kml"),
                'Content-Length' => sizeof($content),
            );
        
            return Response::make($content, 200, $headers);

        }
        



        

    }

}
