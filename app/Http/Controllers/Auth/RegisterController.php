<?php

namespace App\Http\Controllers\Auth;

use App\Map;
use App\User;
use App\Permission;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/redirect';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'    => 'required|string|max:255',
            'surname' => 'required|string|max:255', 
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'        => $data['name'],
            'surname'     => $data['surname'],
            'email'       => $data['email'],
            'password'    => bcrypt($data['password']),
            'roles_id'    => 3,
            'email_token' => str_random(20)
        ]);

        $public_map = Map::where('name', 'Public map')->first();


        Permission::create([
            'user_id' => $user->id,
            'map_id' => $public_map->id,
            'value' => 2
        ]);

        Permission::create([
            'user_id' => $user->id,
            'map_id' => $public_map->id,
            'value' => 1
        ]);

        return $user;
    }

    /**
    * Handle a registration request for the application.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        dispatch(new SendVerificationEmail($user));
        return view("auth.verification");
    
    }

    /**
    * Handle a registration request for the application.
    *
    * @param $token
    * @return \Illuminate\Http\Response
    */
    public function verify($token)
    {
        $user = User::where("email_token",$token)->first();

        if($user == null)
        {
            return view("auth.emailconfirm",["user"=>$user,"verified"=>false,"token"=>true]);
        }

        if($user->verified == 1)
        {
            return view("auth.emailconfirm",["user"=>$user,"verified"=>true,"token"=>false]);
        }
        else
        {
            $user->verified = 1;
            
            if ($user->save())
            {
                return view("auth.emailconfirm",["user"=>$user,"verified"=>false,"token"=>false]);
            }
        }

        
    }




}
