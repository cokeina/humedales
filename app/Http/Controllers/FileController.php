<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    
    public function index()
    {
        if(Auth::check())
        {
            return view('files.index');
        }
        else
        {
            return view('files.login');
        }
        
        
    }

}
