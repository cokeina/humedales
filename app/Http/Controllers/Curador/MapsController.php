<?php

namespace App\Http\Controllers\Curador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Data;
use App\Data_image;
use App\Map;
use App\Map_cat;
use App\Map_data;
use App\Map_preferences;
use App\Map_private_preferences;
use App\MapCategoryPreference;
use App\MapThemePreference;
use App\Map_theme;
use App\Permission;
use App\Point;
use App\Rol;
use App\Theme;
use Validator;
use App\Image;
use App\Access;
use App\User;
use Auth;

class MapsController extends Controller
{

    /**
     * Muestra a todos los mapas creados en la app donde el curador tenga permisos
     *
     * @return Vista con mapas creados
     */
    public function index()
    {

        $user_id = Auth::user()->id;

        $permission = Permission::where('user_id', $user_id)->get();

        $user_maps = array();

        foreach ($permission as $key) {

            $map = Map::where('id', $key->map_id)->first();

            array_push($user_maps, $map);
        }

        $user_maps = array_unique($user_maps);

        return view('curador.maps.index', compact('user_maps'));
    }

    /**
     * Muestra un mapa privado en específico
     *
     * @param  int  $id
     * @return Vista con mapa privado, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function privateMap($id) {

        $map = Map::where('id', $id)->first();
        if (!isset($map)) {
            return back()->withErrors('El mapa seleccionado no existe');
        }

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                array_push($names, $dato->title);
                array_push($points, [$point->latitude, $point->longitude]);

            }
        }

        $map_preferences = Map_private_preferences::where('map_id', $map->id)->where('user_id',Auth::user()->id)->first();

        if($map_preferences == null)
        {
            $map_preferences = new \stdClass();
            $map_preferences->latitude  = '-41.3214705000';
            $map_preferences->longitude = '-73.0139758000';
            $map_preferences->zoom      = 10;
            $map_preferences->map_style = 'basic';
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }

        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }

        $permission = Permission::where('map_id',$map->id);
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->where('value',2)->first();

        $drawline   = 1;

        if ($permission == null) $drawline = 0;

        //DIBUJAR LINEA O POLIGONO
        $dlp = 1;

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if($access == null)
        {
            $dlp = 0;
        }

        return view('admin.maps.private', compact('map', 'themes', 'categories', 'names', 'points', 'map_preferences','clist','tlist','drawline','dlp'));
    }

    /**
     * Trae todos los puntos y su información, asociados a un mapa
     * estos puntos deben tener status 1 (aprobados)
     *
     * @param  int $id
     * @return Arreglo de puntos 
     */
    public function data($id) {

        $map_data = Map_data::where('map_id', $id)->get();

        $response = array();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }


        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 2,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 0,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

        }


        return $response;
    }

    /**
     * Almacena un nuevo dato asociado a un mapa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id de mapa
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addData(Request $request){

        $isAdmin = false;

        $permission = Permission::where('map_id',$request->input("id"));
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->whereIn('value',[2,3])->first();

        if($permission == null)
        {
            $errors = ['errors' => [['No cuentas con los suficientes permisos para mapear']]];
            return $errors;
        }

        $permission = Permission::where('map_id',$request->input("id"));
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->whereIn('value',[3])->first();

        $permission1 = Permission::where('map_id',$request->input("id"));
        $permission1 = $permission1->where('user_id',Auth::user()->id);
        $permission1 = $permission1->whereIn('value',[2])->first();

        if($permission != null && $permission1 == null && $request->input('type') == 'lines')
        {
            $errors = ['errors' => [['No cuentas con los suficientes permisos para dibujar linea']]];
            return $errors;
        }


        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }

        $title = $request->get('title');
        $description = $request->get('description');
        $categories = $request->get('categories');
        $themes = $request->get('themes');
        $type  = $request->get('type');
        $id    = $request->input("id");
        $status = $request->input("status");

        //if($status == 2) $status = 1;

        if ($permission1 != null)
        {
            //ADMINISTRADOR DEL MAPA, PUEDE PUBLICAR
            $status  = 1;
            $isAdmin = true;
        }

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id,
            'approved_by' => Auth::user()->id
        ]);

        Map_data::create([
            'map_id' => $id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');

            $coordinates = explode(",",$coordinates);

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }

        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();

                $image = new Image();
                $image->extension = $extension;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }



        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();

        $data->idAdmin = $isAdmin; //requerimiento administrador de mapa

        return $data;
    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de categoría específico
     *
     * @param  int $id de categoría, int $map corresponde a aid de mapa
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryData($map, $id) {

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if( $access == null)
        {
            $ppl = ['point'];
        }
        else  $ppl = ['point','polygon','lines'];

        $response = [];
        
        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.category_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",1);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.category_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->whereIn("data.status",[0,2]);
        $data = $data->where("data.created_by",Auth::user()->id);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        return $response;
    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de tema específico
     *
     * @param  int $id de tema, int $map corresponde a id de mapa
     * @return Arreglo de puntos de tema específico
     */
    public function showThemeData($map, $id) {

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if( $access == null)
        {
            $ppl = ['point'];
        }
        else  $ppl = ['point','polygon','lines'];

        $response = [];
        
        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.theme_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",1);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.theme_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",2);
        $data = $data->where("data.created_by",Auth::user()->id);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        return $response; 
    }

    /**
     * Muestra un mapa público en específico
     *
     * @param  int  $id
     * @return Vista con mapa publico, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function publicMap(){
        $map = Map::where('name', 'Public map')->first();

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $map->id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                if($point != null)
                {
                    array_push($names, $dato->title);
                    array_push($points, [$point->latitude, $point->longitude]);
                }

                

            }
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }


        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }

        return view('curador.maps.public', compact('map', 'themes', 'categories', 'names', 'points','clist','tlist'));
    }

    /**
     * Trae todos los datos asociados al mapa público
     * estos puntos deben tener status 1 (aprobados)
     *
     * @return Arreglo de puntos asociados a mapa público
     */
    public function publicData() {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }

        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id' => $key->data_id,
                'status' => 0,
                'created_by' => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id' => $key->data_id,
                'status' => 2,
                'created_by' => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }


        }




        return $response;
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de categoría específico
     *
     * @param  int $id de categoría
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryPublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id);
            $data = $data->whereIn('status',[1]);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null)
            { 
                if ($data->category_id == $id) {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id', $data->id)->get();

                    array_push($response, $data); 
            
                }
            }

            $data = new Data();
            $data = $data->whereId($key->data_id); 
            $data = $data->whereIn('status',[0,2]);
            $data = $data->whereIn('type',$ppl);
            $data = $data->where('created_by',Auth::user()->id);
            $data = $data->first();
            
            if ($data != null)
            { 
                if ($data->category_id == $id) 
                {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id',$data->id)->get();
            
                    array_push($response, $data); 
                            
                }
            }


        } 

        return $response; 
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de tema específico
     *
     * @param  int $id de tema
     * @return Arreglo de puntos de tema específico
     */
    public function showThemePublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id);
            $data = $data->whereIn('status',[1]);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null)
            {
                if ($data->theme_id == $id) 
                {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id', $data->id)->get();
    
                    array_push($response, $data); 
              
               }

            }

            

           $data = new Data();
           $data = $data->whereId($key->data_id); 
           $data = $data->whereIn('status',[0,2]);
           $data = $data->whereIn('type',$ppl);
           $data = $data->where('created_by',Auth::user()->id);
           $data = $data->first();
           
                       if ($data != null)
                       { 
           
                               if ($data->theme_id == $id) {
                                   $data->points = Point::where('data_id', $data->id)->get();
                                   $data->images = Image::where('data_id',$data->id)->get();
           
                                   array_push($response, $data); 
                           
                           }
                       }

        } 

        return $response; 
    }

    /**
     * Agrega datos al mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addPublicData(Request $request){

        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }
        
        $map = Map::where('name', 'Public map')->first();

        $title = $request->get('title');
        $description = $request->get('description');
        $categories = $request->get('categories');
        $themes = $request->get('themes');
        $image_file = $request->get('image');
        $image_type = $request->get('image_ext');
        $type  = $request->get('type');
        $status = $request->input("status");
        
        if($status == 2) $status = 1;

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id,
            'approved_by' => Auth::user()->id
        ]);

        Map_data::create([
            'map_id' => $map->id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }

        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();

                $image = new Image();
                $image->extension = $extension;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }

        

        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();

        return $data;
    }

    /**
     * Trae el formulario de personalización de mapa público
     *
     * @return Vista de personalización
     */
    public function personalize(){

        $pref = Map_Preferences::where('user_id',Auth::user()->id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where('name','Public map')->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }

        return view('curador.maps.personalizeForm',compact('pref','categories','mcps','themes','mtps'));
    }
    
    public function personalizeprivate($id)
    {
        $pref = Map_private_preferences::where('user_id',Auth::user()->id);
        $pref = $pref->where('map_id',$id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where("id",$id)->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }

        return view('curador.maps.personalizePrivateForm',compact('pref','map','categories','mcps','themes','mtps'));
    }

    /**
     * Define preferencias de mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Vista anterior con mensaje de éxito 
     */
    public function setPreferences(Request $request) {

        $categories = $request->get('categories'); 
        $themes     = $request->get('themes');
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');
        $zoom      = $request->get('zoom');
        $style     = $request->get('style');

        if(count($categories) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una categoría');
        }

        if(count($themes) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una tema');
        }

        if ($latitude == '') {
            $latitude = '-41.3214705000';
        }

        if ($longitude == '') {
            $longitude = '-73.0139758000';
        }

        if ($zoom == '') {
            $zoom = 10;
        }

        if ($style == '') {
            $style = 'basic';
        }

        $user = Auth::user();

        $user_id = $user->id;


        //primero eliminamos todas las preferencias que ya existen para ese usuario
        $map_key = null;

        if($request->has('map_id'))
        {
            $del     = MapCategoryPreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $request->input("map_id");
        }
        else
        {
            $map     = Map::where('name','Public map')->first();

            $del     = MapCategoryPreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $map->id;

        }

        foreach($categories as $cat)
        {
            $mcp = new MapCategoryPreference();
            $mcp->map_id      = $map_key;
            $mcp->user_id     = Auth::user()->id;
            $mcp->category_id = $cat;
            $mcp->save(); 
        }

        foreach($themes as $thm)
        {
            $mtp = new MapThemePreference();
            $mtp->map_id      = $map_key;
            $mtp->user_id     = Auth::user()->id;
            $mtp->theme_id = $thm;
            $mtp->save(); 
        }

        if($request->has('map_id'))
        {
            $map_preferences = Map_private_preferences::where('user_id', $user_id);
            $map_preferences = $map_preferences->where('map_id',$request->input('map_id'))->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_private_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id,
                    'map_id'    => $request->input('map_id')
                ]);
            }    
        }
        else
        {
            $map_preferences = Map_preferences::where('user_id', $user_id)->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id
                ]);
            }
        }

        $permission = Permission::where('user_id', $user_id)->get();

        $user_maps = array();

        foreach ($permission as $key) {

            $map = Map::where('id', $key->map_id)->first();

            array_push($user_maps, $map);
        }

        $user_maps = array_unique($user_maps);

        $len = count($user_maps);
        for ($i=0; $i < $len; $i++) { 
            if ($user_maps[$i]['name'] == 'Public map'){
                unset($user_maps[$i]);
            }
        }

        return back()->with('status', 'Las preferencias han sido guardadas');
    }
}
