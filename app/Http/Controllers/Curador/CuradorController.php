<?php

namespace App\Http\Controllers\Curador;

use Illuminate\Http\Request;
use App\Mail\SendRejectMail;
use App\Mail\SendPubMail;
use App\Http\Controllers\Controller;
use App\Data;
use App\Category;
use App\Theme;
use App\Map;
use App\Map_data;
use App\Permission;
use App\User;
use App\Rol;
use App\Image;
use App\Point;
use DB;
use Auth;
use Mail;

class CuradorController extends Controller
{

    /**
     * Muestra vista principal perfil curador
     *
     * @return vista principal perfil curador
     */
    public function dashboard()
    {

    	$user = Auth::user();

    	$permissions = Permission::where('user_id', $user->id)->get();

    	$admin_data = 0;
    	$invite = 0;

    	foreach($permissions as $key) {
    		if($key->value = 2) {
    			$admin_data = 2;
    		} 

    		if($key->value = 3) {
    			$invite = 3;
    		} 
    	}

    	return view('curador.index', compact('admin_data', 'invite'));
    }

    /**
     * muestra todos los mapas creados en app
     *
     * @return vista con todos los mapas creados
     */
    public function maps()
    {
    	$maps = Map::all();

    	return view('curador.maps.index', compact('maps'));
    }

    /**
     * muestra todos los usuarios creados en la app
     *
     * @return vista con todos los usuarios creados
     */
    public function users() 
    {
    	$user = Auth::user();

    	$permissions = Permission::where('user_id', $user->id)->get();

    	$admin_data = 0;
    	$invite = 0;

    	foreach($permissions as $key) {
    		if($key->value == 2) {
    			$admin_data = 2;
    		} 

    		if($key->value == 3) {
    			$invite = 3;
    		} 
    	}

    	$users = User::where('roles_id','=', 3)->get();

    	return view('curador.users.index', compact('admin_data', 'invite', 'users'));
    }

    /**
     * Busca el usuario seleccionado y trae su información, además de traer todos los mapas creados en la app
     * para generar permisos sobre estos
     *
     * @param  int $id
     * @return Vista de edición de usuario con datos de usuario, su rol, mapas y permisos
     */
    public function editUser($id) {
    	$user = User::whereId($id)->first();

        $maps = Map::where('name', '!=', 'Public map')->get();

    	$roles = Rol::all();

        foreach($maps as $map) {

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 1
            ];
            $map->see = Permission::where($matchRequest)->first();

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 2
            ];
            $map->admin = Permission::where($matchRequest)->first();

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 3
            ];
            $map->invite = Permission::where($matchRequest)->first();
        }

        $rol_selected = Rol::whereId($user->roles_id)->first();

        $rol_selected = $rol_selected->rol_name;

    	return view('curador.users.edit', compact('user','roles', 'maps', 'rol_selected', 'permisions'));
    }

    /**
     * Actualiza a un usuario con el id seleccionado, dentro de la actualización se consideran los permisos para
     * los mapas seleccionados,  además de cambiar el rol asociado a este
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Vuelve a vista de edición de usuario más mensaje de edición exitosa
     */
    public function updateUser($id, Request $request){

        $sees     = $request->get('see');
        $admins   = $request->get('admin');
        $invites  = $request->get('invite');

        $user    = User::whereId($id)->first();

        $permissions = Permission::all();

        foreach($permissions as $permission) {
            if ($permission->user_id == $id) {
                $permission->delete();
            }
        }

        if ($sees != null) {
            foreach ($sees as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }

        if ($admins != null) {
            foreach ($admins as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }    

        if ($invites != null) {
            foreach ($invites as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }

        return back()->with('status', 'El usuario ha sido editado');
    }

    /**
     * Muestra todos los datos creados en la app
     *
     * @return Vista de datos con todos los puntos de la app más usuario creador, cateogría y tema, además de mapa al que 
     * pertenecen
     */
    public function showData(){

        $category = Category::all();
        $theme    = Theme::all(); 
        $users    = User::all();
        $maps     = Map::all();
        
        
        /*$user = Auth::user();

    	$user_id = $user->id;

    	$matchThese = [
    		'user_id' => $user_id,
    		'value'   => 2
    	];

    	$permissions = Permission::where($matchThese)->get();

    	$data_id = [];

        $public_map = Map::where('name', 'Public map')->first();
        $cross = Map_data::where('map_id', $public_map->id)->get();   

        foreach($cross as $as) {
            array_push($data_id, $as->data_id);
        }

    	foreach($permissions as $key) {

    		$cross = Map_data::where('map_id', $key->map_id)->get();
    		foreach($cross as $as) {
    			array_push($data_id, $as->data_id);
    		}	
    	}

    	$data = [];
    	foreach($data_id as $key) {

    		$response = Data::whereId($key)->first();

    		array_push($data, $response);
        }
        
        if ($data != null) {
            foreach($data as $key){

                $map = Map_data::where('data_id', $key->id)->first();
                $map = Map::whereId($map->map_id)->first();
                $user = User::whereId($key->created_by)->first();
                $category = Category::whereId($key->category_id)->first();
                $theme = Theme::whereId($key->theme_id)->first();

                $key->map = $map->name;

                if($user != null ) $key->user = $user->name;
                $key->category = $category->category_name;
                $key->theme = $theme->themes_name;

                $key->map_id  = $map->id;
                
                                if($map->name == 'Public map') $key->public = 1;
                                else $key->public = 0;
                
                                $point = Point::where('data_id',$key->id)->first();

                                if($point != null)
                                {
                                    $key->latitude  = $point->latitude;
                                    $key->longitude = $point->longitude; 
                                }
                
                                

            }
        }*/

        return view('curador.data.index', compact('category','theme','maps','users'));
    }

    public function grid(Request $request)
    {
        $lst = [];
        
        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = $request->input('length');
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];
                
        $with = $request->input('with');
        
        $status   = $request->input('status');
        $category = $request->input('category');
        $theme    = $request->input('theme');
        $map      = $request->input('map');
        $user     = $request->input('user');
        $title    = $request->input('title');
        $created  = $request->input('created');
        $pub      = $request->input('pub');
        $offset   = $request->input('offset');
        $offsetp   = $request->input('offsetp');

        $type     = $request->input('type'); 

        $datas = new Data();
        $datas  = $datas->join("map_data","data.id","=","map_data.data_id");
        $datas  = $datas->join("maps","map_data.map_id","=","maps.id");
        $datas  = $datas->join("themes","data.theme_id","=","themes.id");
        $datas  = $datas->join("categories","data.category_id","=","categories.id");
        $datas  = $datas->join("users","data.created_by","=","users.id");

        $list = DB::table('data')->whereIn('status',[0,3]);
        $list = $list->where('created_by','<>',Auth::user()->id)->get();

        foreach($list as $l)
        {
            $lst[] = $l->id;
        }

        $datas  = $datas->whereNotIn('data.id',$lst);
        

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                if($field != 'id') $datas = $datas->orderBy($field, $dir);
            }

            if($status != 99)
            {
                $datas = $datas->where("data.status","=",DB::raw($status));
            }

            if($category != 0)
            {
                $datas = $datas->where("data.category_id","=",DB::raw($category));
            }

            if($theme != 0)
            {
                $datas = $datas->where("data.theme_id","=",DB::raw($theme));
            }

            if($map != '0')
            {
                $datas = $datas->where("map_data.map_id","=",$map);
            }

            if($user != 0)
            {
                $datas = $datas->where("data.created_by","=",$user);
            }

            if($title != '')
            {
                $datas = $datas->where("data.title","like",'%'.$title.'%');
            }

            if($created != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.created_at + INTERVAL ".$offset." MINUTE  )"),"=",$created);
            }

            if($pub != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.approved_at + INTERVAL ".$offsetp." MINUTE  )"),"=",$pub);
            }

            if($type == 'curator')
            {
                $mids = [];
                
                $user = Auth::user();
                
                $user_id = $user->id;
                
                $matchThese = [
                            'user_id' => $user_id,
                            'value'   => 2
                ];
                
                $permissions = Permission::where($matchThese)->get();

                $public_map = Map::where('name', 'Public map')->first();

                $mids[] = $public_map->id;

                foreach($permissions as $prm)
                {
                    $mids[] = $prm->map_id;
                }

                $datas = $datas->whereIn("map_data.map_id",$mids);

            }


            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $datas = $datas->select(['data.id',DB::raw("IF(maps.name = 'Public map',1,0) as flag"),'data.created_at','data.approved_at','data.title','categories.category_name as cat','themes.themes_name as theme','maps.name as map',DB::raw('CONCAT(users.name," ",IFNULL(users.surname,"")) as user'),'data.status','maps.id as map_id']);

            $data->datas = $datas->get();

            $datas = $datas->orderBy(DB::raw('flag'),'desc');
            $datas = $datas->orderBy('maps.name','asc');

            foreach($data->datas as $dt)
            {
                $point = Point::where('data_id',$dt->id)->first();

                if($point != null)
                {
                    $dt->latitude  = $point->latitude;
                    $dt->longitude = $point->longitude;
                }
                
                 
            }

            $data->recordsTotal = Data::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }

    }

    /**
     * Destruye un punto asociado a un mapa 
     *
     * @param  int $id 
     * @return Vista anterior con mensaje de éxito 
     */
    public function destroyData($id){

        $data_image = Image::where('data_id', $id)->get();
        if ($data_image != null) {
            foreach($data_image as $key){
                $key->delete();
            }
        }

        $points = Point::where('data_id', $id)->get();
        if ($points != null) {
            foreach($points as $key){
                $key->delete();
            }
        }

        $map_data = Map_data::where('data_id', $id)->get();
        if ($map_data != null) {
            foreach($map_data as $key){
                $key->delete();
            }
        }

        $data = Data::whereId($id)->first();
        $data->delete();

        return back()->with('status', 'Se ha eliminado el dato seleccionado');
    }

    /**
     * Trae un punto específico
     *
     * @param  int $id 
     * @return Vista edición de dato
     */
    public function editData($id){

        $data = Data::whereId($id)->first();

        $point = Point::where("data_id",$data->id)->first();
        
        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }
        
        $mdata = Map_data::where("data_id",$data->id)->first();
        
        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();
        
        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();

        return view('curador.data.edit', compact('data','point','cat','themes'));
    }

    public function seeData($id){

        $data = Data::whereId($id)->first();

        $point = Point::where("data_id",$data->id)->first();
        
        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }
        
        $mdata = Map_data::where("data_id",$data->id)->first();
        
        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();
        
        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();

        return view('curador.data.show', compact('data','point','cat','themes'));
    }

    /**
     * Actualiza información de dato, se puede actualizar titulo, descripción y estado de dato (publicado, rechazado)
     *
     * @param  int $id 
     * @param  \Illuminate\Http\Request  $request
     * @return Vista anterior con mensaje de éxito 
     */
    public function storeData($id, Request $request) {

        $title = $request->get('title');
        $description = $request->get('description');
        $status = $request->get('status');
        $category = $request->get("category");
        $theme    = $request->get("theme");

        $data = Data::whereId($id)->first();

        if($description != null) {
            $data->description = $description;
        }

        if($data->type == 'point')
        {
            $point = Point::where('data_id',$data->id)->first();

            if($point != null)
            {
                $point->latitude  = $request->input("latitude");
                $point->longitude = $request->input("longitude");

                $point->save();
            }

        }

        $data->title = $title;
        if($status != 3) $data->status = $status;
        if($status == 1) $data->approved_by = Auth::user()->id;
        $data->category_id = $category;
        $data->theme_id    = $theme;
        $data->save();

        if($data->status == 1)
        {
            $user = User::find($data->created_by);
            $map  = Map_data::where('data_id',$data->id)->first();
            $map  = Map::find($map->map_id);

            Mail::to($user->email)->queue(new SendPubMail($map,$user,$data));
        }

        if($status == 1) return redirect("curador/data");

        return back()->with('status', 'El dato ha sido actualizado');
    }

    /**
     * Aprueba un dato cambiando el estado de este
     *
     * @param  int $id 
     * @return Vista anterior con mensaje de éxito 
     */
    public function approveData($id){

        $data = Data::whereId($id)->first();

        $data->status      = 1;
        $data->approved_by = Auth::user()->id;

        $data->save();

        $user = User::find($data->created_by);
        $map  = Map_data::where('data_id',$data->id)->first();
        $map  = Map::find($map->map_id);

        Mail::to($user->email)->queue(new SendPubMail($map,$user,$data));

        return back()->with('status', 'El dato ha sido aprobado');
    }

    public function rejectData(Request $request,$id)
    {

        $data = Data::find($id);

        if($data == null)
        {
            return response()->json([
                'success' => true,
                'code' => 404,
                'message' => 'No se encuentra el dato seleccionado',
                'data' => null
            ], 200);
        }

        if($data->status == 3)
        {
            return response()->json([
                'success' => true,
                'code' => 400,
                'message' => 'Dato ya se encuentra rechazado',
                'data' => null
            ], 200);
        }

        $user = User::find($data->created_by);

        $cause  = $request->input('cause');
        $causet = $request->input('other');

        $data->status = 3;
        $data->save();

        Mail::to($user->email)->queue(new SendRejectMail($user,$data,$cause,$causet));

        

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Dato rechazado con éxito',
            'data' => null
        ], 200);

    }

    public function multiClone(Request $request)
    {

        $success = true;
        $msg     = '';

        $map  = Map::where('name','Public map')->first();

        $ids = $request->input('ids');

        $ids = explode(',',$ids);

        //UNA VALIDACION ADICIONAL ES QUE TODOS LOS PUNTOS A CLONAR DEBEN ENCONTRARSE 
        //EN UN MAPA DONDE EL USUARIO SEA ADMINISTRADOR DEL MAPA EN CUESTION

        foreach($ids as $id)
        {
            $info = Map_data::where('data_id',$id)->first();

            if($info == null)
            {
                $msg     = 'Uno de los datos presentes no existe';
                $success = false;
                break; 
            }

            if($info->map_id == $map->id)
            {
                $msg     = 'Uno de los datos a clonar ya se encuentra en el mapa público';
                $success = false;
                break;
            }

            $permission = Permission::where('map_id',$info->map_id);
            $permission = $permission->where('user_id',Auth::user()->id);
            $permission = $permission->where('value',2)->first();

            if ($permission == null)
            {
                $msg     = 'Uno de los datos a clonar pertenece a un mapa en el cual no eres administrador';
                $success = false;
                break;
            }


        }

        if($success)
        {

            foreach($ids as $id)
            {
                $data = Data::where('id',$id)->first();

                $newData              = new Data();
                $newData->title       = $data->title;
                $newData->description = $data->description;
                $newData->created_by  = Auth::user()->id;
                $newData->type        = $data->type;
                $newData->theme_id    = $data->theme_id;
                $newData->category_id = $data->category_id;
                $newData->status      = 2; //EN REVISIÓN

                $newData->save();

                //asociamos punto/poligono a mapa 
                $mapData           = new Map_data();
                $mapData->map_id   = $map->id;
                $mapData->data_id  = $newData->id;
                $mapData->save();

                //copiamos coordenadas 
                $points = Point::where('data_id',$data->id)->get();

                foreach ($points as $point)
                {
                    $newPoint = new Point();
                    $newPoint->latitude  = $point->latitude;
                    $newPoint->longitude = $point->longitude;
                    $newPoint->data_id   = $newData->id;
                    $newPoint->save();
                }

                $images = Image::where('data_id',$data->id)->get();

                foreach ($images as $image)
                {
                    $newImage = new Image();
                    $newImage->extension = $image->extension;
                    $newImage->filename  = $image->filename;
                    $newImage->data_id   = $newData->id;
                    $newImage->save();
                    
                    Storage::copy('points/'.$image->id.'.'.$image->extension,'points/'.$newImage->id.'.'.$newImage->extension);

                }

            }

        }

        return response()->json([
            'success' => $success,
            'code' => 200,
            'message' => $msg,
            'data' => null
        ], 200);
        
    }


}
