<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Initiative;
use App\Infraestructure;
use App\Initiative_Category;
use App\Initiative_Theme;
use App\Manager;
use App\Material;
use App\Photo;
use App\Product;
use App\Recognition;
use App\Req_money;
use App\Req_service;
use App\Reward;
use App\Service;
use App\Task;
use Auth;
use DateTime;

class InitiativesController extends Controller
{
    /**
     * Muestra todas las iniciativas aprobadas
     *
     * @return Vista donde se listan iniciativas
     */
    public function index()
    {

        $initiatives = Initiative::where('status_id', 5)->get();


        foreach($initiatives as $initiative) {

            $req_money = Req_money::where('initiative_id', $initiative->id)->first();
            $initiative->goal = $req_money->takings_goal;
            $initiative->actual = $req_money->takings_actual;
            $initiative->actual = ($initiative->actual * 100) / $initiative->goal;

            $photo = Photo::where('initiative_id', $initiative->id)->first();
            $initiative->image = $photo->photo_file;
            $initiative->image_ext = $photo->photo_file;
        }

        return view('initiatives', compact('initiatives'));
    }

    /**
     * Creación de iniciativa - primer paso
     *
     * @return Vista para crear iniciativa - primer paso
     */
    public function createFirstStep()
    {
        if(!Auth::check())
        {
            return redirect('/login')->withErrors('Debes iniciar sesión para crear una iniciativa');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(1);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/second-step');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(2);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/third-step');
        }

        return view('initiatives.first-step');
    }

    /**
     * Creación de iniciativa - segundo paso
     *
     * @return Vista para crear iniciativa - segundo paso
     */
    public function createSecondStep()
    {
        if(!Auth::check())
        {
            return redirect('/login')->withErrors('Debes iniciar sesión para crear una iniciativa');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(1);
        if (isset($initiative_id)) {
            return view('initiatives.second-step', compact('initiative_id'));
        }

        $initiative_id = $this->userHasIncompletesInitiatives(2);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/third-step');
        }

        return redirect('/initiatives/create/first-step');
    }

    /**
     * Creación de iniciativa - tercer paso
     *
     * @return Vista para crear iniciativa - tercer paso
     */
    public function createThirdStep()
    {
        if(!Auth::check())
        {
            return redirect('/login')->withErrors('Debes iniciar sesión para crear una iniciativa');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(1);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/second-step');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(2);
        if (isset($initiative_id)) {
            return view('initiatives.third-step', compact('initiative_id'));
        }

        return redirect('/initiatives/create/first-step');
    }

    /**
     * Creación de iniciativa - ultimo paso
     *
     * @return Vista para crear iniciativa - ultimo paso
     */
    public function createLastStep()
    {
        if(!Auth::check())
        {
            return redirect('/login')->withErrors('Debes iniciar sesión para crear una iniciativa');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(1);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/second-step');
        }

        $initiative_id = $this->userHasIncompletesInitiatives(2);
        if (isset($initiative_id)) {
            return redirect('/initiatives/create/third-step');
        }

        return view('initiatives.last-step');
    }

    /**
     * Almacena información de creación de primer paso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirige a ruta de segundo paso
     */
    public function storeFirstStep(Request $request)
    {
        $name           = Auth::user()->name;
        $email          = Auth::user()->email;
        $user_id        = Auth::user()->id;

        $title          = $request->get('title');
        $resume         = $request->get('resume');
        $motivation     = $request->get('resume');
        $description    = $request->get('description');
        $start_date      = $request->get('start_date');
        $end_date       = $request->get('end_date');
        $url_web        = $request->get('web-url');
        $url_fb         = $request->get('fb-url');
        $url_tw         = $request->get('tw-url');
        $url_yt         = $request->get('yt-url'); 

        $categories = $request->get('categories');
        $themes = $request->get('themes');

        $photo_name = $request->get('filenames_photos');
        $filetypes_photos = $request->get('filetypes_photos');
        $filebs_photos = $request->get('filebs_photos');

        if(!isset($title)) {
            return back()->withErrors('Debes ingresar un titulo para tu iniciativa')->withInput();
        }

        if(!isset($resume)) {
            return back()->withErrors('Debes ingresar un resumen para tu iniciativa')->withInput();
        }

        if(!isset($motivation)) {
            return back()->withErrors('Debes ingresar una motivación para tu iniciativa')->withInput();
        }

        if(!isset($description)) {
            return back()->withErrors('Debes ingresar una descripción para tu iniciativa')->withInput();
        }

        if(!isset($start_date) || !isset($end_date)) {
            return back()->withErrors('Debes agregar una fecha de inicio y fin a tu iniciativa')->withInput();
        }

        if(!isset($categories)) {
            return back()->withErrors('Debes seleccionar al menos una categoría')->withInput();
        }

        if(!isset($themes)) {
            return back()->withErrors('Debes seleccionar al menos un tema')->withInput();
        }

        $len_photos = count($photo_name);
        if( ( $len_photos != count($filetypes_photos)) || ($len_photos != count($filebs_photos)) ) {
            return back()->withErrors('Debes agregar al menos una foto sobre tu iniciativa')->withInput();
        } 

        if (!isset($filebs_photos[0])) {
            return back()->withErrors('Debes agregar al menos una foto sobre tu iniciativa')->withInput();
        }

        $created_initiative = Initiative::create([
            'initiative_name'        => $name,
            'email'                  => $email,
            'initiative_title'       => $title,
            'initiative_resume'      => $resume,
            'initiative_motivation'  => $motivation,
            'initiative_description' => $description,
            'start_date'             => $start_date,
            'finish_date'            => $end_date,
            'url_web'                => $url_web,
            'url_fb'                 => $url_fb,
            'url_tw'                 => $url_tw,
            'url_yt'                 => $url_yt,
            'user_id'                => $user_id,
            'category_id'            => $categories[0],
            'theme_id'               => $themes[0], 
            'status_id'              => 1

        ]);

        $managments = $request->get('managments');

        foreach($managments as $manager){
            Manager::create([
                'manager_name' => $manager,
                'initiative_id' => $created_initiative->id
            ]);
        }

        for ($i=0; $i < $len_photos; $i++) { 
            
            Photo::create([
                'photo_name'   => $photo_name[$i],
                'photo_type'  => $filetypes_photos[$i],
                'initiative_id' => $created_initiative->id,
                'photo_file'   => $filebs_photos[$i]
            ]);
        }

        return redirect('/initiatives/create/second-step');

    }

    /**
     * Almacena información de creación de segundo paso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirige a ruta de tercer paso
     */
    public function storeSecondStep(Request $request)
    {
        $initiative_id = $request->get('initiative');

        $money_total = $request->get('money_total');
        $money_opt   = $request->get('money_opt');
        $money_min   = $request->get('money_min');

        if (!isset($money_total)) {
            return back()->withErrors('Debes definir un monto total en tu iniciativa')->withInput();
        }

        if (!isset($money_opt)) {
            return back()->withErrors('Debes definir un monto óptimo en tu iniciativa')->withInput();
        }

        if (!isset($money_min)) {
            return back()->withErrors('Debes definir un monto mínimo en tu iniciativa')->withInput();
        }

        if ($money_total < $money_min || $money_total < $money_opt) {
            return back()->withErrors('El monto total debe ser mayor al mínimo y el óptimo')->withInput();
        }

        if ($money_opt < $money_min) {
            return back()->withErrors('El monto óptimo debe ser mayor al mínimo')->withInput();
        }

        $counted_total = 0;

        $material_amounts = $request->get('material_amount');
        if (isset($material_amounts)) {

            foreach ($material_amounts as $material_amount) {
                $material_amount = (int)substr($material_amount, 1, strlen($material_amount));
                $counted_total += $material_amount;
            }
        }

        $task_amounts = $request->get('task_amount');
        if (isset($task_amounts)) {

            foreach ($task_amounts as $task_amount) {
                $task_amount = (int)substr($task_amount, 1, strlen($task_amount));
                $counted_total += $task_amount;
            }
        }

        $inf_amounts = $request->get('infraestructure_amount');
        if (isset($inf_amounts)) {

            foreach ($inf_amounts as $inf_amount) {
                $inf_amount = (int)substr($inf_amount, 1, strlen($inf_amount));
                $counted_total += $inf_amount;
            }
        }

        if ($counted_total == 0) {
            return back()->withErrors('Debes hacer el desglose del monto seleccionado')->withInput();
        }

        if ($counted_total < $money_total) {
            return back()->withErrors('La suma del desglose debe ser igual al monto total requerido')->withInput(); 
        }

        Req_money::create([
            'takings_goal' => $money_total,
            'takings_actual' => 0,
            'takings_minimum' => $money_min,
            'takings_optimum' => $money_opt,
            'initiative_id' => $initiative_id
        ]);

        $material_name        = $request->get('material_name');
        $material_description = $request->get('material_description');
        $material_type        = $request->get('material_type');
        $material_amount      = $request->get('material_amount');  

        $len_material         = count($material_amount);       
        for ($i=0; $i < $len_material; $i++) { 
            Material::create([
                'material_name' => $material_name[$i],
                'material_description' => $material_description[$i],
                'material_money' => (int)substr($material_amount[$i], 1, strlen($material_amount[$i])),
                'material_importance' => $material_type[$i],
                'initiative_id' => $initiative_id
            ]);
        }

        $task_name        = $request->get('task_name');
        $task_description = $request->get('task_description');
        $task_type        = $request->get('task_type');
        $task_amount      = $request->get('task_amount');  

        $len_task         = count($task_amount);       
        for ($i=0; $i < $len_task; $i++) { 
            Task::create([
                'task_name' => $task_name[$i],
                'task_description' => $task_description[$i],
                'task_money' => (int)substr($task_amount[$i], 1, strlen($task_amount[$i])),
                'task_importance' => $task_type[$i],
                'initiative_id' => $initiative_id
            ]);
        }

        $infraestructure_name        = $request->get('infraestructure_name');
        $infraestructure_description = $request->get('infraestructure_description');
        $infraestructure_type        = $request->get('infraestructure_type');
        $infraestructure_amount      = $request->get('infraestructure_amount'); 

        $len_infraestructure         = count($infraestructure_amount);        
        for ($i=0; $i < $len_infraestructure; $i++) { 
            Infraestructure::create([
                'infraestructure_name' => $infraestructure_name[$i],
                'infraestructure_description' => $infraestructure_description[$i],
                'infraestructure_money' => (int)substr($infraestructure_amount[$i], 1, strlen($infraestructure_amount[$i])),
                'infraestructure_importance' => $infraestructure_type[$i],
                'initiative_id' => $initiative_id
            ]);
        }

        $serv_names = $request->get('servs');
        $len_serv  = count($serv_names);
        if ($len_serv != 0) {
            foreach($serv_names as $serv) {
                Req_service::create([
                    'service_name' => $serv,
                    'status' => 0,
                    'initiative_id' => $initiative_id
                ]);
            }
        }

        $good_names = $request->get('goods');
        $len_good  = count($good_names);
        if ($len_good != 0) {
            foreach($good_names as $good) {
                Asset::create([
                    'asset_name' => $serv,
                    'status' => 0,
                    'initiative_id' => $initiative_id
                ]);
            }
        }

        $initiative_update = Initiative::whereId($initiative_id)->first();

        $initiative_update->status_id = 2;

        $initiative_update->save();

        return redirect('/initiatives/create/third-step');

    }

    /**
     * Almacena información de creación de tercer paso
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirige a ruta de cuarto paso
     */
    public function storeThirdStep(Request $request)
    {
        $initiative_id = $request->get('initiative');


        /* RECOMPENSAS */
        $reward_name = $request->get('reward_name');
        $reward_description = $request->get('reward_description');
        $reward_amount = $request->get('reward_amount');
        $photo_reward = $request->get('filebs_rewards');
        $photo_type_reward = $request->get('filetypes_rewards');

        if ($reward_name[0] == null) {
            return back()->withErrors('Debes ingresar un nombre a tu recompensa');
        }

        if ($reward_description[0] == null) {
            return back()->withErrors('Debes ingresar una descripción a tu recompensa');            
        }

        if ($reward_amount[0] == null) {
            return back()->withErrors('Debes ingresar un monto a tu recompensa');            
        }

        if ($photo_reward[0] == null) {
            return back()->withErrors('Debes ingresar una foto de tu recompensa');            
        }

        if (count($reward_name) != count($reward_description)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        if (count($reward_name) != count($reward_amount)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        if (count($reward_name) != count($photo_reward)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        if (count($reward_description) != count($reward_amount)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        if (count($reward_description) != count($photo_reward)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        if (count($reward_amount) != count($photo_reward)) {
            return back()->withErrors('Todos los campos de las recompensas son obligatorios');  
        }

        /* PRODUCTOS */
        $product_name = $request->get('product_name');
        $product_description = $request->get('product_description');
        $product_amount = $request->get('product_amount');
        $filetypes_product = $request->get('filetypes_products');
        $filebs_product = $request->get('filebs_products');

        if (count($product_name) != count($product_description)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        if (count($product_name) != count($product_amount)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        if (count($product_name) != count($filebs_product)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        if (count($product_description) != count($filebs_product)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        if (count($product_description) != count($product_amount)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        if (count($product_amount) != count($filebs_product)) {
            return back()->withErrors('Si deseas agregar un producto, todos sus campos son obligatorios');  
        }

        /* SERVICIOS */
        $service_name = $request->get('service_name');
        $service_description = $request->get('service_description');
        $service_amount = $request->get('service_amount');
        $filetypes_service = $request->get('filetypes_service');
        $filebs_service = $request->get('filebs_service');

        if (count($service_name) != count($service_description)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        if (count($service_name) != count($service_amount)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        if (count($service_name) != count($filebs_service)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        if (count($service_description) != count($filebs_service)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        if (count($service_description) != count($service_amount)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        if (count($service_amount) != count($filebs_service)) {
            return back()->withErrors('Si deseas agregar un servicio, todos sus campos son obligatorios');  
        }

        /* RECONOCIMIENTOS */
        $recognition_name = $request->get('recognition_name');
        $recognition_description = $request->get('recognition_description');
        $recognition_amount = $request->get('recognition_amount');
        $filetypes_recognition = $request->get('filetypes_recognition');
        $filebs_recognition = $request->get('filebs_recognition');

        if (count($recognition_name) != count($recognition_description)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }

        if (count($recognition_name) != count($recognition_amount)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }

        if (count($recognition_name) != count($filebs_recognition)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }

        if (count($recognition_description) != count($filebs_recognition)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }

        if (count($recognition_description) != count($recognition_amount)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }

        if (count($recognition_amount) != count($filebs_recognition)) {
            return back()->withErrors('Si deseas agregar un reconocimiento, todos sus campos son obligatorios');  
        }


        $count_reward = count($reward_name);
        for ($i = 0; $i < $count_reward; $i++) {
            Reward::create([
                'reward_name' => $reward_name[$i],
                'reward_description' => $reward_description[$i],
                'reward_image' => $photo_reward[$i],
                'reward_amount' => $reward_amount[$i],
                'reward_type' => $photo_type_reward[$i],
                'initiative_id' => $initiative_id
            ]);
        }

        if ($product_name[0] != null) {
            $count_recognition = count($product_name);
            for ($i = 0; $i < $count_product; $i++) {
                Product::create([
                    'product_name' => $product_name[$i],
                    'product_description' => $product_description[$i],
                    'product_image' => $photo_product[$i],
                    'product_mount' => $filebs_product[$i],
                    'product_type' => $filebs_product[$i],
                    'initiative_id' => $initiative_id
                ]);
            }
        }

        if ($service_name[0] != null) {
            $count_service = count($service_name);
            for ($i = 0; $i < $count_service; $i++) {
                Service::create([
                    'service_name' => $service_name[$i],
                    'service_description' => $service_description[$i],
                    'service_image' => $photo_service[$i],
                    'service_amount' => $filebs_service[$i],
                    'status' => 1,
                    'service_type' => $filebs_service[$i],
                    'initiative_id' => $initiative_id
                ]);
            }
        }

        if ($recognition_name[0] != null) {
            $count_recognition = count($recognition_name);
            for ($i = 0; $i < $count_recognition; $i++) {
                Recognition::create([
                    'recognition_name' => $recognition_name[$i],
                    'recognition_description' => $recognition_description[$i],
                    'recognition_image' => $photo_recognition[$i],
                    'recognition_amount' => $filebs_recognition[$i],
                    'recognition_type' => $filebs_recognition[$i],
                    'initiative_id' => $initiative_id
                ]);
            }
        }

        $initiative_update = Initiative::whereId($initiative_id)->first();

        $initiative_update->status_id = 3;

        $initiative_update->save();

        return redirect('/initiatives/create/last-step');

    }

    /**
     * Muestra una iniciativa en específico
     *
     * @param  int  $id
     * @return Vista con detalle de iniciativa
     */
    public function show($id)
    {
        $initiative = Initiative::whereId($id)->first();

        if(!$initiative) {
            return redirect('/initiatives');
        }

        $datetime1 = $initiative->start_date;
        $datetime2 = $initiative->finish_date;

        $datetime1 = new DateTime($datetime1);
        $datetime2 = new DateTime($datetime2);

        $initiative->diff = $datetime1->diff($datetime2);

        $money = Req_money::where('initiative_id', $initiative->id)->first();
        $initiative->optimum = $money->takings_optimum;
        $initiative->actual  = $money->takings_actual;
        $initiative->minimum = $money->takings_minimum;

        $photos = Photo::where('initiative_id', $initiative->id)->get();

        $materials = Material::where('initiative_id', $initiative->id)->get();

        $tasks = Task::where('initiative_id', $initiative->id)->get();

        $infraestructures = Infraestructure::where('initiative_id', $initiative->id)->get();

        $services = Req_service::where('initiative_id', $initiative->id)->get();

        $assets = Asset::where('initiative_id', $initiative->id)->get();

        $managers = Manager::where('initiative_id', $initiative->id)->get();

        $rewards = Reward::where('initiative_id', $initiative->id)->get();

        return view('initiatives.detail', compact('initiative', 'photos', 'materials', 'tasks', 'infraestructures', 'services', 'assets', 'managers', 'rewards'));
    }


    /** 
    * Función privada que verifica si usuario tiene una iniciativa incompleta, en caso de existir, redirige 
    * a paso espeficio para que termine iniciativa creada anteriormente
    *  
    * @param  int $status de iniciativa
    * @return int $id_iniciativa en caso de que exista iniciativa incompleta, null en caso contrario
    **/
    private function userHasIncompletesInitiatives($status){

        $response = null;
        $initiatives = Auth::user()->initiatives;
        foreach($initiatives as $initiative) {
            if ($initiative->status_id == $status) {
                $response = $initiative->id;
            }
        }

        return $response;
    }
}
