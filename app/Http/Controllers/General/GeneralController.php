<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Initiative;
use App\Photo;
use App\Req_money;
use App\Rejection;
use App\Status;
use Auth;

class GeneralController extends Controller
{

    /**
     * Muestra vista principal perfil general
     *
     * @return vista principal perfil general
     */
    public function dashboard()
    {
    	return view('general.index');
    }

    /**
     * Muestra vista con iniciativas creadas por usuario
     *
     * @return vista iniciativas creadas
     */
    public function initiatives()
    {

    	$user = Auth::user();

    	$initiatives = Initiative::where('user_id', $user->id)->get();

    	foreach ($initiatives as $initiative) {

    		$image = Photo::where('initiative_id', $initiative->id)->first();
    		$initiative->image = $image->photo_file;
    		$initiative->image_ext = $image->photo_type;

            $money = Req_money::where('initiative_id', $initiative->id)->first();
            if ($money != null ) {
                $initiative->optimum = $money->takings_optimum;
                $initiative->actual  = $money->takings_actual;
            } else {
                $initiative->optimum = 0;
                $initiative->actual = 0;
            }

            $status = Status::whereId($initiative->status_id)->first();
            $initiative->status = $status->status_name;

            $rejection = Rejection::where('initiative_id', $initiative->id)->first();
            if ($rejection != null) {
                $initiative->rejection_description = $rejection->rejection_description;
            }
    	}


    	return view('general.initiatives.index', compact('initiatives'));

    }
}
