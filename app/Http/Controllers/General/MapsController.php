<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Data;
use App\Data_image;
use App\Map;
use App\Map_cat;
use App\Map_data;
use App\Map_preferences;
use App\Map_private_preferences;
use App\MapCategoryPreference;
use App\MapThemePreference;
use App\Map_theme;
use App\Permission;
use Carbon\Carbon;
use App\Point;
use App\Rol;
use App\Theme;
use Validator;
use App\Image;
use App\Access;
use App\User;
use DB;
use Auth;

class MapsController extends Controller
{
    /**
     * Muestra vista con mapas creados por usuario general
     *
     * @return vista mapas usuario general
     */
    public function index()
    {

    	$user_id = Auth::user()->id;

        $permission = Permission::where('user_id', $user_id)->get();

        $user_maps = array();

        foreach ($permission as $key) {

            $map = Map::where('id', $key->map_id)->first();

            array_push($user_maps, $map);
        }

        $user_maps = array_unique($user_maps);

        $len = count($user_maps);
        for ($i=0; $i < $len; $i++) { 
            if ($user_maps[$i]['name'] == 'Public map'){
                unset($user_maps[$i]);
            }
        }

    	return view('general.maps.index', compact('user_maps'));
    }

    /**
     * Muestra un mapa privado en específico
     *
     * @param  int  $id
     * @return Vista con mapa privado, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function privateMap($id) {

        $map = Map::where('id', $id)->first();
        if (!isset($map)) {
            return back()->withErrors('El mapa seleccionado no existe');
        }

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                if($point != null)
                {
                    array_push($names, $dato->title);
                    array_push($points, [$point->latitude, $point->longitude]);
                }

                

            }
        }

        $map_preferences = Map_private_preferences::where('map_id', $map->id)->where("user_id",Auth::user()->id)->first();

        if($map_preferences == null)
        {
            $map_preferences = new \stdClass();
            $map_preferences->latitude  = '-41.3214705000';
            $map_preferences->longitude = '-73.0139758000';
            $map_preferences->zoom      = 10;
            $map_preferences->map_style = 'basic';
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }

        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }

        $permission = Permission::where('map_id',$map->id);
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->where('value',2)->first();

        $drawline   = 1;

        if ($permission == null) $drawline = 0;


        //DIBUJAR LINEA O POLIGONO
        $dlp = 1;

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if($access == null)
        {
            $dlp = 0;
        }
        

        return view('general.maps.private', compact('map', 'themes', 'categories', 'names', 'points', 'map_preferences','clist','tlist','drawline','dlp'));
    }

    /**
     * Trae todos los puntos y su información, asociados a un mapa
     * estos puntos deben tener status 1 (aprobados)
     *
     * @param  int $id
     * @return Arreglo de puntos 
     */
    public function data($id) {

        $map_data = Map_data::where('map_id', $id)->get();

        $response = array();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }

        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 2,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 0,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

        }


        return $response;
    }

    /**
     * Almacena un nuevo dato asociado a un mapa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id de mapa
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addData(Request $request){

        $id = $request->input("id");

        $isAdmin = false;

        $permission = Permission::where('map_id',$id);
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->whereIn('value',[2,3])->first();

        if($permission == null)
        {
            $errors = ['errors' => [['No cuentas con los suficientes permisos para mapear']]];
            return $errors;
        }

        $permission = Permission::where('map_id',$id);
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->whereIn('value',[3])->first();

        $permission1 = Permission::where('map_id',$id);
        $permission1 = $permission1->where('user_id',Auth::user()->id);
        $permission1 = $permission1->whereIn('value',[2])->first();

        if($permission != null && $permission1 == null && $request->input('type') == 'lines')
        {
            $errors = ['errors' => [['No cuentas con los suficientes permisos para dibujar linea']]];
            return $errors;
        }
        
        
        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }

        $title              = $request->get('title');
        $description        = $request->get('description');
        $categories         = $request->get('categories');
        $themes             = $request->get('themes');
        $image_file         = $request->get('image');
        $image_type         = $request->get('image_ext');
        $type               = $request->get('type');
        $status             = $request->get('status');

        if ($permission1 != null)
        {
            //ADMINISTRADOR DEL MAPA, PUEDE PUBLICAR
            $status = 1;
            $isAdmin = true;
        }

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id,
        ]);

        Map_data::create([
            'map_id' => $id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');

            $coordinates = explode(",",$coordinates);

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }

        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();

                $image = new Image();
                $image->extension = $extension;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }


        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();

        $data->isAdmin = $isAdmin;

        return $data;
    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de categoría específico
     *
     * @param  int $id de categoría, int $map corresponde a aid de mapa
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryData($map, $id) {

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if( $access == null)
        {
            $ppl = ['point'];
        }
        else  $ppl = ['point','polygon','lines'];

        $response = [];
        
        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.category_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",1);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.category_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->whereIn("data.status",[0,2]);
        $data = $data->where("data.created_by",Auth::user()->id);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        return $response; 

    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de tema específico
     *
     * @param  int $id de tema, int $map corresponde a id de mapa
     * @return Arreglo de puntos de tema específico
     */
    public function showThemeData($map, $id) {

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        if( $access == null)
        {
            $ppl = ['point'];
        }
        else  $ppl = ['point','polygon','lines'];

        $response = [];
        
        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.theme_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",1);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        $data = new Data();
        $data = $data->join("map_data","map_data.data_id","=","data.id");
        $data = $data->where("map_data.map_id",$map);
        $data = $data->where("data.theme_id",$id);
        $data = $data->whereIn("data.type",$ppl);
        $data = $data->where("data.status",2);
        $data = $data->where("data.created_by",Auth::user()->id);
        $data = $data->select(['data.*'])->get();

        
        foreach($data as $dt)
        {
            $dt->points = Point::where('data_id', $dt->id)->get();
            $dt->images = Image::where('data_id', $dt->id)->get();

            array_push($response, $dt);
        }

        return $response; 

    }

    /**
     * Muestra un mapa público en específico
     *
     * @param  int  $id
     * @return Vista con mapa publico, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function publicMap(){
        $map = Map::where('name', 'Public map')->first();

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $map->id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                if($point != null)
                {
                    array_push($names, $dato->title);
                    array_push($points, [$point->latitude, $point->longitude]);
                }

                

            }
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }


        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }

        return view('general.maps.public', compact('map', 'themes', 'categories', 'names', 'points','clist','tlist'));
    }

    /**
     * Trae todos los datos asociados al mapa público
     * estos puntos deben tener status 1 (aprobados)
     *
     * @return Arreglo de puntos asociados a mapa público
     */
    public function publicData() {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();

        $access = new Access();
        $access = $access->where('user_id',Auth::user()->id);
        $access = $access->where('config',1);
        $access = $access->first();

        $ppl = ['point','polygon','lines'];

        if($access == null)
        {
            $ppl = ['point'];
        }


        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 0,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }


            $matchThese = [
                'id'            => $key->data_id,
                'status'        => 2,
                'created_by'    => Auth::user()->id
            ];

            $data = Data::where($matchThese);
            $data = $data->whereIn('type',$ppl)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }

        }

        return $response;
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de categoría específico
     *
     * @param  int $id de categoría
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryPublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        if (Auth::check())
        {
            $access = new Access();
            $access = $access->where('user_id',Auth::user()->id);
            $access = $access->where('config',1);
            $access = $access->first();

            $ppl = ['point','polygon','lines'];

            if($access == null)
            {
                $ppl = ['point'];
            }

        }
        else
        {
            $ppl = ['point','polygon','lines'];
        }

        

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1);
            $data = $data->whereIn('type',$ppl)->first();
            if($data != null)
            {
                if ($data->category_id == $id) {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id', $data->id)->get();

                    array_push($response, $data); 
            
                }
            }

            if (Auth::check())
            {

                $data = Data::whereId($key->data_id);
                $data = $data->whereIn("status",[0,2]);
                $data = $data->whereIn('type',$ppl);
                $data = $data->where("created_by",Auth::user()->id)->first();


                if($data != null)
                {
                    if ($data->category_id == $id) {
                        $data->points = Point::where('data_id', $data->id)->get();
                        $data->images = Image::where('data_id', $data->id)->get();

                        array_push($response, $data); 
                
                    }
                }
            }
        } 

        return $response; 
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de tema específico
     *
     * @param  int $id de tema
     * @return Arreglo de puntos de tema específico
     */
    public function showThemePublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        if (Auth::check())
        {
            $access = new Access();
            $access = $access->where('user_id',Auth::user()->id);
            $access = $access->where('config',1);
            $access = $access->first();

            $ppl = ['point','polygon','lines'];

            if($access == null)
            {
                $ppl = ['point'];
            }

        }
        else
        {
            $ppl = ['point','polygon','lines'];
        }

        

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1);
            $data = $data->whereIn('type',$ppl)->first();
            if($data != null)
            {
                if ($data->theme_id == $id) {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id', $data->id)->get();
    
                    array_push($response, $data); 
              
                }
            }

            if (Auth::check())
            {
                $data = Data::whereId($key->data_id);
                $data = $data->whereIn("status",[0,2]);
                $data = $data->whereIn('type',$ppl);
                $data = $data->where("created_by",Auth::user()->id)->first();
    
    
                if($data != null)
                {
                    if ($data->theme_id == $id) {
                        $data->points = Point::where('data_id', $data->id)->get();
                        $data->images = Image::where('data_id', $data->id)->get();
        
                        array_push($response, $data); 
                  
                    }
                }
            }

            
            
        } 

        return $response; 
    }

    /**
     * Agrega datos al mapa público, este queda pendiente de aprobación por admnistrador o curador
     *
     * @param  \Illuminate\Http\Request  $request
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addPublicData(Request $request){

        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }

        $map = Map::where('name', 'Public map')->first();

        $title = $request->get('title');
        $description = $request->get('description');
        $categories = $request->get('categories');
        $themes = $request->get('themes');
        $image_file = $request->get('image');
        $image_type = $request->get('image_ext');
        $type  = $request->get('type');
        $status = $request->get('status');

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id
        ]);

        Map_data::create([
            'map_id' => $map->id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }

        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();

                $image = new Image();
                $image->extension = $extension;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }

        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();

        return $data;
    }

    /**
     * Trae el formulario de personalización de mapa público
     *
     * @return Vista de personalización
     */
    public function personalize(){

        $pref = Map_Preferences::where('user_id',Auth::user()->id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where('name','Public map')->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }

        return view('general.maps.personalizeForm',compact('pref','categories','mcps','themes','mtps'));
    }

    public function personalizeprivate($id)
    {
        $pref = Map_private_preferences::where('user_id',Auth::user()->id);
        $pref = $pref->where('map_id',$id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where("id",$id)->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }

        return view('general.maps.personalizePrivateForm',compact('pref','map','categories','mcps','themes','mtps'));
    }

    /**
     * Define preferencias de mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Vista anterior con mensaje de éxito 
     */
    public function setPreferences(Request $request) {

        $categories = $request->get('categories'); 
        $themes     = $request->get('themes');
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');
        $zoom      = $request->get('zoom');
        $style     = $request->get('style');

        if(count($categories) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una categoría');
        }

        if(count($themes) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una tema');
        }

        if ($latitude == '') {
            $latitude = '-41.3214705000';
        }

        if ($longitude == '') {
            $longitude = '-73.0139758000';
        }

        if ($zoom == '') {
            $zoom = 10;
        }

        if ($style == '') {
            $style = 'basic';
        }

        $user = Auth::user();

        $user_id = $user->id;

        //primero eliminamos todas las preferencias que ya existen para ese usuario
        $map_key = null;

        if($request->has('map_id'))
        {
            $del     = MapCategoryPreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $request->input("map_id");
        }
        else
        {
            $map     = Map::where('name','Public map')->first();

            $del     = MapCategoryPreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $map->id;

        }

        foreach($categories as $cat)
        {
            $mcp = new MapCategoryPreference();
            $mcp->map_id      = $map_key;
            $mcp->user_id     = Auth::user()->id;
            $mcp->category_id = $cat;
            $mcp->save(); 
        }

        foreach($themes as $thm)
        {
            $mtp = new MapThemePreference();
            $mtp->map_id      = $map_key;
            $mtp->user_id     = Auth::user()->id;
            $mtp->theme_id = $thm;
            $mtp->save(); 
        }


        if($request->has('map_id'))
        {
            $map_preferences = Map_private_preferences::where('user_id', $user_id);
            $map_preferences = $map_preferences->where('map_id',$request->input('map_id'))->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_private_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id,
                    'map_id'    => $request->input('map_id')
                ]);
            }    
        }
        else
        {
            $map_preferences = Map_preferences::where('user_id', $user_id)->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id
                ]);
            }
        }

        $permission = Permission::where('user_id', $user_id)->get();

        $user_maps = array();

        foreach ($permission as $key) {

            $map = Map::where('id', $key->map_id)->first();

            array_push($user_maps, $map);
        }

        $user_maps = array_unique($user_maps);

        $len = count($user_maps);
        for ($i=0; $i < $len; $i++) { 
            if ($user_maps[$i]['name'] == 'Public map'){
                unset($user_maps[$i]);
            }
        }

        return back()->with('status', 'Las preferencias han sido guardadas');
    }

    public function showData()
    {
        /*$data = Data::all()->where("created_by",Auth::user()->id);
        
        if ($data != null) {
            foreach($data as $key){
        
                $map = Map_data::where('data_id', $key->id)->first();
                $map = Map::whereId($map->map_id)->first();
                $category = Category::whereId($key->category_id)->first();
                $theme = Theme::whereId($key->theme_id)->first();
        
                $key->map = $map->name;
                $key->category = $category->category_name;
                $key->theme = $theme->themes_name;

                $key->map_id  = $map->id;
                
                if($map->name == 'Public map') $key->public = 1;
                else $key->public = 0;

                $point = Point::where('data_id',$key->id)->first();

                $key->latitude  = $point->latitude;
                $key->longitude = $point->longitude; 


            }
        }*/

        $category = Category::all();
        $theme    = Theme::all();
        $maps     = Map::all();
        
        return view('general.data.index', compact('category','theme','maps'));
    }

    public function destroyData($id)
    {
        
        $data = Data::whereId($id)->first();

        if ($data == null)
        {
            return back()->with('error', 'No existe dato seleccionado');
        }

        if ($data->status == 1)
        {
            return back()->with('error', 'No se puede eliminar un dato publicado');
        }

        if ($data->status == 2)
        {
            return back()->with('error', 'No se puede eliminar un dato en revisión');
        }

        $data_image = Image::where('data_id', $id)->get();
        if ($data_image != null) {
            foreach($data_image as $key){
                $key->delete();
            }
        }

        $points = Point::where('data_id', $id)->get();
        if ($points != null) {
            foreach($points as $key){
                $key->delete();
            }
        }

        $map_data = Map_data::where('data_id', $id)->get();
        if ($map_data != null) {
            foreach($map_data as $key){
                $key->delete();
            }
        }

        $data = Data::whereId($id)->first();
        $data->delete();

        return back()->with('status', 'Se ha eliminado el dato seleccionado');

    }

    public function editData($id)
    {
        $data = Data::whereId($id)->first();
        
        if ($data == null)
        {
                return back()->with('error', 'No existe dato seleccionado');
        }

        $mdata = Map_data::where("data_id",$data->id)->first();

        $admin = Permission::where('map_id',$mdata->map_id);
        $admin = $admin->where('user_id',Auth::user()->id);
        $admin = $admin->whereIn('value',[2])->first();

        if ($data->status == 1 && $admin == null)
        {
            return back()->with('error', 'No se puede editar un dato publicado');
        }
        /*if ($data->status == 2)
        {
            return back()->with('error', 'No se puede editar un dato en revisión');
        }*/
        if ($data->created_by != Auth::user()->id && $admin == null)
        {
            return back()->with('error', 'No tienes permisos para editar este dato');
        }


        $point = Point::where("data_id",$data->id)->first();

        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }


        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();

        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();


        return view('general.data.edit', compact('data','point','cat','themes'));
    }

    public function seeData($id)
    {
        $data = Data::whereId($id)->first();
        
        if ($data == null)
        {
                return back()->with('error', 'No existe dato seleccionado');
        }

        
        


        $point = Point::where("data_id",$data->id)->first();

        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }

        $mdata = Map_data::where("data_id",$data->id)->first();

        $admin = Permission::where('map_id',$mdata->map_id);
        $admin = $admin->where('user_id',Auth::user()->id);
        $admin = $admin->whereIn('value',[2])->first();

        if ($data->created_by != Auth::user()->id && $admin == null)
        {
            return back()->with('error', 'No tienes permisos para editar este dato');
        }

        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();

        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();


        return view('general.data.show', compact('data','point','cat','themes'));
    }

    public function storeData($id, Request $request) {
        
        $title = $request->get('title');
        $description = $request->get('description');
        
        $data = Data::whereId($id)->first();

        $mdata = Map_data::where("data_id",$data->id)->first();

        $admin = Permission::where('map_id',$mdata->map_id);
        $admin = $admin->where('user_id',Auth::user()->id);
        $admin = $admin->whereIn('value',[2])->first();

        $status = $request->input("status");

        if($data->status == 1 && $admin == null)
        {
            return back()->with('error', 'No es posible editar un dato publicado');
        }

        if($data->status == 2 && $admin == null)
        {
            return back()->with('error', 'No es posible editar un dato en revisión');
        }

        if($admin != null && $status == 2)
        {
            //SI ES ADMIN Y SE ENCUENTRA EN REVISION, LO DEJA PUBLICADO 
            $status = 1;
            $data->approved_by = Auth::user()->id;
            $data->approved_at = Carbon::now();
        }

        if($admin != null && $status == 0)
        {
            //SI ES ADMINISTRADOR DEL MAPA Y EL ESTADO  ES GUARDADO, SE MANTIENE SU ESTADO ACTUAL
            $status = $data->status;
        }
        
        if($description != null) 
        {
            $data->description = $description;
        }

        if($data->type == 'point')
        {
            $point = Point::where('data_id',$data->id)->first();

            if($point != null)
            {
                $point->latitude  = $request->input("latitude");
                $point->longitude = $request->input("longitude");

                $point->save();
            }

        }
        
        $data->category_id  = $request->input("category");
        $data->theme_id     = $request->input("theme");
        $data->status       = $status;
        
        $data->title = $title;
        //$data->status = $status;
        $data->save();

        if($data->status == 2) return redirect('general/data');
        
        return back()->with('status', 'El dato ha sido actualizado');
    }

    public function grid(Request $request)
    {

        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = $request->input('length');
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];
                
        $with = $request->input('with');
        
        $status   = $request->input('status');
        $category = $request->input('category');
        $theme    = $request->input('theme');

        $map      = $request->input('map');
        $title    = $request->input('title');
        $created  = $request->input('created');
        $pub      = $request->input('pub');
        $offset   = $request->input('offset');
        $offsetp   = $request->input('offsetp');

        $type     = $request->input('type'); 

        $datas = new Data();
        $datas  = $datas->join("map_data","data.id","=","map_data.data_id");
        $datas  = $datas->join("maps","map_data.map_id","=","maps.id");
        $datas  = $datas->join("themes","data.theme_id","=","themes.id");
        $datas  = $datas->join("categories","data.category_id","=","categories.id");
        $datas  = $datas->join("users","data.created_by","=","users.id");
        $datas  = $datas->leftJoin("permissions",function($query){
            $query->on('maps.id','=','permissions.map_id');
            $query = $query->where('permissions.user_id','=',Auth::id());
            $query = $query->where('permissions.value','=',2);
        });

        

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                if($field != 'id') $datas = $datas->orderBy($field, $dir);
            }

            if($status != 99)
            {
                $datas = $datas->where("data.status","=",DB::raw($status));
            }

            if($category != 0)
            {
                $datas = $datas->where("data.category_id","=",DB::raw($category));
            }

            if($theme != 0)
            {
                $datas = $datas->where("data.theme_id","=",DB::raw($theme));
            }

            if($map != '0')
            {
                $datas = $datas->where("map_data.map_id","=",$map);
            }

            if($title != '')
            {
                $datas = $datas->where("data.title","like",'%'.$title.'%');
            }

            if($created != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.created_at + INTERVAL ".$offset." MINUTE  )"),"=",$created);
            }

            if($pub != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.approved_at + INTERVAL ".$offsetp." MINUTE  )"),"=",$pub);
            }

            if($type == 'general')
            {
                $datas = $datas->where(function($query){
                    $query->where('data.created_by','=',Auth::id());
                    $query->orWhere('permissions.user_id','=',Auth::id());
                });
                //$datas = $datas->where('data.created_by','=',Auth::id());
            }


            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $datas = $datas->select(['data.id','permissions.value',DB::raw("IF(maps.name = 'Public map',1,0) as flag"),'data.created_at','data.approved_at','data.title','categories.category_name as cat','themes.themes_name as theme','maps.name as map',DB::raw('CONCAT(users.name," ",IFNULL(users.surname,"")) as user'),'data.status','maps.id as map_id']);

            $data->datas = $datas->get();
            
            $datas = $datas->orderBy(DB::raw('flag'),'desc');
            $datas = $datas->orderBy('maps.name','asc');

            foreach($data->datas as $dt)
            {
                $point = Point::where('data_id',$dt->id)->first();

                if($point != null)
                {
                    $dt->latitude  = $point->latitude;
                    $dt->longitude = $point->longitude;
                }
                
                 
            }

            $data->recordsTotal = Data::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }

    }

    public function multiClone(Request $request)
    {

        $success = true;
        $msg     = '';

        $map  = Map::where('name','Public map')->first();

        $ids = $request->input('ids');

        $ids = explode(',',$ids);

        //UNA VALIDACION ADICIONAL ES QUE TODOS LOS PUNTOS A CLONAR DEBEN ENCONTRARSE 
        //EN UN MAPA DONDE EL USUARIO SEA ADMINISTRADOR DEL MAPA EN CUESTION

        foreach($ids as $id)
        {
            $info = Map_data::where('data_id',$id)->first();

            if($info == null)
            {
                $msg     = 'Uno de los datos presentes no existe';
                $success = false;
                break; 
            }

            if($info->map_id == $map->id)
            {
                $msg     = 'Uno de los datos a clonar ya se encuentra en el mapa público';
                $success = false;
                break;
            }

            $permission = Permission::where('map_id',$info->map_id);
            $permission = $permission->where('user_id',Auth::user()->id);
            $permission = $permission->where('value',2)->first();

            if ($permission == null)
            {
                $msg     = 'Uno de los datos a clonar pertenece a un mapa en el cual no eres administrador';
                $success = false;
                break;
            }


        }

        if($success)
        {

            foreach($ids as $id)
            {
                $data = Data::where('id',$id)->first();

                $newData              = new Data();
                $newData->title       = $data->title;
                $newData->description = $data->description;
                $newData->created_by  = Auth::user()->id;
                $newData->type        = $data->type;
                $newData->theme_id    = $data->theme_id;
                $newData->category_id = $data->category_id;
                $newData->status      = 2; //EN REVISIÓN

                $newData->save();

                //asociamos punto/poligono a mapa 
                $mapData           = new Map_data();
                $mapData->map_id   = $map->id;
                $mapData->data_id  = $newData->id;
                $mapData->save();

                //copiamos coordenadas 
                $points = Point::where('data_id',$data->id)->get();

                foreach ($points as $point)
                {
                    $newPoint = new Point();
                    $newPoint->latitude  = $point->latitude;
                    $newPoint->longitude = $point->longitude;
                    $newPoint->data_id   = $newData->id;
                    $newPoint->save();
                }

                $images = Image::where('data_id',$data->id)->get();

                foreach ($images as $image)
                {
                    $newImage = new Image();
                    $newImage->extension = $image->extension;
                    $newImage->filename  = $image->filename;
                    $newImage->data_id   = $newData->id;
                    $newImage->save();
                    
                    Storage::copy('points/'.$image->id.'.'.$image->extension,'points/'.$newImage->id.'.'.$newImage->extension);

                }

            }

        }

        return response()->json([
            'success' => $success,
            'code' => 200,
            'message' => $msg,
            'data' => null
        ], 200);
        
    }

    public function rejectData(Request $request,$id)
    {

        $data = Data::find($id);

        $info = Map_data::where('data_id',$id)->first();

        if($data == null)
        {
            return response()->json([
                'success' => true,
                'code' => 404,
                'message' => 'No se encuentra el dato seleccionado',
                'data' => null
            ], 200);
        }

        if($data->status == 3)
        {
            return response()->json([
                'success' => true,
                'code' => 400,
                'message' => 'Dato ya se encuentra rechazado',
                'data' => null
            ], 200);
        }

        $permission = Permission::where('map_id',$info->map_id);
        $permission = $permission->where('user_id',Auth::user()->id);
        $permission = $permission->where('value',2)->first();

        if ($permission == null)
        {
            return response()->json([
                'success' => true,
                'code' => 400,
                'message' => 'No cuentas con los suficientes privilegios para realizar esta operación',
                'data' => null
            ], 200);
        }



        //

        $user = User::find($data->created_by);

        $cause  = $request->input('cause');
        $causet = $request->input('other');

        $data->status = 3;
        $data->save();

        //Mail::to($user->email)->queue(new SendRejectMail($user,$data,$cause,$causet));

        

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Dato rechazado con éxito',
            'data' => null
        ], 200);

    }


}
