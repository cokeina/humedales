<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Data_cat;
use App\Data_theme;
use App\Data_image;
use App\Data;
use App\Map;
use App\Map_data;
use App\Map_preferences;
use App\Permission;
use App\Point;
use App\Theme;
use App\Image;
use Validator;
use Storage;
use Auth;
use Log;
use DB;

class DataController extends Controller
{


    /**
     * Trae todos los datos asociados al mapa público
     * estos puntos deben tener status 1 (aprobados)
     *
     * @return Arreglo de puntos asociados a mapa público
     */
    public function publicData() {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();

        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = []; //Data_image::where('data_id', $data->id)->get();

                array_push($response, $data); 
            }
        }

        return $response;
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de tema específico
     *
     * @param  int $id de tema
     * @return Arreglo de puntos de tema específico
     */
    public function showThemeData($id) {

        $map = Map::where('name', 'Public map')->first();

        $data_themes = Data_theme::where('theme_id', $id)->get();

        $data = [];
        foreach($data_themes as $var) {

            $single_data = Data::whereId($var->data_id)->first();
            if ($single_data->map_id == $map->id) {
                array_push($data, $single_data);
            }
        }

        foreach($data as $var) {
            $categories = Data_cat::where('data_id', $var->id)->get();
            $themes = Data_theme::where('data_id', $var->id)->get();
            $var->categories = $categories[0]->category_id;
            $var->themes      = $themes[0]->theme_id;
        }

        return $data;
    }

    /**
     * Trae todos los puntos asociados a mapa publico con un id de categoría específico
     *
     * @param  int $id de categoría
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryData($id) {

        $map = Map::where('name', 'Public map')->first();

        $data_categories = Data_cat::where('category_id', $id)->get();

        $data = [];
        foreach($data_categories as $var) {

            $single_data = Data::whereId($var->data_id)->first();
            if ($single_data->map_id == $map->id) {
                array_push($data, $single_data);
            }
        }

        foreach($data as $var) {
            $categories = Data_cat::where('data_id', $var->id)->get();
            $themes = Data_theme::where('data_id', $var->id)->get();
            $var->categories = $categories[0]->category_id;
            $var->themes      = $themes[0]->theme_id;
        }

        return $data;
    }

    /**
    * Muestra El contenido asociado a un punto específico
    *
    * @param int $id de dato
    * @return vista para mostrar contenido, con información asociada a un punto
    */
    public function content($id) {

        $user = Auth::user();

        $map_data = Map_data::where('data_id', $id)->first();

        $data = Data::whereId($id)->first();

        $category = Category::whereId($data->category_id)->first();
        $data->category = $category->category_name;

        $theme = Theme::whereId($data->theme_id)->first();
        $data->theme = $theme->themes_name;

        $map_id = $map_data->map_id;

        $data->image = Data_image::where('data_id', $data->id)->get();

        return view('shared.content', compact('data', 'map_id'));
    }

    /**
     * Define preferencias de mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Vista anterior con mensaje de éxito 
     */
    public function preferences(){

        $user = Auth::user();

        if ($user) {

            $map_preferences = Map_preferences::where('user_id', $user->id)->first();
            if ($map_preferences) {
                return $map_preferences;
            } else {
                $map_preferences = [
                    'latitude'  => -41.3214705,
                    'longitude' => -73.0139758,
                    'zoom'      => 10,
                    'map_style' => 'basic'
                ];

                return $map_preferences;
            }
        } else {
            $map_preferences = [
                'latitude'  => -41.3214705,
                'longitude' => -73.0139758,
                'zoom'      => 10,
                'map_style' => 'basic'
            ];

            return $map_preferences;
        }
    }

    public function images(Request $request)
    {
        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = 5;
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];
                
        $with = $request->input('with');
        
        $status   = $request->input('status');
        $category = $request->input('category');
        $theme    = $request->input('theme');

        $data_id  = $request->input('data_id'); 

        $datas = new Image();

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                $datas = $datas->orderBy($field, $dir);
            }

            $datas = $datas->where('data_id','=',$data_id);

            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $data->images = $datas->get();

            $data->recordsTotal = Image::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }
    }

    public function uploadimage(Request $request)
    {
        
        try
        {
            $validator = Validator::make($request->all(),['file'=>'required|mimes:jpg,jpeg,png,bmp|max:20000']);
            
            if ($validator->fails())
            {
                Log::info("message=[{$validator->errors()}]");
                $errors = ['errors' => $validator->errors()];
                return response()->json([
                    'success' => false,
                    'code' => 201,
                    'message' => '',
                    'data' => $errors
                ], 201);
            }

            $fl       = $request->file('file');
            $data_id  = $request->input('data_id');

            $extension = $fl->getClientOriginalExtension();
            $filename  = $fl->getClientOriginalName();

            $data              = new Image();
            $data->extension  = $extension;
            $data->filename   = $filename;
            $data->data_id    = $data_id ;
            
            $data->save();
            
            $fl->storeAs("points",$data->id.".".$extension);


            return response()->json([
                'success' => false,
                'code' => 200,
                'message' => 'ok',
                'data' => null
            ], 200);    


        }
        catch(\Exception $e){
            
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            return response()->json([
                'success' => false,
                'code' => 500,
                'message' => 'nok',
                'data' => null
            ], 200);
        }
    }

    public function deleteimage(Request $request,$id)
    {
        try{
            $data = Image::find($id);

            if($data === null) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => 'nok',
                    'data' => null
                ], 404);
            }

            $data->delete();

            Storage::delete('points/'.$id.'.'.$data->extension);

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => null
            ], 200);


        } catch(\Exception $e) {
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            return response()->json([
                'success' => false,
                'code' => 500,
                'message' => 'nok',
                'data' => null
            ], 500);
        }
    }

}
