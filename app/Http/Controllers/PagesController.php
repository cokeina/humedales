<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Category;
use App\Data;
use App\Map;
use App\Map_cat;
use App\Map_data;
use App\Map_theme;
use App\Point;
use App\Theme;
use App\Visit;
use App\Content;

class PagesController extends Controller
{
    /** 
    * Controlador creado para controlar las redirecciones en la aplicación, aquí se ingresa a cada una de las secciónes de la * app
    **/


    public function main()
    {
        $categories = count(Category::all());

        $themes = count(Theme::all());

        $data = Data::where('status',1)->count();

        $visits = Visit::count(DB::raw('DISTINCT visitor'));

        $lcontents = Content::all();

        $contents = [];

        foreach ($lcontents as $content)
        {
            $contents[$content->name] = $content->content;
        }

        return view('index', compact('categories', 'themes','data','visits','contents'));
    }

    public function whatis()
    {
        $lcontents = Content::all();

        $contents = [];

        foreach ($lcontents as $content)
        {
            $contents[$content->name] = $content->content;
        }
        
        return view('what-is',compact('contents'));
    }

    public function maps(Request $request)
    {
        $map = Map::where('name', 'Public map')->first();

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $map->id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                if($point != null)
                {
                    array_push($names, $dato->title);
                    array_push($points, [$point->latitude, $point->longitude]);
                }

                

            }
        }

        $visit = new Visit();
        $visit->visitor = $request->ip();
        $visit->save();

        return view('map', compact('map', 'themes', 'categories', 'names', 'points'));
    }

    public function download()
    {
        return view('download');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function contact()
    {
        return view('contact');
    }

    public function team()
    {
        return view('team');
    }

    public function api()
    {
        return view('api');
    }

    public function seedata($id)
    {
        $data         = Data::find($id);
        $points       = Point::where('data_id',$id)->get();

        $t = [];

        foreach($points as $point)
        {
            $lng = $point->longitude;
            $lat = $point->latitude;
            $t[] = ['longitude'=>$lng,'latitude'=>$lat];
        }

        $data->points = json_encode($t,JSON_UNESCAPED_SLASHES );
        
        return view('seedata',compact('data'));
    }


    /* Funcion creada para redirigir al usuario cuando ingresa con sus credenciales a la app, en base al rol de usuario */
    public function redirect()
    {
    	if (!Auth::check())
    	{
    		return redirect('/login');
    	} else {
    		$user = Auth::user();

            $rol_id = $user->roles->id;
            switch ($rol_id) {
                case 1:
                    return redirect('admin/');
                    break;
                case 2:
                    return redirect('curador/');
                    break;
                case 3:
                    return redirect('general/');
                    break;
                
                default:
                    return redirect('/login');
                    break;
            }
    	}
    }

    /* Funcion creada para redirigir al usuario desde la vista de mapa público (al momento de seleccionar personalizar mapa) */
    public function personalizeMap()
    {
        if (!Auth::check())
        {
            return redirect('/login');
        } else {
            $user = Auth::user();

            $rol_id = $user->roles->id;
            switch($rol_id) {
                case 1:
                    return redirect('admin/my-maps/personalize'); 
                    break;
                case 2:
                    return redirect('curador/my-maps/personalize'); 
                    break;
                case 3:
                    return redirect('general/my-maps/personalize'); 
                    break;
                default:
                    return redirect('/login');
                    break;
            }
        }
    }

    public function personalizePrivateMap($id)
    {
        if (!Auth::check())
        {
            return redirect('/login');
        } else {
            $user = Auth::user();

            $rol_id = $user->roles->id;
            switch($rol_id) {
                case 1:
                    return redirect('admin/my-maps/personalize/'.$id); 
                    break;
                case 2:
                    return redirect('curador/my-maps/personalize/'.$id); 
                    break;
                case 3:
                    return redirect('general/my-maps/personalize/'.$id); 
                    break;
                default:
                    return redirect('/login');
                    break;
            }
        }
    }


}
