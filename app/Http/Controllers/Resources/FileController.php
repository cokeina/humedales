<?php

namespace App\Http\Controllers\Resources;

use Log;
use Auth;
use Storage;
use App\File;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = $request->input('length');
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];
                
        $with = $request->input('with');
        
        $filters = $request->input('filters');

        $datas = new File();

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                $datas = $datas->orderBy($field, $dir);
            }

            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $data->files = $datas->get();
            $data->recordsTotal = File::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(),['description'=>'required|string','file'=>'required|max:2048']);
            
            if ($validator->fails())
            {
                Log::info("message=[{$validator->errors()}]");
                $errors = ['errors' => $validator->errors()];
                return response()->json([
                    'success' => false,
                    'code' => 201,
                    'message' => '',
                    'data' => $errors
                ], 201);
            }

            $data              = new File();
            $data->description = $request->input("description");
            $data->created_by  = Auth::id();
            $data->updated_by  = Auth::id();
            $data->filename  = '';

            $data->save();

            $rfile = $request->file('file');
            
            $filename = $data->id;

            
            $request->file('file')->storeAs('documents',$filename);

            $data->filename = $rfile->getClientOriginalName();
            $data->save();

            return response()->json([
                'success' => false,
                'code' => 200,
                'message' => 'ok',
                'data' => null
            ], 200);    


        }
        catch(\Exception $e){
            
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            return response()->json([
                'success' => false,
                'code' => 500,
                'message' => 'nok',
                'data' => null
            ], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try{
            $data = File::find($id);

            if($data === null) {
                return response()->json([
                    'success' => false,
                    'code' => 404,
                    'message' => 'nok',
                    'data' => null
                ], 404);
            }

            $data->delete();

            Storage::delete('documents/'.$id);

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => null
            ], 200);


        } catch(\Exception $e) {
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            return response()->json([
                'success' => false,
                'code' => 500,
                'message' => 'nok',
                'data' => null
            ], 500);
        }
    }

    public function download($id)
    {
        $file   = File::find($id);
        
        return response()->download(public_path().'/storage/documents/'.$id,$file->filename);
    }

}
