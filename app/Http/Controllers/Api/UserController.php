<?php

namespace App\Http\Controllers\Api;


use App\Data;
use App\Point;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    
    public function points(Request $request)
    {

         $datas = new Data();
         $datas = $datas->join('points','points.data_id','=','data.id');
         $datas = $datas->join('map_data','map_data.data_id','=','data.id');
         $datas = $datas->join('maps','map_data.map_id','=','maps.id');
         $datas = $datas->join('categories','categories.id','data.category_id');
         $datas = $datas->join('themes','themes.id','=','data.theme_id');   
         $datas = $datas->where('created_by',$request->user()->id);


         $datas = $datas->select(['data.id','data.title','data.description',
                                  'maps.name as map_name','categories.category_name',
                                  'themes.themes_name','data.status','data.type'])->get();

         foreach ($datas as $data)
         {
            switch ($data->status)
            {
                case 0: 
                    $data->statusName = 'POR GUARDAR';
                break;

                case 1: 
                    $data->statusName = 'PUBLICADO';
                break;

                case 2: 
                    $data->statusName = 'EN REVISIÓN';
                break;

                case 3: 
                    $data->statusName = 'RECHAZADO';
                break;

            }

            $points = new Point();
            $points = $points->where('data_id',$data->id);
            $points = $points->select(['latitude','longitude'])->get();
            $data->points = $points;

         }


         return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'ok',
            'data' => $datas
        ], 200);


    }

}
