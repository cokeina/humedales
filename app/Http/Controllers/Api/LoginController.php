<?php

namespace App\Http\Controllers\Api;

use Log;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $client = new Client([
            'timeout'  => 30.0,
        ]);
        
        Log:info('LOG - IN!');

        $apiCrendentials = config('app.api_credentials');

        $params['grant_type'] = 'password';

        $params['client_id'] =   $apiCrendentials['client_id'];
        $params['client_secret'] = $apiCrendentials['client_secret'];
        $params['username'] = $request->input('email');
        $params['password'] = $request->input('password');

        $response = $client->request('POST', url('api/oauth/get'), ['form_params' => $params]);    

        if($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody(), true);

            return response()->json([
                $body
            ], 200);
        }    
        else
        {
            return response()->json([
                $response
            ], 200);
        } 

    }
}
