<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Map;
use App\Point;
use App\Data;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapsController extends Controller
{
    
    public function index(Request $request)
    {
        
       
        if ($request->user()->roles_id == 1)
        {
            //ES ADMINISTRADOR, PUEDE VER TODOS LOS MAPAS 
            $maps = Map::all('id','name');
        }
        else
        {
            //ES USUARIO COMUN, SOLO PUEDEN LISTARSE LOS MAPAS DISPONIBLES PARA EL USUARIO
            $permisions = new Permission();
            $permisions = $permisions->where('user_id',$request->user()->id);
            $permisions = $permisions->select(['map_id'])->get();

            $public_map = new Map();
            $public_map = $public_map->where('name','Public map');
            $public_map = $public_map->select(['id'])->first();

            $permisions[] = $public_map;
            
            $maps = new Map();
            $maps = $maps->whereIn($permisions);
            $maps = $maps->select(['id','name'])->get();
            
        }
        

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'ok',
            'data' => $maps
        ], 200);

    }

    public function points(Request $request,$id)
    {
        //PRIMERO SE VERIFICA SI TIENE PERMISOS PARA VER EN EL MAPA 
        $allowed = true;

        if($request->user()->roles_id != 1)
        {
            //DISTINTO DE ADMINISTRADOR 
            $permission = new Permission();
            $permission = $permission->where('user_id',$request->user()->id);
            $permission = $permission->where('map_id',$id);
            $permission = $permission->get();

            if($permission == null)
            {
                //NO TIENE PERMISO DE VER DATOS DE MAPA 
                //SE VERIFICA SI ES MAPA PUBLICO

                $public_map = new Map();
                $public_map = $public_map->where('name','Public map');
                $public_map = $public_map->select(['id'])->first();

                if($id != $public_map->id)
                {
                    $allowed = false;
                }

            }

        } 

        if ($allowed)
        {
            //LISTA DATOS PUBLICADOS  EN UN MAPA
            $datas = new Data();
            $datas = $datas->join('map_data','data.id','=','map_data.id');
            $datas = $datas->join('categories','data.category_id','=','categories.id');
            $datas = $datas->join('themes','data.theme_id','=','themes.id');
            $datas = $datas->where('map_data.map_id',$id);
            $datas = $datas->where('data.status',1);
            $datas = $datas->select(['data.id','data.title','data.description',
                                    'categories.category_name','themes.themes_name','data.type'])->get();

            foreach($datas as $data)
            {
                $points = new Point();
                $points = $points->where('data_id',$data->id);
                $points = $points->select(['latitude','longitude'])->get();
                $data->points = $points;

            }


            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $datas
            ], 200);

        }
        else
        {
            return response()->json([
                'success' => true,
                'code' => 403,
                'message' => 'No tienes permisos para realizar esta operación',
                'data' => ''
            ], 403);
        }

        
        
        

    }

}
