<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use App\Mail\SendContact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    
    public function send(Request $request)
    {
        $rules = [
            'name'    => 'required|string',
            'email'   => 'required|email',
            'message' => 'required|string'
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return back()->withErrors($validator->errors())->withInput();
        }

        $contact           = new \stdClass();
        $contact->name     = $request->input('name');
        $contact->email    = $request->input('email');
        $contact->message  = $request->input('message');

        Mail::to('mapahumedalesurbanos@gmail.com')->queue(new SendContact($contact));

        return redirect()->back()->with('message', 'Mensaje enviado');

    }


}
