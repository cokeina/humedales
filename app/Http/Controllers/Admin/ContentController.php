<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    
    public function index(Request $request)
    {
        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = $request->input('length');
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];

        $datas = new Content();

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                $datas = $datas->orderBy($field, $dir);
            }

            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $datas = $datas->select(['id','name','description','type']);

            $data->contents = $datas->get();

            $data->recordsTotal = Content::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }


    }

    public function show(Request $request,$id)
    {
        
        $data = Content::find($id);

        if($data == null)
        {
            return response()->json([
                'success' => true,
                'code' => 404,
                'message' => 'ok',
                'data' => null
            ], 200);
        }
        else
        {
            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);
        }

    }

    public function update(Request $request)
    {
        //ACTUALIZA 
        $type = $request->input('type');

        if($type == 1)
        {
            //TEXTO

            $data = Content::find($request->input('id'));

            if($data == null)
            {
                return response()->json([
                    'success' => true,
                    'code' => 404,
                    'message' => 'ok',
                    'data' => null
                ], 200);
            }

            $data->content = $request->input('content');

            $data->save();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }
        else
        {
            //IMAGEN
            $validator = Validator::make($request->all(),[
                'file' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
            ]);
    
            if ($validator->fails())
            {
                $errors = ['errors' => $validator->errors()];
                
                return response()->json([
                    'success' => true,
                    'code' => 201,
                    'message' => 'ok',
                    'data' => $errors
                ], 201);

            }

            $file = $request->file('file');

            $extension = $file->getClientOriginalExtension();    

            $data = Content::find($request->input('id'));

            if($data == null)
            {
                return response()->json([
                    'success' => true,
                    'code' => 404,
                    'message' => 'ok',
                    'data' => null
                ], 200);
            }

            $data->content = $data->id.'.'.$extension;
            $data->save();

            $file->storeAs("public/contents",$data->id.'.'.$extension);

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }

    }

}
