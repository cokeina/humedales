<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Mail\SendRejectMail;
use App\Mail\SendPubMail;
use App\Category;
use Carbon\Carbon;
use App\Data;
use App\Data_image;
use App\Initiative;
use Validator;
use App\Map;
use App\Map_cat;
use App\Map_data;
use App\Map_preferences;
use App\Map_private_preferences;
use App\MapCategoryPreference;
use App\MapThemePreference;
use App\Map_theme;
use App\Permission;
use App\Point;
use App\Rejection;
use App\Rol;
use App\Theme;
use App\User;
use App\Image;
use App\Access;
use DB;
use Auth;
use Mail;
use Log;


class AdminController extends Controller
{

    /**
     * Muestra vista principal perfil administador
     *
     * @return vista principal perfil administrador
     */
    public function dashboard()
    {
    	return view('admin.index');
    }

    public function downloads()
    {
        return view('files.admin');
    }

    public function contents()
    {
        return view('admin.contents.index');
    }


    /**
     * Lista todos los usuarios de la app y devuelve a la vista usuarios
     *
     * @return vista donde se muestran todos los usuarios
     */
    public function users() {

    	$rol_user = Auth::user()->roles->id;

    	$users = User::where('roles_id', '!=', $rol_user)->get();

    	foreach ($users as $user) {
    		$rol_id = $user->roles_id;
    		$rol = Rol::where('id', $rol_id)->first();

    		$user->roles_id = $rol->rol_name;
    	}

    	return view('admin.users.index', compact('users'));
    }

    /**
     * Muestra todas las iniciativas pendientes de aprobación o publicadas en la app
     *
     * @return Vista con todas las iniciativas
     */
    public function initiatives()
    {

        $status = [3, 4];
        $initiatives = Initiative::where('status_id', $status)->get();

        return view('admin.initiatives.index', compact('initiatives'));
    }


    /**
     * Busca el usuario seleccionado y trae su información, además de traer todos los mapas creados en la app
     * para generar permisos sobre estos
     *
     * @param  int $id
     * @return Vista de edición de usuario con datos de usuario, su rol, mapas y permisos
     */
    public function editUser($id) {

        
    	$user = User::whereId($id)->first();

        $maps = Map::where('name', '!=', 'Public map')->get();

    	$roles = Rol::all();

        $access = new Access();
        $access = $access->where('user_id',$id)->first();

        if($access == null || $access->config == 0)
        {
            $user->access = false;
        }
        else
        {
            $user->access = true;
        }


        foreach($maps as $map) {

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 1
            ];
            $map->see = Permission::where($matchRequest)->first();

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 2
            ];
            $map->admin = Permission::where($matchRequest)->first();

            $matchRequest = [
                'map_id' => $map->id,
                'user_id' => $id,
                'value'   => 3
            ];
            $map->invite = Permission::where($matchRequest)->first();
        }

        $rol_selected = Rol::whereId($user->roles_id)->first();

        $rol_selected = $rol_selected->rol_name;

    	return view('admin.users.edit', compact('user','roles', 'maps', 'rol_selected', 'permisions'));
    }


    /**
     * Actualiza a un usuario con el id seleccionado, dentro de la actualización se consideran los permisos para
     * los mapas seleccionados,  además de cambiar el rol asociado a este
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Vuelve a vista de edición de usuario más mensaje de edición exitosa
     */
    public function updateUser($id, Request $request){


        $new_rol  = $request->get('rol');
        $sees     = $request->get('see');
        $admins   = $request->get('admin');
        $invites  = $request->get('invite');
        $acs      = $request->get('access');

        $user    = User::whereId($id)->first();
        $user_rol = $user->roles_id;
        if ($user_rol != $new_rol) {
            $user->roles_id = $new_rol;
            $user->save();
        }

        $access = new Access();
        $access = $access->where('user_id',$id)->first();


        if($access == null)
        {
            $access = new Access();
            $access->user_id = $id;
            $access->config  = $acs;
            $access->save();
        }
        else
        {
            $access->config = $acs ;
            $access->save();
        }
        



        $permissions = Permission::all();

        foreach($permissions as $permission) {
            if ($permission->user_id == $id) {
                $permission->delete();
            }
        }

        if ($sees != null) {
            foreach ($sees as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }

        if ($admins != null) {
            foreach ($admins as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }    

        if ($invites != null) {
            foreach ($invites as $key) {

                $key = explode("$", $key);

                Permission::create([
                    'user_id' => $id,
                    'map_id'  => $key[0],
                    'value'   => $key[1]
                ]);
            }
        }

        return back()->with('status', 'El usuario ha sido editado');

    }

    /**
     * Elimina un usuario seleccionado
     *
     * @param  int  $id
     * @return redirecciona a pantalla donde se listan todos los usuarios con mensaje de eliminación
     */
    public function destroyUser($id) {

        $user = User::whereId($id)->first();
        
        $permissions = Permission::all();

        foreach($permissions as $permission) {
            if ($permission->user_id == $user->id) {
                $permission->delete();
            }
        }

    	$user->delete();

    	return redirect('admin/users')->with('status', 'El usuario ha sido eliminado');
    }


    /**
     * Muestra a todos los mapas creados en la app
     *
     * @return Vista con mapas creados
     */
    public function maps() {
        $maps = Map::where('name', '!=', 'Public map')->get();

        return view('admin.maps.index', compact('maps'));
    }

    public function createMap() {

        return view('admin.maps.create');
    }

    /**
     * Valida y crea un nuevo mapa
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirige a vista de nuevo mapa creado
     */
    public function storeMap(Request $request)
    {
        $title      = $request->input('title');
        $categories = $request->get('categories');
        $themes     = $request->get('themes');
        $latitude   = $request->get('latitude');
        $longitude  = $request->get('longitude');
        $map_style  = $request->get('style');
        $zoom       = $request->get('zoom');
        $clone      = $request->get('clone');
        $user_id = Auth::user()->id;

        if (!isset($title)) {
            return back()->withErrors('Debes ingresar un título')->withInput();
        }

        $matchThese = [
            'name' => $title,
            'user_id' => $user_id
        ];

        $mapExists = Map::where($matchThese)->get();
        if (count($mapExists) != 0) {
            return back()->withErrors('Ya tienes un mapa con ese nombre')->withInput();
        }

        if (count($categories) == 0) {
            return back()->withErrors('Debes seleccionar al menos una categoría')->withInput();
        }

        if (count($themes) == 0) {
            return back()->withErrors('Debes seleccionar al menos un tema')->withInput();
        }

        if ($latitude == '') {
            $latitude = '-41.3214705000';
        }

        if ($longitude == '') {
            $longitude = '-73.0139758000';
        }

        if ($zoom == '') {
            $zoom = 10;
        }

        $map = Map::create([
            'name'    => $title,
            'user_id' => $user_id
        ]);

        $map = Map::where('name', $title)->first();

        foreach ($categories as $category) {
            Map_cat::create([
                'map_id' => $map->id,
                'category_id' => $category
            ]);
        }

        foreach ($themes as $theme) {
            Map_theme::create([
                'map_id' => $map->id,
                'theme_id' => $theme
            ]);
        }

        Map_private_preferences::create([
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'zoom'      => $zoom,
            'map_style' => $map_style,
            'map_id'    => $map->id,
            'user_id'   => Auth::user()->id
        ]);

        if ($clone == "1")
        {
            //seleccionamos los datos a clonar
            $pmap = Map::where("name","Public map")->first();

            $cloneData = DB::table('data')->whereIn('data.theme_id',$themes);
            $cloneData = $cloneData->whereIn('category_id',$categories);
            $cloneData = $cloneData->join("map_data","data.id","=","map_data.data_id");
            $cloneData = $cloneData->where('map_data.map_id',$pmap->id); 
            $cloneData = $cloneData->select(['data.*'])->get();

            foreach ($cloneData as $clone)
            {
                $newData              = new Data();
                $newData->title       = $clone->title;
                $newData->description = $clone->description;
                $newData->created_by  = Auth::user()->id;
                $newData->type        = $clone->type;
                $newData->theme_id    = $clone->theme_id;
                $newData->category_id = $clone->category_id;
                $newData->status      = 1;
        
                $newData->save();

                //asociamos punto/poligono a mapa 
                $mapData           = new Map_data();
                $mapData->map_id   = $map->id;
                $mapData->data_id  = $newData->id;
                $mapData->save();

                //copiamos coordenadas 
                $points = Point::where('data_id',$clone->id)->get();

                foreach ($points as $point)
                {
                    $newPoint = new Point();
                    $newPoint->latitude  = $point->latitude;
                    $newPoint->longitude = $point->longitude;
                    $newPoint->data_id   = $newData->id;
                    $newPoint->save();
                }

                $images = Image::where('data_id',$clone->id)->get();

                foreach ($images as $image)
                {
                    $newImage = new Image();
                    $newImage->extension = $image->extension;
                    $newImage->filename  = $image->filename;
                    $newImage->data_id   = $newData->id;
                    $newImage->save();

                    Log::info($image->id);
                    Log::info($newImage->id);
                    
                    Storage::copy('points/'.$image->id.'.'.$image->extension,'points/'.$newImage->id.'.'.$newImage->extension);

                    Log::info("lo hace");
                }

            }

        }

        return redirect('admin/private-map/'.$map->id);
        

        
    }

    /**
     * Muestra un mapa privado en específico
     *
     * @param  int  $id
     * @return Vista con mapa privado, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function privateMap($id) {

        $map = Map::where('id', $id)->first();
        if (!isset($map)) {
            return back()->withErrors('El mapa seleccionado no existe');
        }

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                array_push($names, $dato->title);
                if($point != null) array_push($points, [$point->latitude, $point->longitude]);

            }
        }

        $map_preferences = Map_private_preferences::where('map_id', $map->id);
        $map_preferences = $map_preferences->where("user_id",Auth::user()->id);
        $map_preferences = $map_preferences->first();

        if($map_preferences == null)
        {
            $map_preferences = new \stdClass();
            $map_preferences->latitude  = '-41.3214705000';
            $map_preferences->longitude = '-73.0139758000';
            $map_preferences->zoom      = 10;
            $map_preferences->map_style = 'basic';
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }

        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }
        
        $drawline   = 1;
        $dlp        = 1;

        return view('admin.maps.private', compact('map', 'themes', 'categories', 'names', 'points', 'map_preferences','clist','tlist','drawline','dlp'));
    }

    /**
     * Muestra un mapa público en específico
     *
     * @param  int  $id
     * @return Vista con mapa publico, entrega además categorías, temas, nombres de datos y coordenadas (para búsqueda)
     */
    public function publicMap(){
        $map = Map::where('name', 'Public map')->first();

        $category_ids = Map_cat::where('map_id', $map->id)->get();
        
        $categories = [];
        $count = 0;
        foreach ($category_ids as $ids) {
            $category_selected = Category::where('id', $ids->category_id)->first();
            $categories[$count] = $category_selected;
            $count += 1;
        }

        $theme_ids = Map_theme::where('map_id', $map->id)->get();
        
        $themes = [];
        $count = 0;
        foreach ($theme_ids as $ids) {
            $theme_selected = Theme::where('id', $ids->theme_id)->first();
            $themes[$count] = $theme_selected;
            $count += 1;
        }

        $data = Map_data::where('map_id', $map->id)->get();

        $names = [];
        $points = [];
        if ($data != null) {
            foreach($data as $key){
                $dato = Data::whereId($key->data_id)->first();

                $point = Point::where('data_id', $key->data_id)->first();

                if($point != null)
                {
                    array_push($names, $dato->title);
                    array_push($points, [$point->latitude, $point->longitude]);
                }

                

            }
        }

        $deleteCat = MapCategoryPreference::where('map_id',$map->id);
        $deleteCat = $deleteCat->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $clist = [];
        $dcatlist = [];

        foreach ($deleteCat as $delCat)
        {
            $dcatlist[] =  $delCat->category_id;
        }

        if(count($dcatlist) != 0)
        {
            $lists = Map_cat::where('map_id',$map->id)->whereNotIn('category_id',$dcatlist)->get();

            foreach ($lists as $list)
            {
                $clist[] = $list->category_id;
            }

        }

        $deleteThm = MapThemePreference::where('map_id',$map->id);
        $deleteThm = $deleteThm->where('user_id',Auth::user()->id)->get();

        $count = 0;
        $tlist = [];
        $dthmlist = [];

        foreach ($deleteThm as $delThm)
        {
            $dthmlist[] =  $delThm->theme_id;
        }


        if(count($dthmlist) != 0)
        {
            $lists = Map_theme::where('map_id',$map->id)->whereNotIn('theme_id',$dthmlist)->get();

            foreach ($lists as $list)
            {
                $tlist[] = $list->theme_id;
            }

        }


        return view('admin.maps.public', compact('map', 'themes', 'categories', 'names', 'points','clist','tlist'));
    }

    /**
     * Muestra vista para edición de mapa
     *
     * @param  int  $map_id
     * @return Vista para edición de mapa con categorías y temas asociados
     */
    public function editMap($map_id){

        $map = Map::whereId($map_id)->first();
        if ($map_id == "a24e7a20-aa42-11e7-bcd0-59be6b373a54") {
            return back()->withErrors('El mapa público no puede ser editado');
        }

        if ($map != null) {

            $map_cat = Map_cat::where('map_id', $map->id)->get();
            $category = [];
            for ($i = 0; $i < count($map_cat); $i++) {
                $category[$i] = $map_cat[$i]->category_id;
            }

            $map_theme = Map_theme::where('map_id', $map->id)->get();
            $theme = [];
            for ($i = 0; $i < count($map_theme); $i++) {
                $theme[$i] = $map_theme[$i]->theme_id;
            }

            return view('admin.maps.edit', compact('map', 'category', 'theme'));
        } else {
            return back()->withErrors('Se ha producido un error');
        }

    }

    /**
     * Actualiza mapa seleccionado
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Vuelve a la vista anterior con mensaje de éxito
     */
    public function updateMap($id, Request $request) {

        $categories = $request->get('categories');
        $themes = $request->get('themes');
        $title = $request->get('title');
        $latitude   = $request->get('latitude');
        $longitude  = $request->get('longitude');
        $map_style  = $request->get('style');
        $zoom       = $request->get('zoom');

        if ($title == null) {
            return back()->withErrors('El mapa debe llevar un nombre');
        }
        if ($categories == null || $themes == null) {
            return back()->withErrors('El mapa debe tener al menos una categoría y un tema asociados');
        }

        $map = Map::whereId($id)->first();
        if ($map != null) {
            $map->name = $title;
            $map->save();
        } else {
            return back()->withErrors('Se ha producido un error');
        }

        $map_cat = Map_cat::where('map_id', $id)->get();
        foreach($map_cat as $key){
            $key->delete();
        }

        $map_theme = Map_theme::where('map_id', $id)->get();
        foreach($map_theme as $key){
            $key->delete();
        }

        foreach($categories as $key){
            Map_cat::create([
                'map_id' => $id,
                'category_id' => $key
            ]);
        }

        foreach($themes as $key){
            Map_theme::create([
                'map_id' => $id,
                'theme_id' => $key
            ]);
        }

        $map_preferences = Map_private_preferences::where('map_id', $id)->first();

        if ($latitude == '') {
            $latitude = $map_preferences->latitude;
        }

        if ($longitude == '') {
            $longitude = $map_preferences->longitude;
        }

        if ($zoom == '') {
            $zoom = $map_preferences->zoom;
        }

        $map_preferences->latitude = $latitude;
        $map_preferences->longitude = $longitude;
        $map_preferences->zoom = $zoom;
        $map_preferences->map_style = $map_style;
        $map_preferences->save();

        return back()->with('status', 'El mapa ha sido actualizado');
    }

    /**
     * Elimina un mapa seleccionado
     *
     * @param  int  $id
     * @return Retorna a la vista anterior con mensaje de éxito o fracaso en caso de no existir mapa
     */
    public function destroyMap($map_id){

        $map = Map::whereId($map_id)->first();
        if ($map_id == "a24e7a20-aa42-11e7-bcd0-59be6b373a54") {
            return back()->withErrors('El mapa público no puede ser eliminado');
        }

        if ($map != null) {

            $map->delete();
            return back()->with('status', 'El mapa ha sido eliminado');
        } else {
            return back()->withErrors('Se ha producido un error');
        }
    }

    /**
     * Almacena un nuevo dato asociado a un mapa
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id de mapa
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addData(Request $request){

        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }

        $id = $request->input("id");

        $title = $request->get('title');
        $description = $request->get('description');
        $categories = $request->get('categories');
        $themes = $request->get('themes');
        //$image_file = $request->get('image');
        //$image_type = $request->get('image_ext');
        $type  = $request->get('type');

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => 1,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id,
            'approved_by' => Auth::user()->id,
            'approved_at' => Carbon::now()
        ]);

        Map_data::create([
            'map_id' => $id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');

            $coordinates = explode(",",$coordinates);

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }


        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();
                $filename  = $fl->getClientOriginalName();

                $image = new Image();
                $image->extension = $extension;
                $image->filename  = $filename;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }

        /*if ($image_file != null) {
            for($i = 0; $i < count($image_file); $i++){
                Data_image::create([
                    'image_file' => $image_file[$i],
                    'image_ext'  => $image_type[$i],
                    'data_id'    => $data->id
                ]);
            }
        }*/

        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();
        $data->errors = null;

        return $data;
    }

    /**
     * Agrega datos al mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @return retorna el nuevo punto ingresado para ser desplegado en el mapa una vez guardado
     */
    public function addPublicData(Request $request){

        $validator = Validator::make($request->all(),[
            'file.*' => 'sometimes|mimes:jpg,jpeg,png,bmp|max:20000'
        ]);

        if ($validator->fails())
        {
            $errors = ['errors' => $validator->errors()];
            return $errors;
        }

        $map = Map::where('name', 'Public map')->first();

        $title = $request->get('title');
        $description = $request->get('description');
        $categories = $request->get('categories');
        $themes = $request->get('themes');
        $image_file = $request->get('image');
        $image_type = $request->get('image_ext');
        $type  = $request->get('type');
        $status = $request->get("status");

        if($status == 2) $status = 1;

        $data = Data::create([
            'title' => $title,
            'description' => $description,
            'status' => $status,
            'type' => $type,
            'theme_id' => $themes[0],
            'category_id' => $categories[0],
            'created_by' => Auth::user()->id,
            'approved_by' => Auth::user()->id
        ]);

        Map_data::create([
            'map_id' => $map->id,
            'data_id' => $data->id
        ]);

        if ($type == 'point') {
            $latitude = $request->get('latitude');
            $longitude = $request->get('longitude');

            Point::create([
                'latitude' => $latitude,
                'longitude' => $longitude,
                'data_id' => $data->id
            ]);
        } else {

            $coordinates = $request->get('coordinates');
            
            $coordinates = explode(",",$coordinates);

            for($i = 0; $i < count($coordinates); $i = $i + 2) {

                Point::create([
                    'longitude' => $coordinates[$i],
                    'latitude' => $coordinates[$i+1],
                    'data_id' => $data->id
                ]);
            }

        }

        if($request->hasFile('file'))
        {
            $files = $request->file('file');

            foreach ($files as $fl)
            {
                $extension = $fl->getClientOriginalExtension();
                $filename  = $fl->getClientOriginalName();

                $image = new Image();
                $image->extension = $extension;
                $image->filename  = $filename;
                $image->data_id   = $data->id;

                $image->save();

                $fl->storeAs("points",$image->id.".".$extension);

            }

        }


        $data = Data::whereId($data->id)->first();

        $data->points = Point::where('data_id', $data->id)->get();

        $data->images = Image::where('data_id', $data->id)->get();

        return $data;
    }

    /**
     * Trae todos los puntos y su información, asociados a un mapa
     * estos puntos deben tener status 1 (aprobados)
     *
     * @param  int $id
     * @return Arreglo de puntos 
     */
    public function data($id) {

        $map_data = Map_data::where('map_id', $id)->get();

        $response = array();

        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }
        }

        return $response;
    }

    /**
     * Trae todos los datos asociados al mapa público
     * estos puntos deben tener status 1 (aprobados)
     *
     * @return Arreglo de puntos asociados a mapa público
     */
    public function publicData() {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();

        foreach($map_data as $key) {

            $matchThese = [
                'id' => $key->data_id,
                'status' => 1
            ];

            $data = Data::where($matchThese)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id' => $key->data_id,
                'status' => 0,
                'created_by' => Auth::user()->id
            ];

            $data = Data::where($matchThese)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }

            $matchThese = [
                'id' => $key->data_id,
                'status' => 2,
                'created_by' => Auth::user()->id
            ];

            $data = Data::where($matchThese)->first();

            if ($data != null) {
                $data->points = Point::where('data_id', $data->id)->get();

                $data->images = Image::where('data_id',$data->id)->get();

                array_push($response, $data); 
            }

        }

        return $response;
    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de categoría específico
     *
     * @param  int $id de categoría, int $map corresponde a aid de mapa
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryData($map, $id) {

        $map_data = Map_data::where('map_id', $map)->get();

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1)->first();
            
            if ($data != null)
            {   
                if ($data->category_id == $id) 
                {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id',$data->id)->get();
    
                    array_push($response, $data); 
              
               }
            }
            
        } 

        return $response; 
    }

    /**
     * Trae todos los puntos asociados a un mapa específico con un id de tema específico
     *
     * @param  int $id de tema, int $map corresponde a id de mapa
     * @return Arreglo de puntos de tema específico
     */
    public function showThemeData($map, $id) {

        $map_data = Map_data::where('map_id', $map)->get();

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1)->first();
            
            if ($data != null)
            { 
            
                if ($data->theme_id == $id) {
                        $data->points = Point::where('data_id', $data->id)->get();
                        $data->images = Image::where('data_id',$data->id)->get();

                        array_push($response, $data); 
                
                }
            }
        } 

        return $response; 
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de categoría específico
     *
     * @param  int $id de categoría
     * @return Arreglo de puntos de categoría específica
     */
    public function showCategoryPublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1)->first();

            if ($data != null)
            { 
                if ($data->category_id == $id) {
                        $data->points = Point::where('data_id', $data->id)->get();
                        $data->images = Image::where('data_id',$data->id)->get();

                        array_push($response, $data); 
                
                }
            }

            $data = new Data();
            $data = $data->whereId($key->data_id); 
            $data = $data->whereIn('status',[0,2]);
            $data = $data->where('created_by',Auth::user()->id);
            $data = $data->first();
            
            if ($data != null)
            { 
                if ($data->category_id == $id) 
                {
                    $data->points = Point::where('data_id', $data->id)->get();
                    $data->images = Image::where('data_id',$data->id)->get();
            
                    array_push($response, $data); 
                            
                }
            }

        } 

        return $response; 
    }

    /**
     * Trae todos los puntos asociados a mapa público con un id de tema específico
     *
     * @param  int $id de tema
     * @return Arreglo de puntos de tema específico
     */
    public function showThemePublicData($id) {

        $map = Map::where('name', 'Public map')->first();

        $map_data = Map_data::where('map_id', $map->id)->get();

        $response = array();
        foreach($map_data as $key) {

            $data = Data::whereId($key->data_id)->where("status",1)->first();

            if ($data != null)
            { 

                    if ($data->theme_id == $id) {
                        $data->points = Point::where('data_id', $data->id)->get();
                        $data->images = Image::where('data_id',$data->id)->get();

                        array_push($response, $data); 
                
                }
            }


            $data = new Data();
            $data = $data->whereId($key->data_id); 
            $data = $data->whereIn('status',[0,2]);
            $data = $data->where('created_by',Auth::user()->id);
            $data = $data->first();
            
                        if ($data != null)
                        { 
            
                                if ($data->theme_id == $id) {
                                    $data->points = Point::where('data_id', $data->id)->get();
                                    $data->images = Image::where('data_id',$data->id)->get();
            
                                    array_push($response, $data); 
                            
                            }
                        }

        } 

        return $response; 
    }

    /**
     * Muestra todos los datos creados en la app
     *
     * @return Vista de datos con todos los puntos de la app más usuario creador, cateogría y tema, además de mapa al que 
     * pertenecen
     */
    public function showData(){

        $category = Category::all();
        $theme    = Theme::all();
        $maps     = Map::all();
        $users    = User::all();

        /*$data = new Data();
        $data = $data->where("status","<>",0);
        $data = $data->get();

        if ($data != null) {
            foreach($data as $key){

                $map = Map_data::where('data_id', $key->id)->first();
                $map = Map::whereId($map->map_id)->first();
                $user = User::whereId($key->created_by)->first();
                $category = Category::whereId($key->category_id)->first();
                $theme = Theme::whereId($key->theme_id)->first();

                $key->map     = $map->name;
                $key->map_id  = $map->id;

                if($map->name == 'Public map') $key->public = 1;
                else $key->public = 0;

                $point = Point::where('data_id',$key->id)->first();

                $key->latitude  = $point->latitude;
                $key->longitude = $point->longitude; 

                $key->user = $user->name;
                $key->category = $category->category_name;
                $key->theme = $theme->themes_name;
            }
        }*/

        return view('admin.data.index',compact('category','theme','maps','users'));
    }

    /**
     * Destruye un punto asociado a un mapa 
     *
     * @param  int $id 
     * @return Vista anterior con mensaje de éxito 
     */
    public function destroyData($id){

        $data_image = Image::where('data_id', $id)->get();
        if ($data_image != null) {
            foreach($data_image as $key){
                $key->delete();
            }
        }

        $points = Point::where('data_id', $id)->get();
        if ($points != null) {
            foreach($points as $key){
                $key->delete();
            }
        }

        $map_data = Map_data::where('data_id', $id)->get();
        if ($map_data != null) {
            foreach($map_data as $key){
                $key->delete();
            }
        }

        $data = Data::whereId($id)->first();
        $data->delete();

        return back()->with('status', 'Se ha eliminado el dato seleccionado');
    }

    /**
     * Trae un punto específico
     *
     * @param  int $id 
     * @return Vista edición de dato
     */
    public function editData($id){

        $data = Data::whereId($id)->first();

        $point = Point::where("data_id",$data->id)->first();
        
        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }
        
        $mdata = Map_data::where("data_id",$data->id)->first();
        
        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();
        
        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();

        return view('admin.data.edit', compact('data','point','cat','themes'));
    }

    public function seeData($id){

        $data = Data::whereId($id)->first();

        $point = Point::where("data_id",$data->id)->first();
        
        if($point == null)
        {
            $point            = new \stdClass();
            $point->latitude  = 0;
            $point->longitude = 0;
        }
        
        $mdata = Map_data::where("data_id",$data->id)->first();
        
        $cat = new Category();
        $cat = $cat->join("map_cat","categories.id","=","map_cat.category_id");
        $cat = $cat->where("map_cat.map_id",$mdata->map_id);
        $cat = $cat->select(["categories.id","categories.category_name"])->get();
        
        $themes = new Theme();
        $themes = $themes->join("map_theme","themes.id","=","map_theme.theme_id");
        $themes = $themes->where("map_theme.map_id",$mdata->map_id);
        $themes = $themes->select(["themes.id","themes.themes_name"])->get();

        return view('admin.data.show', compact('data','point','cat','themes'));
    }

    /**
     * Actualiza información de dato, se puede actualizar titulo, descripción y estado de dato (publicado, rechazado)
     *
     * @param  int $id 
     * @param  \Illuminate\Http\Request  $request
     * @return Vista anterior con mensaje de éxito 
     */
    public function storeData($id, Request $request) {

        $title = $request->get('title');
        $description = $request->get('description');
        $status = $request->get('status');
        $category = $request->get("category");
        $theme    = $request->get("theme");

        $data = Data::whereId($id)->first();

        if($description != null) {
            $data->description = $description;
        }

        if($data->type == 'point')
        {
            $point = Point::where('data_id',$data->id)->first();

            if($point != null)
            {
                $point->latitude  = $request->input("latitude");
                $point->longitude = $request->input("longitude");

                $point->save();
            }

        }

        $data->title = $title;
        if($status != 3) $data->status      = $status;
        if($status == 1){
            $data->approved_by = Auth::user()->id;
            $data->approved_at = Carbon::now();
        } 
        $data->category_id = $category;
        $data->theme_id    = $theme;
        $data->save();

        if($data->status == 1)
        {
            $user = User::find($data->created_by);
            $map  = Map_data::where('data_id',$data->id)->first();
            $map  = Map::find($map->map_id);

            Mail::to($user->email)->queue(new SendPubMail($map,$user,$data));
        }

        if($status == 1) return redirect("admin/data");

        return back()->with('status', 'El dato ha sido actualizado');
    }


    /**
     * Aprueba un dato cambiando el estado de este
     *
     * @param  int $id 
     * @return Vista anterior con mensaje de éxito 
     */
    public function approveData($id){

        $data = Data::whereId($id)->first();

        $data->status      = 1;
        $data->approved_by = Auth::user()->id;
        $data->approved_at = Carbon::now();

        $data->save();

        $user = User::find($data->created_by);
        $map  = Map_data::where('data_id',$data->id)->first();
        $map  = Map::find($map->map_id);

        Mail::to($user->email)->queue(new SendPubMail($map,$user,$data));

        return back()->with('status', 'El dato ha sido aprobado');
    }

    /**
     * Trae el formulario de personalización de mapa público
     *
     * @return Vista de personalización
     */
    public function personalize(){

        $pref = Map_Preferences::where('user_id',Auth::user()->id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where('name','Public map')->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }


        return view('admin.maps.personalizeForm',compact('pref','categories','mcps','themes','mtps'));
    }

    public function personalizeprivate($id)
    {
        $pref = Map_private_preferences::where('user_id',Auth::user()->id);
        $pref = $pref->where('map_id',$id)->first();

        if($pref == null)
        {
            $pref = new \stdClass();
            $pref->latitude  = '-41.3214705000';
            $pref->longitude = '-73.0139758000';
            $pref->zoom      = 10;
            $pref->map_style = 'basic';
        }

        $map = Map::where("id",$id)->first();

        $cat = Map_cat::where('map_id',$map->id);
        $cat = $cat->join("categories","map_cat.category_id","=","categories.id");
        $cat = $cat->select(['categories.id','categories.category_name'])->get();


        $categories = [];

        foreach($cat as $c)
        {
            $categories[$c->id] = $c->category_name;
        }

        $mcp = MapCategoryPreference::where('map_id',$map->id);
        $mcp = $mcp->where('user_id',Auth::user()->id);
        $mcp = $mcp->join("categories","map_category_preferences.category_id","=","categories.id");
        $mcp = $mcp->select(['categories.id','categories.category_name'])->get();

        $mcps = [];

        if($mcp->count() == 0)
        {
            foreach($cat as $c)
            {
                $mcps[$c->id] = $c->category_name;
            }
        }
        else
        {
           
            foreach($mcp as $mc)
            {
                $mcps[$mc->id] = $mc->category_name;
            }
        }

        $thm = Map_theme::where('map_id',$map->id);
        $thm = $thm->join("themes","map_theme.theme_id","=","themes.id");
        $thm = $thm->select(['themes.id','themes.themes_name'])->get();

        $themes = [];

        foreach($thm as $t)
        {
            $themes[$t->id] = $t->themes_name;
        }

        $mtp = MapThemePreference::where('map_id',$map->id);
        $mtp = $mtp->where('user_id',Auth::user()->id);
        $mtp = $mtp->join("themes","map_theme_preferences.theme_id","=","themes.id");
        $mtp = $mtp->select(['themes.id','themes.themes_name'])->get();

        $mtps = [];

        if($mtp->count() == 0)
        {
            foreach($thm as $t)
            {
                $mtps[$t->id] = $t->themes_name;
            }
        }
        else
        {
           
            foreach($mtp as $mt)
            {
                $mtps[$mt->id] = $mt->themes_name;
            }
        }

        return view('admin.maps.personalizePrivateForm',compact('pref','map','categories','mcps','themes','mtps'));
    }

    /**
     * Define preferencias de mapa público
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Vista anterior con mensaje de éxito 
     */
    public function setPreferences(Request $request) {

        $categories = $request->get('categories'); 
        $themes     = $request->get('themes');
        $latitude   = $request->get('latitude');
        $longitude  = $request->get('longitude');
        $zoom       = $request->get('zoom');
        $style      = $request->get('style');

        if(count($categories) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una categoría');
        }

        if(count($themes) == 0)
        {
            return back()->withErrors('Debe seleccionar al menos una tema');
        }

        if ($latitude == '') {
            $latitude = '-41.3214705000';
        }

        if ($longitude == '') {
            $longitude = '-73.0139758000';
        }

        if ($zoom == '') {
            $zoom = 10;
        }

        if ($style == '') {
            $style = 'basic';
        }

        $user = Auth::user();

        $user_id = $user->id;

        //primero eliminamos todas las preferencias que ya existen para ese usuario
        $map_key = null;

        if($request->has('map_id'))
        {
            $del     = MapCategoryPreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$request->input("map_id"));
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $request->input("map_id");
        }
        else
        {
            $map     = Map::where('name','Public map')->first();

            $del     = MapCategoryPreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();
            
            $del     = MapThemePreference::where('map_id',$map->id);
            $del     = $del->where('user_id',Auth::user()->id)->delete();

            $map_key = $map->id;

        }

        foreach($categories as $cat)
        {
            $mcp = new MapCategoryPreference();
            $mcp->map_id      = $map_key;
            $mcp->user_id     = Auth::user()->id;
            $mcp->category_id = $cat;
            $mcp->save(); 
        }

        foreach($themes as $thm)
        {
            $mtp = new MapThemePreference();
            $mtp->map_id      = $map_key;
            $mtp->user_id     = Auth::user()->id;
            $mtp->theme_id = $thm;
            $mtp->save(); 
        }


        if($request->has('map_id'))
        {
            $map_preferences = Map_private_preferences::where('user_id', $user_id);
            $map_preferences = $map_preferences->where('map_id',$request->input('map_id'))->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_private_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id,
                    'map_id'    => $request->input('map_id')
                ]);
            }    
        }
        else
        {
            $map_preferences = Map_preferences::where('user_id', $user_id)->first();

            if ($map_preferences) {
                $map_preferences->latitude  = $latitude;
                $map_preferences->longitude = $longitude;
                $map_preferences->zoom      = $zoom;
                $map_preferences->map_style     = $style;
                $map_preferences->save(); 
            } else {
                Map_preferences::create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'zoom'      => $zoom,
                    'map_style' => $style,
                    'user_id'   => $user_id
                ]);
            }
        }

            

        $permission = Permission::where('user_id', $user_id)->get();

        $user_maps = array();

        foreach ($permission as $key) {

            $map = Map::where('id', $key->map_id)->first();

            array_push($user_maps, $map);
        }

        $user_maps = array_unique($user_maps);

        $len = count($user_maps);
        for ($i=0; $i < $len; $i++) { 
            if ($user_maps[$i]['name'] == 'Public map'){
                unset($user_maps[$i]);
            }
        }

        return back()->with('status', 'Las preferencias han sido guardadas');
    }

    /**
     * aprueba una iniciativa ingresada por un usuairo
     *
     * @param int $id iniciativa
     * @param  Vista anterior con mensaje de éxito 
     */
    public function acceptInitiative($id)
    {
        $initiative = Initiative::whereId($id)->first();

        $initiative->status_id = 5;

        $initiative->save();

        return back()->with('status', 'La iniciativa ha sido aceptada y ahora está publicada');
    }

    /**
     * Devuelve formulario para ingresar motivo de rechazo de la iniciativa
     *
     * @param int $id iniciativa
     * @param Vista con formulario para ingresar rechazo
     */
    public function rejectInitiative($id)
    {

        $initiative_id = $id;

        return view('admin.initiatives.rejectionForm', compact('initiative_id'));
    }

    /**
     * Realiza rechazo de una iniciativa, almacenando motivo de este además de cambiar el estado de la inicaitiva
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Vista anterior con mensaje de iniciativa rechazada
     */
    public function reject(Request $request)
    {
        $initiative_id = $request->get('initiative');
        $motive = $request->get('motive');

        if($motive == null) {
            return back()->withErrors('Debes ingresar un motivo de rechazo');
        }

        $initiative = Initiative::whereId($initiative_id)->first();

        $initiative->status_id = 4;

        $initiative->save();

        Rejection::create([
            'initiative_id' => $initiative_id,
            'rejection_description' => $motive     
        ]);

        return back()->with('status', 'La iniciativa ha sido rechazada');
    }

    public function cloneData(Request $request,$id)
    {
        
        $map  = Map::where('name','Public map')->first();
        $data = Data::where('id',$id)->first();

        if ($data == null)
        {
            return back()->withErrors('Dato no existe');
        }

        $newData              = new Data();
        $newData->title       = $data->title;
        $newData->description = $data->description;
        $newData->created_by  = Auth::user()->id;
        $newData->type        = $data->type;
        $newData->theme_id    = $data->theme_id;
        $newData->category_id = $data->category_id;
        $newData->status      = 2;
        
        $newData->save();

        //asociamos punto/poligono a mapa 
        $mapData           = new Map_data();
        $mapData->map_id   = $map->id;
        $mapData->data_id  = $newData->id;
        $mapData->save();

        //copiamos coordenadas 
        $points = Point::where('data_id',$data->id)->get();

        foreach ($points as $point)
        {
            $newPoint = new Point();
            $newPoint->latitude  = $point->latitude;
            $newPoint->longitude = $point->longitude;
            $newPoint->data_id   = $newData->id;
            $newPoint->save();
        }

        $images = Image::where('data_id',$data->id)->get();

        foreach ($images as $image)
        {
            $newImage = new Image();
            $newImage->extension = $image->extension;
            $newImage->filename  = $image->filename;
            $newImage->data_id   = $newData->id;
            $newImage->save();
            
            Storage::copy('points/'.$image->id.'.'.$image->extension,'points/'.$newImage->id.'.'.$newImage->extension);

        }

        
        return back()->with('status', 'El dato fue copiado de forma exitosa');
    }

    public function drawGrid(Request $request)
    {
        $lst = [];


        $data = new \stdClass();
        
        $start = $request->input('start');
        $length = $request->input('length');
        $draw = $request->input('draw');
                
        $columns = $request->input('columns');
        $order = $request->input('order');
        
        $dir = $order[0]['dir'];
        $field = $columns[$order[0]['column']]['data'];
                
        $with = $request->input('with');
        
        $status   = $request->input('status');
        $category = $request->input('category');
        $theme    = $request->input('theme');
        $map      = $request->input('map');
        $user     = $request->input('user');
        $title    = $request->input('title');
        $created  = $request->input('created');
        $pub      = $request->input('pub');
        $offset   = $request->input('offset');
        $offsetp   = $request->input('offsetp');

        $type     = $request->input('type'); 

        $datas = new Data();
        $datas  = $datas->join("map_data","data.id","=","map_data.data_id");
        $datas  = $datas->join("maps","map_data.map_id","=","maps.id");
        $datas  = $datas->join("themes","data.theme_id","=","themes.id");
        $datas  = $datas->join("categories","data.category_id","=","categories.id");
        $datas  = $datas->join("users","data.created_by","=","users.id");

        $list = DB::table('data')->whereIn('status',[0,3]);
        $list = $list->where('created_by','<>',Auth::user()->id)->get();

        foreach($list as $l)
        {
            $lst[] = $l->id;
        }

        $datas  = $datas->whereNotIn('data.id',$lst);
        

        if ($start !== null && $length !== null) 
		{
            if ($order !== null) 
			{
                if($field != 'id') $datas = $datas->orderBy($field, $dir);
            }

            if($status != 99)
            {
                $datas = $datas->where("data.status","=",DB::raw($status));
            }

            if($category != 0)
            {
                $datas = $datas->where("data.category_id","=",DB::raw($category));
            }

            if($theme != 0)
            {
                $datas = $datas->where("data.theme_id","=",DB::raw($theme));
            }

            if($map != 0)
            {
                $datas = $datas->where("map_data.map_id","=",$map);
            }

            if($user != 0)
            {
                $datas = $datas->where("data.created_by","=",$user);
            }

            if($title != '')
            {
                $datas = $datas->where("data.title","like",'%'.$title.'%');
            }

            if($created != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.created_at + INTERVAL ".$offset." MINUTE  )"),"=",$created);
            }

            if($pub != '')
            {
                $datas = $datas->where(DB::raw("DATE(data.approved_at + INTERVAL ".$offsetp." MINUTE  )"),"=",$pub);
            }

            if($type == 'general')
            {
                $datas = $datas->where('data.created_by','=',Auth::user()->id);
            }


            $data->draw = $draw;
            $data->recordsFiltered = $datas->count();

            if ($length != -1) 
			{
                $datas = $datas->offset($start)->limit($length);
            }

            $datas = $datas->select(['data.id',DB::raw("IF(maps.name = 'Public map',1,0) as flag"),'data.created_at','data.approved_at','data.title','categories.category_name as cat','themes.themes_name as theme','maps.name as map',DB::raw('CONCAT(users.name," ",IFNULL(users.surname,"")) as user'),'data.status','maps.id as map_id']);

            $datas = $datas->orderBy(DB::raw('flag'),'desc');
            $datas = $datas->orderBy('maps.name','asc');

            $data->datas = $datas->get();

            foreach($data->datas as $dt)
            {
                $point = Point::where('data_id',$dt->id)->first();

                if($point != null)
                {
                    $dt->latitude  = $point->latitude;
                    $dt->longitude = $point->longitude;
                }
                
                 
            }

            $data->recordsTotal = Data::count();

            return response()->json([
                'success' => true,
                'code' => 200,
                'message' => 'ok',
                'data' => $data
            ], 200);

        }

    }

    public function rejectData(Request $request,$id)
    {

        $data = Data::find($id);

        if($data == null)
        {
            return response()->json([
                'success' => true,
                'code' => 404,
                'message' => 'No se encuentra el dato seleccionado',
                'data' => null
            ], 200);
        }

        if($data->status == 3)
        {
            return response()->json([
                'success' => true,
                'code' => 400,
                'message' => 'Dato ya se encuentra rechazado',
                'data' => null
            ], 200);
        }

        $user = User::find($data->created_by);

        $cause  = $request->input('cause');
        $causet = $request->input('other');

        $data->status = 3;
        $data->save();

        Mail::to($user->email)->queue(new SendRejectMail($user,$data,$cause,$causet));

        

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Dato rechazado con éxito',
            'data' => null
        ], 200);

    }

    public function multiClone(Request $request)
    {

        $success = true;
        $msg     = '';

        $map  = Map::where('name','Public map')->first();

        $ids = $request->input('ids');

        $ids = explode(',',$ids);

        

        foreach($ids as $id)
        {
            $info = Map_data::where('data_id',$id)->first();

            if($info == null)
            {
                $msg     = 'Uno de los datos presentes no existe';
                $success = false;
                break; 
            }

            if($info->map_id == $map->id)
            {
                $msg     = 'Uno de los datos a clonar ya se encuentra en el mapa público';
                $success = false;
                break;
            }


        }

        if($success)
        {

            foreach($ids as $id)
            {
                $data = Data::where('id',$id)->first();

                $newData              = new Data();
                $newData->title       = $data->title;
                $newData->description = $data->description;
                $newData->created_by  = Auth::user()->id;
                $newData->type        = $data->type;
                $newData->theme_id    = $data->theme_id;
                $newData->category_id = $data->category_id;
                $newData->status      = 1; //PUBLICADO

                $newData->save();

                //asociamos punto/poligono a mapa 
                $mapData           = new Map_data();
                $mapData->map_id   = $map->id;
                $mapData->data_id  = $newData->id;
                $mapData->save();

                //copiamos coordenadas 
                $points = Point::where('data_id',$data->id)->get();

                foreach ($points as $point)
                {
                    $newPoint = new Point();
                    $newPoint->latitude  = $point->latitude;
                    $newPoint->longitude = $point->longitude;
                    $newPoint->data_id   = $newData->id;
                    $newPoint->save();
                }

                $images = Image::where('data_id',$data->id)->get();

                foreach ($images as $image)
                {
                    $newImage = new Image();
                    $newImage->extension = $image->extension;
                    $newImage->filename  = $image->filename;
                    $newImage->data_id   = $newData->id;
                    $newImage->save();
                    
                    Storage::copy('points/'.$image->id.'.'.$image->extension,'points/'.$newImage->id.'.'.$newImage->extension);

                }

            }

        }

        return response()->json([
            'success' => $success,
            'code' => 200,
            'message' => $msg,
            'data' => null
        ], 200);
        
    }

}
