<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Guardian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $user = Auth::user();
        if ($user == null) {
            return redirect('/login');
        }

        $user_rol = $user->roles->id;
        if ($user_rol != $role)
        {
            return redirect('/login');
        }
        return $next($request);
    }
}
