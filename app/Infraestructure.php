<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infraestructure extends Model
{
    protected $table = 'infraestructures';

    protected $fillable = ['infraestructure_name', 'infraestructure_description', 'infraestructure_money', 'infraestructure_importance', 'initiative_id'];
}
