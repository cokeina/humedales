<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Req_money extends Model
{
    protected $table = 'req_monetary';

    protected $fillable = ['takings_goal', 'takings_actual', 'takings_minimum', 'takings_optimum', 'initiative_id'];
}
