<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = ['title', 'description', 'map_id', 'type', 'status', 'theme_id', 'category_id','created_by','approved_by'];

    public function categories()
    {
        return $this->hasOne('App\Category','id','category_id');
    }

    public function themes()
    {
        return $this->hasOne('App\Theme','id','theme_id');
    }

}
