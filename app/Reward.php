<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $table = 'rewards';

    protected $fillable = ['reward_name', 'reward_description','reward_amount','reward_image','initiative_id', 'reward_type'];
}
