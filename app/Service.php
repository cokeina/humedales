<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $fillable = ['service_name', 'service_image','initiative_id', 'service_type', 'status'];
}
