<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map_theme extends Model
{
    protected $table = 'map_theme';

    protected $fillable = ['map_id', 'theme_id'];
}
