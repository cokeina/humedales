<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map_cat extends Model
{
    protected $table = 'map_cat';

    protected $fillable = ['map_id', 'category_id'];
}
