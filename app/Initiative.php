<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Initiative extends Model
{

    protected $table = 'initiatives';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'initiative_name',
            'email',
            'initiative_title',
            'initiative_resume',
            'initiative_motivation',
            'initiative_description',
            'start_date',
            'finish_date',
            'url_web',
            'url_fb',
            'url_tw',
            'url_yt',
            'user_id',
            'theme_id',
            'category_id',
            'status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
