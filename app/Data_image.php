<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data_image extends Model
{
	protected $table = 'data_image';

    protected $fillable = ['image_file', 'image_ext', 'data_id'];
}
