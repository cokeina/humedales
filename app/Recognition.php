<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recognition extends Model
{
    protected $table = 'recognitions';

    protected $fillable = ['recognition_name', 'recognition_image','initiative_id', 'recognition_type'];
}
