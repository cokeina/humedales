<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data_cat extends Model
{
    protected $table = 'data_cat';

    protected $fillable = ['data_id', 'category_id'];
}
