<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos_initiatives';

    protected $fillable = ['photo_name', 'photo_type', 'photo_file', 'initiative_id'];
}
