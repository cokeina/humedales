<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data_theme extends Model
{
    protected $table = 'data_theme';

    protected $fillable = ['data_id', 'theme_id'];
}
