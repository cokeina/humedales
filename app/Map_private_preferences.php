<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map_private_preferences extends Model
{
    protected $table  = 'map_preferences_private';

    protected $fillable = ['latitude', 'longitude', 'map_style', 'zoom', 'map_id','user_id'];
}
