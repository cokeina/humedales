<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'req_assets';

    protected $fillable = ['asset_name', 'status', 'initiative_id'];
}
