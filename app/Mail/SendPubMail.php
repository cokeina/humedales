<?php

namespace App\Mail;

use App\Map;
use App\Data;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPubMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $map;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Map $map, User $user, Data $data)
    {
        $this->map  = $map;
        $this->user = $user;
        $this->data = $data;  
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.publish')->subject('Publicación de dato');
    }
}
