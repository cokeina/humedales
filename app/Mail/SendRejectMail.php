<?php

namespace App\Mail;

use App\User;
use App\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRejectMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $data;
    public $cause;
    public $causet;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,Data $data,$cause,$causet)
    {
        $this->user   = $user;
        $this->data   = $data;
        $this->cause  = $cause;
        $this->causet = $causet; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.reject')->subject('Rechazo publicación de dato');
    }
}
