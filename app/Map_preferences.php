<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map_preferences extends Model
{
    protected $table  = 'map_preferences';

    protected $fillable = ['latitude', 'longitude', 'map_style', 'zoom', 'user_id'];
}
