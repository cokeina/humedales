<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map_data extends Model
{
    protected $table = 'map_data';

    protected $fillable = ['map_id', 'data_id'];
}
