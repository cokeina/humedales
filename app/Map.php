<?php

namespace App;

use App\Data;
use App\Map_data;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
	use Uuids;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = ['name', 'user_id'];

    public function data()
    {
        return $this->belongsToMany('App\Data','map_data')->with(['categories','themes']);
    }

}
