<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Req_service extends Model
{
    protected $table = 'req_services';

    protected $fillable = ['service_name', 'status', 'initiative_id'];
}
