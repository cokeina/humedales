<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@main');
Route::get('/what-is', 'PagesController@whatis');
Route::get('/initiatives', 'InitiativesController@index');
Route::get('/map', 'PagesController@maps');
Route::get('/download', 'PagesController@download');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'ContactController@send');
Route::get('/privacy', 'PagesController@privacy');
Route::get('/team', 'PagesController@team');
Route::get('/api', 'PagesController@api');
Route::get('/personalize-map', 'PagesController@personalizeMap');
Route::get('/personalize-private-map/{id}', 'PagesController@personalizePrivateMap');

Route::get('/downloads', 'FileController@index');
Route::resource('/files','Resources\FileController');
Route::get('/files/download/{id}','Resources\FileController@download');	

Route::post('/map/setpreferences', 'MapController@setpreferences');
Route::get('/map/export', 'MapController@export');

Route::get('/map/data', 'DataController@publicData');
Route::get('/map/category/{id?}', 'General\MapsController@showCategoryPublicData');
Route::get('/map/theme/{id?}', 'General\MapsController@showThemePublicData');
Route::get('/my-maps/preferences', 'DataController@preferences');
Route::get('/public-map/category/{id?}', 'DataController@showCategoryData');
Route::get('/public-map/theme/{id?}', 'DataController@showThemeData');
Route::get('/content/{id?}', 'DataController@content');

Route::get("/verifyemail/{token}", "Auth\RegisterController@verify");

Route::get('/initiatives/create/first-step', 'InitiativesController@createFirstStep');
Route::post('/initiatives/create/first-step', 'InitiativesController@storeFirstStep');
Route::get('/initiatives/create/second-step', 'InitiativesController@createSecondStep');
Route::post('/initiatives/create/second-step', 'InitiativesController@storeSecondStep');
Route::get('/initiatives/create/third-step', 'InitiativesController@createThirdStep');
Route::post('/initiatives/create/third-step', 'InitiativesController@storeThirdStep');
Route::get('/initiatives/create/last-step', 'InitiativesController@createLastStep');
Route::get('/initiatives/detail/{id?}', 'InitiativesController@show');

Route::get('/data/images','DataController@images');
Route::post('/data/image/upload','DataController@uploadimage');
Route::delete('/data/image/delete/{id}','DataController@deleteimage');

Route::get('/seedata/{id}','PagesController@seedata');

/** Redirect users route **/
Route::get('/redirect', 'PagesController@redirect');

/** Auth users routes **/
Route::get('/users/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/users/register', 'Auth\RegisterController@register');
Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');
/* Social Login con Facebook */
Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/** Admin routes **/
Route::group(array('prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'guardian:1'), function(){
	Route::get('/', 'AdminController@dashboard');
	/* Usuarios */
	Route::get('/users', 'AdminController@users');
	Route::get('/users/edit/{id?}', 'AdminController@editUser');
	Route::post('/users/edit/{id?}', 'AdminController@updateUser');
	Route::get('/users/delete/{id?}', 'AdminController@destroyUser');
	
	Route::get('/contents','AdminController@contents');
	Route::get('/contents/grid','ContentController@index');

	Route::post('/contents','ContentController@update');
	Route::get('/contents/{id}','ContentController@show');	

	/* Descargas */
	Route::get('/downloads', 'AdminController@downloads');

	/* Mapas */
	Route::get('/maps', 'AdminController@maps');
	Route::get('/maps/create', 'AdminController@createMap');
	Route::post('/maps/create', 'AdminController@storeMap');
	Route::get('/maps/edit/{id?}', 'AdminController@editMap');
	Route::post('/maps/edit/{id?}', 'AdminController@updateMap');
	Route::get('/maps/delete/{id?}', 'AdminController@destroyMap');
	/* Personalizar */
	Route::get('/my-maps/personalize', 'AdminController@personalize');
	Route::post('/my-maps/personalize', 'AdminController@setPreferences');
	Route::get('/my-maps/personalize/{id?}', 'AdminController@personalizeprivate');
	/* Privado */
	Route::get('/private-map/{id?}', 'AdminController@privateMap');
	Route::post('/private-map/add-data', 'AdminController@addData');
	Route::get('/private-map/{id?}/data', 'AdminController@data');
	Route::get('/private-map/{map?}/category/{id?}', 'AdminController@showCategoryData');
	Route::get('/private-map/{map?}/theme/{id?}', 'AdminController@showThemeData');
	/* Público */
	Route::post('/public-map/add-data', 'AdminController@addPublicData');
	Route::get('/public-map/data', 'AdminController@publicData');
	Route::get('/public-map/category/{id?}', 'AdminController@showCategoryPublicData');
	Route::get('/public-map/theme/{id?}', 'AdminController@showThemePublicData');
	Route::get('/public-map/', 'AdminController@publicMap');
	/* Datos */
	Route::get('/data', 'AdminController@showData');
	Route::get('/data/delete/{id?}', 'AdminController@destroyData');
	Route::get('/data/edit/{id?}', 'AdminController@editData');
	Route::get('/data/show/{id?}', 'AdminController@seeData');
	Route::post('/data/edit/{id?}', 'AdminController@storeData');
	Route::get('/data/approve/{id?}', 'AdminController@approveData');
	Route::get('/data/clone/{id?}', 'AdminController@cloneData');
	Route::get('/data/multiclone', 'AdminController@multiClone');
	Route::post('/data/reject/{id}', 'AdminController@rejectData');
	Route::get('/grid','AdminController@drawGrid');
	/* Iniciativas */
	Route::get('/initiatives', 'AdminController@initiatives');
	Route::get('/initiatives/reject/{id?}', 'AdminController@rejectInitiative');
	Route::post('/initiatives/reject/{id?}', 'AdminController@reject');
	Route::get('/initiatives/accept/{id?}', 'AdminController@acceptInitiative');
});

/** Curador routes **/
Route::group(array('prefix' => 'curador', 'namespace' => 'Curador', 'middleware' => 'guardian:2'), function(){
	Route::get('/', 'CuradorController@dashboard');
	/* Mapas */
	Route::get('/maps', 'MapsController@index');
	/* Privado */
	Route::get('/private-map/{id?}', 'MapsController@privateMap');
	Route::get('/private-map/{id?}/data', 'MapsController@data');
	Route::post('/private-map/add-data', 'MapsController@addData');
	Route::get('/private-map/{map?}/category/{id?}', 'MapsController@showCategoryData');
	Route::get('/private-map/{map?}/theme/{id?}', 'MapsController@showThemeData');
	/* Público */
	Route::post('/public-map/add-data', 'MapsController@addPublicData');
	Route::get('/public-map/data', 'MapsController@publicData');
	Route::get('/public-map/category/{id?}', 'MapsController@showCategoryPublicData');
	Route::get('/public-map/theme/{id?}', 'MapsController@showThemePublicData');
	Route::get('/public-map/', 'MapsController@publicMap');
	/* Personalizar */
	Route::get('/my-maps/personalize', 'MapsController@personalize');
	Route::post('/my-maps/personalize', 'MapsController@setPreferences');
	Route::get('/my-maps/personalize/{id?}', 'MapsController@personalizeprivate');
	/* Usuarios */
	Route::get('/users', 'CuradorController@users');
	Route::get('/users/edit/{id?}', 'CuradorController@editUser');
	Route::post('/users/edit/{id?}', 'CuradorController@updateUser');
	/* Datos */
	Route::get('/data', 'CuradorController@showData');
	Route::get('/data/delete/{id?}', 'CuradorController@destroyData');
	Route::get('/data/edit/{id?}', 'CuradorController@editData');
	Route::get('/data/show/{id?}', 'CuradorController@seeData');
	Route::post('/data/edit/{id?}', 'CuradorController@storeData');
	Route::get('/data/approve/{id?}', 'CuradorController@approveData');
	Route::post('/data/reject/{id}', 'CuradorController@rejectData');
	Route::get('/data/grid','CuradorController@grid');
	Route::get('/data/multiclone', 'CuradorController@multiClone');
});

/** General routes **/
Route::group(array('prefix' => 'general', 'namespace' => 'General', 'middleware' => 'guardian:3'), function(){
	Route::get('/', 'GeneralController@dashboard');
	/* Mapas */
	Route::get('/my-maps', 'MapsController@index');
	/* Personalizar */
	Route::get('/my-maps/personalize', 'MapsController@personalize');
	Route::post('/my-maps/personalize', 'MapsController@setPreferences');
	Route::get('/my-maps/personalize/{id?}', 'MapsController@personalizeprivate');
	
	// Route::get('/my-maps/create', 'MapsController@create');
	// Route::post('/my-maps/create', 'MapsController@store');
	/* Privado */
	Route::get('/private-map/{id?}', 'MapsController@privateMap');
	Route::post('/private-map/add-data', 'MapsController@addData');
	Route::get('/private-map/{id?}/data', 'MapsController@data');
	Route::get('/private-map/{map?}/category/{id?}', 'MapsController@showCategoryData');
	Route::get('/private-map/{map?}/theme/{id?}', 'MapsController@showThemeData');
	/* Público */
	Route::post('/public-map/add-data', 'MapsController@addPublicData');
	Route::get('/public-map/data', 'MapsController@publicData');
	Route::get('/public-map/category/{id?}', 'MapsController@showCategoryPublicData');
	Route::get('/public-map/theme/{id?}', 'MapsController@showThemePublicData');
	Route::get('/public-map', 'MapsController@publicMap');
	/* Iniciativas */
	Route::get('/initiatives', 'GeneralController@initiatives');

	/* Datos*/
	Route::get('/data/grid','MapsController@grid');
	Route::get('/data', 'MapsController@showData');
	Route::get('/data/delete/{id?}', 'MapsController@destroyData');
	Route::get('/data/edit/{id?}', 'MapsController@editData');
	Route::post('/data/edit/{id?}', 'MapsController@storeData');
	Route::get('/data/show/{id?}', 'MapsController@seeData');
	Route::post('/data/reject/{id}', 'MapsController@rejectData');
	Route::get('/data/multiclone', 'MapsController@multiClone');
	

});


