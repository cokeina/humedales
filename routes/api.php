<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {

    Route::get('user/maps','Api\MapsController@index');
    Route::get('map/{id?}/points','Api\MapsController@points');
    Route::post('map/{id?}/add','Api\MapsController@add');
    Route::get('user/points','Api\UserController@points');

});


Route::post('oauth/token','Api\LoginController@login');
Route::post('oauth/get', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');
