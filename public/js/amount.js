/*


CONTROLA ASPECTOS RELACIONADOS A LOS MONTOS EN EL PASO 3 DE LA CREACIÓN DE INICIATIVAS


*/



/* Activa/Desactiva checkbox */
function changeCheckbox($checkbox_id) {
	switch($checkbox_id){
		case '#type_amount_material':
			$('#type_amount_task').attr('checked', false);
			$('#type_amount_inf').attr('checked', false);
			break;
		case '#type_amount_task':
			$('#type_amount_inf').attr('checked', false);
			$('#type_amount_material').attr('checked', false);
			break;
		case '#type_amount_inf':
			$('#type_amount_task').attr('checked', false);
			$('#type_amount_material').attr('checked', false);
			break;
		case '#imp_amount':
			$('#adi_amount').attr('checked', false);
			break;
		case '#adi_amount':
			$('#imp_amount').attr('checked', false);
			break;
	}
}

/* Valida que todos los campos estén con datos */
$(document).on('click', '.amount-btn', function(){

	$name 	= $('#name_amount').val();
	$money 	= $('#money_amount').val();
	$material_checkbox = $('#type_amount_material').is(":checked");
	$task_checkbox = $('#type_amount_task').is(":checked");
	$inf_checkbox = $('#type_amount_inf').is(":checked");
	$description = $('#description_amount').val();
	$imp_amount = $('#imp_amount').is(":checked");
	$adi_amount = $('#adi_amount').is(":checked");

	if ($name.length == 0 || $money.length == 0 || $description.length == 0 || ($material_checkbox == false && $task_checkbox == false && $inf_checkbox == false) || ($imp_amount == false && $adi_amount == false)) {
		document.getElementById('response').innerHTML = "<p style='color:red;font-size:.8em;font-weight:bold;text-align:center;margin-top:1em'>(*) Algunos campos requeridos están vacíos</p>";
	}

	var div = document.createElement('div');
	if ($imp_amount) {
		$type = 'Imprescindible';
	} else {
		$type = 'Adicional';
	}


	if ($material_checkbox) {
	 	div.id 	= 'added_material';

		var input =	 "<div class='row' style='margin-bottom:2em'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$name+"' name='material_name[]' style='border:none;font-weight:bold;font-size:.9em;'>" +
		 				"</div>" +
		 			"</div>" +
	 				"<div class='row' style='margin-bottom: 1em;'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$description+"' name='material_description[]' placeholder='"+$description+"' style='border:none;width:100%;'/>" +
		 				"</div>" +
		 			"</div>" +
		 			"<div class='row' style='margin-bottom: 1em;border-bottom: 1px solid #DFE1E8'>" +
		 				"<div class='col-6'>" +
		 					"<input type='text' value='"+$type+"' name='material_type[]' style='border:none;'>" +
		 				"</div>" +
		 				"<div class='col-6'>" +
							"<input type='text' value='$"+$money+"' name='material_amount[]' style='border:none;text-align:right;width:100%'>" +
		 				"</div>" +
		 			"</div>";


	 	div.innerHTML = input;	

	 	var addedInputs = document.getElementById('material_amount');

	 	total_value = $('#material_total_amount').text();

	 	total_value = parseInt(total_value.split("$")[1]);

	 	total_value = total_value + parseInt($money); 

	 	$('#material_total_amount').html('$' + total_value);

	}

	if ($task_checkbox) {
		div.id 	= 'added_task';

		var input =	 "<div class='row' style='margin-bottom:2em'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$name+"' name='task_name[]' style='border:none;font-weight:bold;font-size:.9em;'>" +
		 				"</div>" +
		 			"</div>" +
	 				"<div class='row' style='margin-bottom: 1em;'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$description+"' name='task_description[]' placeholder='"+$description+"' style='border:none;width:100%;'>" +
		 				"</div>" +
		 			"</div>" +
		 			"<div class='row' style='margin-bottom: 1em;border-bottom: 1px solid #DFE1E8'>" +
		 				"<div class='col-6'>" +
		 					"<input type='text' value='"+$type+"' name='task_type[]' style='border:none;'>" +
		 				"</div>" +
		 				"<div class='col-6'>" +
							"<input type='text' value='$"+$money+"' name='task_amount[]' style='border:none;text-align:right;width:100%'>" +
		 				"</div>" +
		 			"</div>";


	 	div.innerHTML = input;	

	 	var addedInputs = document.getElementById('task_amount');

	 	total_value = $('#task_total_amount').text();

	 	total_value = parseInt(total_value.split("$")[1]);

	 	total_value = total_value + parseInt($money); 

	 	$('#task_total_amount').html('$' + total_value);
	}

	if ($inf_checkbox) {
		div.id 	= 'added_infraestructure';

		var input =	 "<div class='row' style='margin-bottom:2em'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$name+"' name='infraestructure_name[]' style='border:none;font-weight:bold;font-size:.9em;'>" +
		 				"</div>" +
		 			"</div>" +
	 				"<div class='row' style='margin-bottom: 1em;'>" +
		 				"<div class='col-12'>" +
		 					"<input type='text' value='"+$description+"' name='infraestructure_description[]' placeholder='"+$description+"' style='border:none;width:100%;'>" +
		 				"</div>" +
		 			"</div>" +
		 			"<div class='row' style='margin-bottom: 1em;border-bottom: 1px solid #DFE1E8'>" +
		 				"<div class='col-6'>" +
		 					"<input type='text' value='"+$type+"' name='infraestructure_type[]' style='border:none;'>" +
		 				"</div>" +
		 				"<div class='col-6'>" +
							"<input type='text' value='$"+$money+"' name='infraestructure_amount[]' style='border:none;text-align:right;width:100%'>" +
		 				"</div>" +
		 			"</div>";


	 	div.innerHTML = input;	

	 	var addedInputs = document.getElementById('infraestructure_amount');

	 	total_value = $('#infraestructure_total_amount').text();

	 	total_value = parseInt(total_value.split("$")[1]);

	 	total_value = total_value + parseInt($money); 

	 	$('#infraestructure_total_amount').html('$' + total_value);
	}

	addedInputs.appendChild(div);

	$name 	= $('#name_amount').val('');
	$money 	= $('#money_amount').val('');
	$material_checkbox = $('#type_amount_material').removeAttr('checked')
	$task_checkbox = $('#type_amount_task').removeAttr('checked')
	$inf_checkbox = $('#type_amount_inf').removeAttr('checked')
	$description = $('#description_amount').val('');
	$imp_amount = $('#imp_amount').removeAttr('checked')
	$adi_amount = $('#adi_amount').removeAttr('checked')

});
