$(document).ready(function() {

	// Se inicia mapa de OpenLayers, coordenadas y zoom inicial
	mymap = MapMaker();


  var getUrl = window.location
  // var url    = getUrl.protocol + "//" + getUrl.host + "/my-maps/preferences";
  // $.get(url, function(response){
  //   mymap.createOSMap(response.longitude, response.latitude, response.zoom, response.map_style);
  // }) 

	// Obtenemos url en la que nos encontramos
	var url    = getUrl.protocol + "//" + getUrl.host + getUrl.pathname + "/data";

	// Hacemos llamado para obtener data
	$.get(url, function(data){


        for(var i = 0; i < data.length; i++) {

          if (data[i].images[0] == null) {
            data[i].images[0] = {
              'image_file': null,
              'image_ext': null
            }
          }

          if (data[i].type == 'point') {
            mymap.addMarker(
            	data[i].points[0].longitude,
            	data[i].points[0].latitude,
            	data[i].category_id,
            	data[i].theme_id,
            	data[i].id,
            	data[i].title,
            	data[i].description,
            	data[i].images[0].image_file,
            	data[i].images[0].image_ext
            );
          } else {
            mymap.addPolygon(
            	data[i].points,
            	data[i].category_id,
            	data[i].theme_id,
            	data[i].id,
            	data[i].title,
            	data[i].description,
            	data[i].images[0].image_file,
            	data[i].images[0].image_ext
            );
          }
        }
	})
})

/* Define tipo de dato a almacenar, luego almacena el tipo de dato en local storage */
function setAddData() {
  $('.modal').attr('id', 'exampleModal');
  swal({
    title: '<p>Agregar dato a mapa público</p>',
    html: '<p>Para agregar un <strong>punto</strong>, clickea sobre el mapa</p>',
  showCancelButton: true,
  cancelButtonText: 'Cancelar'

  }).then(function () {
      localStorage.setItem('type', 'point');
      mymap.addPoint();
  });
}