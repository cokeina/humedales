
var MapMaker = function()
{
    var map;
    var layouts = [];

    function getRootUrl() {
        return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
    }

    return {
        createOSMap: function (lon, lat, zoom,tile="basic")
        {
            window.app = {};
            var app = window.app;
            
            
            
            /**
             * @constructor
             * @extends {ol.interaction.Pointer}
             */
            app.Drag = function() {
            
              ol.interaction.Pointer.call(this, {
                handleDownEvent: app.Drag.prototype.handleDownEvent,
                handleDragEvent: app.Drag.prototype.handleDragEvent,
                handleMoveEvent: app.Drag.prototype.handleMoveEvent,
                handleUpEvent: app.Drag.prototype.handleUpEvent
              });
            
              /**
               * @type {ol.Pixel}
               * @private
               */
              this.coordinate_ = null;
            
              /**
               * @type {string|undefined}
               * @private
               */
              this.cursor_ = 'pointer';
            
              /**
               * @type {ol.Feature}
               * @private
               */
              this.feature_ = null;
            
              /**
               * @type {string|undefined}
               * @private
               */
              this.previousCursor_ = undefined;
            
            };
            ol.inherits(app.Drag, ol.interaction.Pointer);
            
            
            /**
             * @param {ol.MapBrowserEvent} evt Map browser event.
             * @return {boolean} `true` to start the drag sequence.
             */
            app.Drag.prototype.handleDownEvent = function(evt) {
              var map = evt.map;
            
              var feature = map.forEachFeatureAtPixel(evt.pixel,
                  function(feature, layer) {
                    return feature;
                  });
            
              if (feature) {
                this.coordinate_ = evt.coordinate;
                this.feature_ = feature;
              }
            
              return !!feature;
            };
            
            
            /**
             * @param {ol.MapBrowserEvent} evt Map browser event.
             */
            app.Drag.prototype.handleDragEvent = function(evt) {
              var map = evt.map;
            
              var feature = map.forEachFeatureAtPixel(evt.pixel,
                  function(feature, layer) {
                    return feature;
                  });
            
              var deltaX = evt.coordinate[0] - this.coordinate_[0];
              var deltaY = evt.coordinate[1] - this.coordinate_[1];
            
              var geometry = /** @type {ol.geom.SimpleGeometry} */
                  (this.feature_.getGeometry());
              geometry.translate(deltaX, deltaY);
            
              this.coordinate_[0] = evt.coordinate[0];
              this.coordinate_[1] = evt.coordinate[1];
            };
            
            
            /**
             * @param {ol.MapBrowserEvent} evt Event.
             */
            app.Drag.prototype.handleMoveEvent = function(evt) {
              if (this.cursor_) {
                var map = evt.map;
                var feature = map.forEachFeatureAtPixel(evt.pixel,
                    function(feature, layer) {
                      return feature;
                    });
                var element = evt.map.getTargetElement();
                if (feature) {
                  if (element.style.cursor != this.cursor_) {
                    this.previousCursor_ = element.style.cursor;
                    element.style.cursor = this.cursor_;
                  }
                } else if (this.previousCursor_ !== undefined) {
                  element.style.cursor = this.previousCursor_;
                  this.previousCursor_ = undefined;
                }
              }
            };
            
            
            /**
             * @param {ol.MapBrowserEvent} evt Map browser event.
             * @return {boolean} `false` to stop the drag sequence.
             */
            app.Drag.prototype.handleUpEvent = function(evt) {
              

              var feature = map.forEachFeatureAtPixel(evt.pixel,
                function(feature, layer) {
                    return feature;
              });

              
              let coordinates = ol.proj.transform(this.coordinate_, 'EPSG:3857', 'EPSG:4326');

              $("#latitude").val(coordinates[1]);
              $("#longitude").val(coordinates[0]);

              this.coordinate_ = null;
              this.feature_ = null;

              return false;
            };

            /* Define capa para agregar poligonos o lineas a mapa */
            var polygon = new ol.layer.Vector({
              source: new ol.source.Vector({wrapX: false})
            });

            var center = [lon, lat];

            var markerLayer = new ol.layer.Vector({
                source: new ol.source.Vector({ features: [], projection: 'EPSG:4326' })
            });

            layouts.push(new ol.layer.Tile( {
              source: new ol.source.OSM(),
            }));

            layouts.push(new ol.layer.Tile({
              visible:false,
              source: new ol.source.BingMaps({
              key: 'Akhc35qCJyICezRLrQMqQgRZQZ_XOSJpYk2CAgpBkPn1ayrSOOLYqhCaKY0RFMWQ',
              imagerySet: "AerialWithLabels"
            })
            }));

            layouts.push(new ol.layer.Tile({
              visible:false,
              source: new ol.source.BingMaps({
              key: 'Akhc35qCJyICezRLrQMqQgRZQZ_XOSJpYk2CAgpBkPn1ayrSOOLYqhCaKY0RFMWQ',
              imagerySet: "RoadOnDemand"
            })
          }));

            layouts.push(markerLayer);
            layouts.push(polygon);

            if(tile == "basic") layouts[0].setVisible(true);
            if(tile == "AerialWithLabels") layouts[1].setVisible(true);
            if(tile == "RoadOnDemand") layouts[2].setVisible(true);
            
            
            map = new ol.Map({
              interactions: ol.interaction.defaults().extend([new app.Drag()]),
              layers: layouts,
              target: 'map',
              view: new ol.View({
                center:  ol.proj.fromLonLat(center),
                zoom: zoom,
                maxZoom: 19
              })
            });

        },
        addMarker: function(lon, lat, category, theme, id, name, description, images) {
        	var icon; 
          var rootUrl = getRootUrl();

	        var url     = rootUrl + 'images/icons-map/';
	        var icon    = url + category +'/'+ theme +'.png';
          var geom = new ol.geom.Point( ol.proj.transform([parseFloat(lon), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857') );
	        var feature = new ol.Feature({
              geometry: geom,
              content: ''
            });

            feature.setStyle([
                new ol.style.Style({
                  image: new ol.style.Icon(({
                      anchor: [0.5, 0.5],
                      anchorXUnits: 'fraction',
                      anchorYUnits: 'fraction',
                      scale: 0.3,
                      opacity: 1,
                      src: icon
                  }))
                })
            ]);

            if (id != null)
            {
              feature.setId(id);
            }

            feature.set('category', category);
            feature.set('theme', theme);
            feature.set('type', 'point');

            map.getLayers().item(3).getSource().addFeature(feature);
        },
        addPolygon: function(coordinates, category, theme, id, name, description,images) {

          var icon; 
          var rootUrl = getRootUrl();

          var url     = rootUrl + 'images/icons-map/';
          var icon    = url + category +'/'+ theme +'.png';

          var polyCoords = [];

          for (var i = 0; i < coordinates.length; i++) {
            var lat = parseFloat(coordinates[i].latitude);
            var lon = parseFloat(coordinates[i].longitude);
            polyCoords.push(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
          }

          var feature = new ol.Feature({
              geometry: new ol.geom.Polygon([polyCoords]),
              content: ''
          });

          feature.setStyle([
            new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'blue',
                width: 3
              }),
              fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
              }),
              })
            ]);



            if (id != null)
            {
              feature.setId(id);
            }

            feature.set('category', category);
            feature.set('theme', theme);
            feature.set('type', 'polygon');

          map.getLayers().item(4).getSource().addFeature(feature);

        },
        getZoom: function(){
          return map.getView().getZoom();
        },
        getCenter: function()
        {
          return ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
        },
        deleteMarkerById: function(id)
        {
          var id = map.getLayers().item(3).getSource().getFeatureById(id);
          map.getLayers().item(3).getSource().removeFeature(id);
          // delete markerClickCallbacks[id];
        },
        setLayer: function(tl)
        {
          
          
          layouts[0].setVisible(tl === "basic");
          layouts[1].setVisible(tl === "AerialWithLabels");
          layouts[2].setVisible(tl === "RoadOnDemand");
                       
        }
    }
}