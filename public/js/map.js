/*


 FUNCION PARA INICIALIZAR MAPAS
 DATO IMPORTANTE: OPEN LAYERS SIEMPRE RECIBE PRIMERO LA LONGITUD Y LUEGO LA LATITUD, EN LOS MAPAS COMUNES ESTO ES AL REVES


*/

var keyPoint;
var keyPolygon;
var keyLines;
var draw;



var MapMaker = function(){

    var map;
    var markerClickCallbacks;

    var tlayer;
    var layouts = [];
    var returnLayer;



    /* Define capa para agregar marcadores o puntos al mapa */
    var markerLayer = new ol.layer.Vector({
        source: new ol.source.Vector({ features: [], projection: 'EPSG:4326' })
    });

    var source = new ol.source.Vector({wrapX: false});


    /* Define capa para agregar poligonos o lineas a mapa */
    var polygon = new ol.layer.Vector({
      source: source
    });

    /* Define capa de mapa basico */
    var baseLayer = new ol.layer.Tile( {
      source: new ol.source.OSM(),
      visible:false
    });

    var clusterSource = new ol.source.Cluster({
      distance: 10,
      source: markerLayer.getSource()
    });



    var styleCache = {};
    var clusters = new ol.layer.Vector({
      source: clusterSource,
      style: function(feature) {
        var size = feature.get('features').length;
        var style = styleCache[size];
        if (!style && size != 1)  {
          style = new ol.style.Style({
            image: new ol.style.Circle({
              radius: 20,
              stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
              }),
              fill: new ol.style.Fill({
                color: '#3399CC'
              })
            }),
            text: new ol.style.Text({
              text: size.toString(),
              scale:1.5,
              fill: new ol.style.Fill({
                color: '#fff'
              })
            })
          });
          styleCache[size] = style;
        }
        {
          if(size == 1)
          {
            return feature.get('features')[0].styleb
          }
        }
        return style;
      }
    });

    /* Define tipo de capa a desplegar (puede ser básico, satelital, etc.) */
    var setTileLayer = function(tile){
      if (tile == 'basic') {
        return baseLayer;
      }

      var tileLayer = new ol.layer.Tile({
        visible:false,
        source: new ol.source.BingMaps({
        key: 'Akhc35qCJyICezRLrQMqQgRZQZ_XOSJpYk2CAgpBkPn1ayrSOOLYqhCaKY0RFMWQ',
        imagerySet: tile
      })
    });

      return tileLayer;
    }

    var callbackMarkersOnClick = function(pixel)
    {
      map.forEachFeatureAtPixel(pixel, function(feature, layer){
        (markerClickCallbacks[feature.getId()])({id:feature.getId()});
      })
    };

    return {
        /* Crea nuevo mapa con latitud, longitud, zoom y capa */
        createOSMap: function (lon, lat, zoom, tile)
        {
            
            window.app = {};
            var app = window.app;

            returnLayer = tile;
        
            app.SetZoomCenter = function(opt_options) {
              
                      var options = opt_options || {};
              
                      var button = document.createElement('button');
                      button.innerHTML = 'P';
                      button.title = 'Fijar zoom y centro por defecto';
              
                      var this_ = this;
                      var handleRotateNorth = function() {
                        
                        let route       = window.location.pathname;
                        let mapQuery    = '';
                        let zoom        = map.getView().getZoom();;
                        let coordinates = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
                        let url         = window.location.protocol + "/" + window.location.host;
                        let mid         = ''; 
                        
                        if(route.includes("public-map"))
                        {
                            maqQuery = 'public';
                        }
                        if(route.includes("private-map"))
                        {
                            maqQuery = 'private';

                            mid = window.location.pathname.split("/").pop();
                        }
                        
                        

                        var frm = new FormData();
                        frm.append('zoom',zoom);
                        frm.append('longitude',parseFloat(coordinates[0]));
                        frm.append('latitude',parseFloat(coordinates[1]));
                        frm.append('type','zoom');
                        frm.append('map',maqQuery);
                        frm.append('mid',mid);
                        


                        $.ajax({
                          url:'/map/setpreferences',
                          data:frm,
                          cache: false,
                          processData: false,
                          contentType: false,
                          type: 'POST',
                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                          success:function(response){
                              if(response.success)
                              {
                                swal(
                                  'Operación exitosa',
                                  'El zoom / centro fue ajustado de forma exitosa',
                                  'success'
                                )
                              }
                          }
                        });

                        //aqui va el post con las coordenadas
                        //mi duda es si esto tiene que ir en algun nivel xD 

                        

                      };
              
                      button.addEventListener('click', handleRotateNorth, false);
                      button.addEventListener('touchstart', handleRotateNorth, false);
              
                      var element = document.createElement('div');
                      element.className = 'rotate-north ol-unselectable ol-control';
                      element.appendChild(button);
              
                      ol.control.Control.call(this, {
                        element: element,
                        target: options.target
                      });
              
              };


              
              app.SetStyle1 = function(opt_options) {
                
                        var options = opt_options || {};
                
                        var button = document.createElement('button');
                        button.innerHTML = 'B';
                        button.title = 'Vista básica';
                
                        var this_ = this;
                        var handleRotateNorth = function() {
                          
                          let route       = window.location.pathname;
                          let mapQuery    = '';
                          let zoom        = map.getView().getZoom();;
                          let coordinates = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');

                          let url = window.location.protocol + "/" + window.location.host;
                          let mid         = ''; 
  
                          if(route.includes("public-map"))
                          {
                              maqQuery = 'public';
                          }
                          if(route.includes("private-map"))
                          {
                              maqQuery = 'private';
  
                              mid = window.location.pathname.split("/").pop();
                          }
  
                          var frm = new FormData();
                          frm.append('zoom',zoom);
                          frm.append('longitude',parseFloat(coordinates[0]));
                          frm.append('latitude',parseFloat(coordinates[1]));
                          frm.append('type','style');
                          frm.append('style','basic');
                          frm.append('map',maqQuery);
                          frm.append('mid',mid);
  
                          
  
                          $.ajax({
                            url:'/map/setpreferences',
                            data:frm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            success:function(response){
                                if(response.success)
                                {
                                  swal(
                                    'Operación exitosa',
                                    'El estilo fue cambiado de forma exitosa',
                                    'success'
                                  );

                                  mymap.setLayer("basic");
                                }
                            }
                          });
                          //map.setTileLayer("RoadOnDemand");
                          //mymap.test("'AerialWithLabels'");

                          //aqui va el post con las coordenadas
                          //mi duda es si esto tiene que ir en algun nivel xD 
  
                          
  
                        };
                
                        button.addEventListener('click', handleRotateNorth, false);
                        button.addEventListener('touchstart', handleRotateNorth, false);
                
                        var element = document.createElement('div');
                        element.className = 'set-style1 ol-unselectable ol-control';
                        element.appendChild(button);
                
                        ol.control.Control.call(this, {
                          element: element,
                          target: options.target
                        });
                
                };

                app.SetStyle2 = function(opt_options) {
                  
                          var options = opt_options || {};
                  
                          var button = document.createElement('button');
                          button.innerHTML = 'S';
                          button.title = 'Vista satelital';
                  
                          var this_ = this;
                          var handleRotateNorth = function() {
                            
                            let route       = window.location.pathname;
                            let mapQuery    = '';
                            let zoom        = map.getView().getZoom();
                            let coordinates = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
  
                            let url = window.location.protocol + "/" + window.location.host;
                            let mid         = ''; 
    
                            if(route.includes("public-map"))
                            {
                                maqQuery = 'public';
                            }
                            if(route.includes("private-map"))
                            {
                                maqQuery = 'private';
    
                                mid = window.location.pathname.split("/").pop();
                            }
    
                            var frm = new FormData();
                            frm.append('zoom',zoom);
                            frm.append('longitude',parseFloat(coordinates[0]));
                            frm.append('latitude',parseFloat(coordinates[1]));
                            frm.append('type','style');
                            frm.append('style','AerialWithLabels');
                            frm.append('map',maqQuery);
                            frm.append('mid',mid);
    
    
                            $.ajax({
                              url:'/map/setpreferences',
                              data:frm,
                              cache: false,
                              processData: false,
                              contentType: false,
                              type: 'POST',
                              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                              success:function(response){
                                if(response.success)
                                {
                                  swal(
                                    'Operación exitosa',
                                    'El estilo fue cambiado de forma exitosa',
                                    'success'
                                  );

                                  mymap.setLayer("AerialWithLabels");
                                }
                              }
                            });
    
                            //aqui va el post con las coordenadas
                            //mi duda es si esto tiene que ir en algun nivel xD 
    
                            
    
                          };
                  
                          button.addEventListener('click', handleRotateNorth, false);
                          button.addEventListener('touchstart', handleRotateNorth, false);
                  
                          var element = document.createElement('div');
                          element.className = 'set-style2 ol-unselectable ol-control';
                          element.appendChild(button);
                  
                          ol.control.Control.call(this, {
                            element: element,
                            target: options.target
                          });
                  
                  };

                  app.SetStyle3 = function(opt_options) {
                    
                            var options = opt_options || {};
                    
                            var button = document.createElement('button');
                            button.innerHTML = 'R';
                            button.title = 'Vista por relieve';
                    
                            var this_ = this;
                            var handleRotateNorth = function() {
                              
                              let route       = window.location.pathname;
                              let mapQuery    = '';
                              let zoom        = map.getView().getZoom();;
                              let coordinates = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
    
                              let url = window.location.protocol + "/" + window.location.host;
                              let mid         = ''; 
      
                              if(route.includes("public-map"))
                              {
                                  maqQuery = 'public';
                              }
                              if(route.includes("private-map"))
                              {
                                  maqQuery = 'private';
      
                                  mid = window.location.pathname.split("/").pop();
                              }
      
                              var frm = new FormData();
                              frm.append('zoom',zoom);
                              frm.append('longitude',parseFloat(coordinates[0]));
                              frm.append('latitude',parseFloat(coordinates[1]));
                              frm.append('type','style');
                              frm.append('style','RoadOnDemand');
                              frm.append('map',maqQuery);
                              frm.append('mid',mid);
      
      
                              $.ajax({
                                url:'/map/setpreferences',
                                data:frm,
                                cache: false,
                                processData: false,
                                contentType: false,
                                type: 'POST',
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                success:function(response){
                                  if(response.success)
                                  {
                                    swal(
                                      'Operación exitosa',
                                      'El estilo fue cambiado de forma exitosa',
                                      'success'
                                    );
  
                                    mymap.setLayer("RoadOnDemand");
                                  }
                                }
                              });
      
                              //aqui va el post con las coordenadas
                              //mi duda es si esto tiene que ir en algun nivel xD 
      
                              
      
                            };
                    
                            button.addEventListener('click', handleRotateNorth, false);
                            button.addEventListener('touchstart', handleRotateNorth, false);
                    
                            var element = document.createElement('div');
                            element.className = 'set-style3 ol-unselectable ol-control';
                            element.appendChild(button);
                    
                            ol.control.Control.call(this, {
                              element: element,
                              target: options.target
                            });
                    
                    };

                    app.ExportImg = function(opt_options) {
                      
                              var options = opt_options || {};
                      
                              var button = document.createElement('button');
                              button.innerHTML = '<i class="fa fa-xs fa-image fa-fw"></i>';
                              button.title = 'Exportar mapa a png';
                      
                              var this_ = this;

                              function setDPI(canvas, dpi) {
                                  var scaleFactor = dpi / 96;
                                  canvas.width = Math.ceil(canvas.width * scaleFactor);
                                  canvas.height = Math.ceil(canvas.height * scaleFactor);
                                var ctx=canvas.getContext("2d");
                                  ctx.scale(scaleFactor, scaleFactor);
                              }


                              var handleRotateNorth = function() {

                                  map.once('precompose', function(event) {
                                    var canvas = event.context.canvas;
                                    //setDPI(canvas,576);
                                  
                                  });
                                
                                  map.once('postcompose', function(event) {
                                    var canvas = event.context.canvas;

                                    //exportPNGElement.href = canvas.toDataURL('image/png');  
                                    if (navigator.msSaveBlob) {
                                      navigator.msSaveBlob(canvas.msToBlob(), $(".title-map").text()+'.png');
                                    } else {
                                      canvas.toBlob(function(blob) {
                                        saveAs(blob, $(".title-map").text()+'.png');
                                      });
                                    }
                                  });
                                  map.renderSync();
        
                              };
                      
                              button.addEventListener('click', handleRotateNorth, false);
                              button.addEventListener('touchstart', handleRotateNorth, false);
                      
                              var element = document.createElement('div');
                              element.className = 'set-exportimg ol-unselectable ol-control';
                              element.appendChild(button);
                      
                              ol.control.Control.call(this, {
                                element: element,
                                target: options.target
                              });
                      
                      };


                      app.ExportKML = function(opt_options) {
                        
                                var options = opt_options || {};
                        
                                var button = document.createElement('button');
                                button.innerHTML = '<i class="fa fa-xs fa-download fa-fw"></i>';
                                //button.title = 'Exportar mapa a kml';
                                button.className = 'mybutton1';
                        
                                var this_ = this;
                                var handleRotateNorth = function() {

                                     $(".mybutton1").attr("data-toggle","popover");


                                     var content = '<input type="radio" name="tile" onclick=exportFile("kml")> Exportar a KML<br>';
                                     content += '<input type="radio" onclick=exportFile("geojson") name="tile" > Exportar a GeoJSON<br>';
                                     


                                     $(".mybutton1").attr("data-content",content);
                                  
                                    

                                };
                        
                                button.addEventListener('click', handleRotateNorth, false);
                                button.addEventListener('touchstart', handleRotateNorth, false);
                        
                                var element = document.createElement('div');
                                element.className = 'set-exportkml ol-unselectable ol-control';
                                element.appendChild(button);
                        
                                ol.control.Control.call(this, {
                                  element: element,
                                  target: options.target
                                });
                        
                        };


                        app.example = function(opt_options) {
                          
                                  var options = opt_options || {};
                          
                                  var button = document.createElement('button');
                                  button.innerHTML = '<i class="fa fa-xs fa-map fa-fw"></i>';
                                  button.className = 'mybutton';
                            

                                  var this_ = this;
                                  var handleRotateNorth = function() {
                                    
                                     $(".mybutton").attr("data-toggle","popover");

                                     var check1 = '';
                                     var check2 = '';
                                     var check3 = '';

                                     var content = '<input type="radio" name="tile" onclick=changeTile("basic") '+check1+'> Básico<br>';
                                     content += '<input type="radio" onclick=changeTile("AerialWithLabels") '+check2+'  name="tile" > Satélite<br>';
                                     content += '<input type="radio" onclick=changeTile("RoadOnDemand") '+check3+'  name="tile" > Relieve<br>';
                                     


                                     $(".mybutton").attr("data-content",content);

                                  };
                          
                                  button.addEventListener('click', handleRotateNorth, false);
                                  button.addEventListener('touchstart', handleRotateNorth, false);
                          
                                  var element = document.createElement('div');
                                  element.className = 'set-example ol-unselectable ol-control';
                                  element.appendChild(button);
                          
                                  ol.control.Control.call(this, {
                                    element: element,
                                    target: options.target
                                  });
                          
                          };
            
              //ol.inherits(app.SetZoomCenter, ol.control.Control);
              //ol.inherits(app.SetStyle1, ol.control.Control);
              //ol.inherits(app.SetStyle2, ol.control.Control);
              //ol.inherits(app.SetStyle3, ol.control.Control);
              ol.inherits(app.ExportImg, ol.control.Control);
              ol.inherits(app.ExportKML, ol.control.Control);
              ol.inherits(app.example, ol.control.Control);


          
            center = [lon, lat];
            tlayer = setTileLayer(tile);

            layouts.push(baseLayer);
            layouts.push(setTileLayer("AerialWithLabels"));
            layouts.push(setTileLayer("RoadOnDemand"));
            layouts.push(clusters);
            layouts.push(polygon);

            if(tile == "basic") layouts[0].setVisible(true);
            if(tile == "AerialWithLabels") layouts[1].setVisible(true);
            if(tile == "RoadOnDemand") layouts[2].setVisible(true);

            map = new ol.Map({
              target: 'map',
              renderer: 'canvas',
              layers: layouts,
              view: new ol.View({
                  center:  ol.proj.fromLonLat(center),
                  zoom: zoom,
                  maxZoom: 19
              }),
              controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                  collapsible: false
                })
              }).extend([
                //new app.SetZoomCenter(),
                //new app.SetStyle1() ,
                //new app.SetStyle2() ,
                //new app.SetStyle3(),
                new app.ExportImg(),
                new app.ExportKML(),
                new app.example()
              ]),
            });

            $('.mybutton').popover({
              html:true
            });   

            $('.mybutton1').popover({
              html:true
            });

            

            var element = document.getElementById('popup');
            var popup = new ol.Overlay({
              element: element,
              positioning: 'bottom-center',
              stopEvent: false
            });

            map.addOverlay(popup);

            map.on('pointermove', function(evt) {
              var feature = map.forEachFeatureAtPixel(evt.pixel,
                  function(feature, layer) {
                    return feature;
                  });   
              if (feature) {
                  if(feature.get('features') !== undefined)
                  {
                    
                    if(feature.get('features').length == 1)
                    {
                        
                        let ft = feature.get('features')[0];
                        var geometry = ft.getGeometry();
                        if (ft.R['type'] == 'polygon') {
                          var aa = geometry.getExtent();
                          var coord = ol.extent.getCenter(aa);
                        }else {
                          var coord = geometry.getCoordinates();
                        }
                        popup.setPosition(coord);
                        $(element).attr('data-placement', 'top');
                        $(element).attr('data-html', true);
                        $(element).attr('data-content', ft.get('content'));
                        $(element).popover();
                        $(element).popover('show');
                    }
                  } 
                  else
                  {
                        let ft = feature;
                       
                        if(ft.a != undefined)
                        {
                          var geometry = ft.getGeometry();
                          var aa = geometry.getExtent();
                          var coord = ol.extent.getCenter(aa);
                          popup.setPosition(coord);
                          $(element).attr('data-placement', 'top');
                          $(element).attr('data-html', true);
                          $(element).attr('data-content', ft.get('content'));
                          $(element).popover();
                          $(element).popover('show');
                        }

                        
                  } 

                  

                  
                  
              } else {
                $(element).popover('hide');
              }

              //console.log(map.getView().getCenter());
              //console.log(map.getView().getZoom());

            });

            

        },

        setLayer: function(tl)
        {
          
          
          layouts[0].setVisible(tl === "basic");
          layouts[1].setVisible(tl === "AerialWithLabels");
          layouts[2].setVisible(tl === "RoadOnDemand");
                       
        },

        getExtent: function(){

          return map;
        }
        ,

        getTile: function()
        {
           return returnLayer;
        },

        /* Agrega un dato al mapa */
        addMarker: function(lon, lat, category, theme, id, name, description, images) {
        	var icon; 
          var rootUrl = getRootUrl();

	        var url     = rootUrl + 'images/icons-map/';
	        var icon    = url + category +'/'+ theme +'.png';
          var geom = new ol.geom.Point( ol.proj.transform([parseFloat(lon), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857') );
	        var feature = new ol.Feature({
              geometry: geom,
              content: setContent(name, description, images, id)
            });

            feature.setStyle([
                new ol.style.Style({
                  image: new ol.style.Icon(({
                      anchor: [0.5, 0.5],
                      anchorXUnits: 'fraction',
                      anchorYUnits: 'fraction',
                      scale: 0.3,
                      opacity: 1,
                      src: icon
                  }))
                })
            ]);

            feature.styleb = new ol.style.Style({
              image: new ol.style.Icon(({
                  anchor: [0.5, 0.5],
                  anchorXUnits: 'fraction',
                  anchorYUnits: 'fraction',
                  scale: 0.3,
                  opacity: 1,
                  src: icon
              }))
            });

            if (id != null)
            {
              feature.setId(id);
            }

            feature.set('category', category);
            feature.set('theme', theme);
            feature.set('type', 'point');

            map.getLayers().item(3).getSource().getSource().addFeature(feature);
        },
        /* Agrega un conjunto de lineas al mapa */
        addLine:function(coordinates, category, theme, id, name, description,images)
        {
          var icon; 
          var rootUrl = getRootUrl();

          var url     = rootUrl + 'images/icons-map/';
          var icon    = url + category +'/'+ theme +'.png';

          var polyCoords = [];

          for (var i = 0; i < coordinates.length; i++) {
            var lat = parseFloat(coordinates[i].latitude);
            var lon = parseFloat(coordinates[i].longitude);
            polyCoords.push(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
          }


          var feature = new ol.Feature({
              geometry: new ol.geom.MultiLineString([polyCoords]),
              content: setContent(name, description, images, id)
          });

          feature.setStyle([
            new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'blue',
                width: 3
              }),
              fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
              }),
              })
            ]);



            if (id != null)
            {
              feature.setId(id);
            }

            feature.set('category', category);
            feature.set('theme', theme);
            feature.set('type', 'polygon');

          map.getLayers().item(4).getSource().addFeature(feature);

        },

        /* Agrega un polígono al mapa */
        addPolygon: function(coordinates, category, theme, id, name, description,images) {

          var icon; 
          var rootUrl = getRootUrl();

          var url     = rootUrl + 'images/icons-map/';
          var icon    = url + category +'/'+ theme +'.png';

          var polyCoords = [];

          for (var i = 0; i < coordinates.length; i++) {
            var lat = parseFloat(coordinates[i].latitude);
            var lon = parseFloat(coordinates[i].longitude);
            polyCoords.push(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
          }

          var feature = new ol.Feature({
              geometry: new ol.geom.Polygon([polyCoords]),
              content: setContent(name, description, images, id)
          });

          feature.setStyle([
            new ol.style.Style({
              stroke: new ol.style.Stroke({
                color: 'blue',
                width: 3
              }),
              fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
              }),
              })
            ]);



            if (id != null)
            {
              feature.setId(id);
            }

            feature.set('category', category);
            feature.set('theme', theme);
            feature.set('type', 'polygon');

          map.getLayers().item(4).getSource().addFeature(feature);

        },

      /* Elimina un dato sobre el mapa en específico */
      deleteMarkerById: function(id)
      {
        var id = map.getLayers().item(3).getSource().getSource().getFeatureById(id);
        map.getLayers().item(3).getSource().getSource().removeFeature(id);
        // delete markerClickCallbacks[id];
      },

      deleteMarkerByIdP: function(id)
      {
        var id = map.getLayers().item(4).getSource().getFeatureById(id);
        map.getLayers().item(4).getSource().removeFeature(id);
        // delete markerClickCallbacks[id];
      },

      /* Obtiene todas las capas (íconos, capa de mapa) sobre mapa */
      getAllLayers: function()
      {
        return map.getLayers().item(3).getSource().getSource().getFeatures();
      },

      getAllLayersP: function()
      {
        return map.getLayers().item(4).getSource().getFeatures();
      },

      addLines:function()
      {
        
        if(draw != null)
        {
          map.removeInteraction(draw);
        }

        var fline = function(e){

          draw = new ol.interaction.Draw({
            source: source,
            type: 'LineString'
          });
        
          draw.on('drawend', function(evt) {
        
            var feature = evt.feature;
            var coords = feature.getGeometry().getCoordinates();
            
            coords.forEach(function(coord){
              lonlat = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
              coord[0] = lonlat[0];
              coord[1] = lonlat[1];
            });
            localStorage.setItem('coordinates', coords);
        
            $('#title').val("");
            $('#description').val("");
            $('input[name="categories[]"]').removeAttr('checked'); 
            $('input[name="themes[]"]').removeAttr('checked'); 
            $('input[name="file_photos[]"]').val(""); 
        
            $(".feedback-info-photo").empty();
            $('.feedback-info-theme').empty();
            $('.feedback-info-category').empty();
            $('.feedback-info-title').empty();
        
            $('#exampleModal').modal();
        

            map.getInteractions().forEach(function (interaction) {
              if (interaction instanceof ol.interaction.Draw) {
                 map.removeInteraction(interaction);
              }
            });

            setTimeout(function () {
              map.un('dblclick',fline);
           });
        
            
        
          }, this);
        
          map.addInteraction(draw); 
        
         };
        
        keyLines = map.on('dblclick',fline);
      },
      
      /* Agrega dibujo a mapa, para cuando el usuario desea agregar una línea o polígono */
      addDraw: function()
      {
        if(draw != null)
        {
          map.removeInteraction(draw);
        }  
        
        var fpolygon = function(e){
          geometryFunction = ol.interaction.Draw.createBox();

          draw = new ol.interaction.Draw({
            source: source,
            type: 'Polygon'
          });

          draw.on('drawend', function(evt) {

              var feature = evt.feature;
              var coords = feature.getGeometry().getCoordinates();
              var coords = coords[0];
              coords.forEach(function(coord){
                lonlat = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                coord[0] = lonlat[0];
                coord[1] = lonlat[1];
              });
              localStorage.setItem('coordinates', coords);

              $('#title').val("");
              $('#description').val("");
              $('input[name="categories[]"]').removeAttr('checked'); 
              $('input[name="themes[]"]').removeAttr('checked'); 
              $('input[name="file_photos[]"]').val(""); 
  
              $(".feedback-info-photo").empty();
              $('.feedback-info-theme').empty();
              $('.feedback-info-category').empty();
              $('.feedback-info-title').empty();

              $('#exampleModal').modal();
              

              map.getInteractions().forEach(function (interaction) {
                if (interaction instanceof ol.interaction.Draw) {
                   map.removeInteraction(interaction);
                }
              });

              setTimeout(function () {
                map.un('dblclick',fpolygon);
             });

            }, this);

          map.addInteraction(draw);         
        }

        keyPolygon =  map.on('dblclick',fpolygon);  
      },

      /* Al clickear sobre algún punto sobre mapa, levanta modal para agregar data allí, además de almacenar las
         coordenadas en local storage para luego utilizarlas
      */
      addPoint: function()
      {
        
        if(draw != null)
        {
          map.removeInteraction(draw);
        }
        
        keyPoint = map.on('singleclick', function(evt)
        {
            var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
            localStorage.setItem('long', lonlat[0]);
            localStorage.setItem('lat', lonlat[1]);   
            
            
            
            $('#title').val("");
            $('#description').val("");
            $('input[name="categories[]"]').removeAttr('checked'); 
            $('input[name="themes[]"]').removeAttr('checked'); 
            $('input[name="file_photos[]"]').val(""); 

            $(".feedback-info-photo").empty();
            $('.feedback-info-theme').empty();
            $('.feedback-info-category').empty();
            $('.feedback-info-title').empty();
            
            $('#exampleModal').modal();
        });
      },


      /* Define donde se debe centrar mapa */
      setCenter: function(lonlat, zoom)
      {
        map.getView().setCenter(ol.proj.transform([lonlat[1], lonlat[0]], 'EPSG:4326', 'EPSG:3857'));
        map.getView().setZoom(zoom);
      },

    }
};


/* Funcion creada para agregar o quitar marcadores del mapa */
function changeMarker(id, type, value) {


	if ( $('#' + id).is(':checked') ) {
    var getUrl = window.location;
    var path = getUrl.pathname;
    var url = '';
    if (path.includes('private-map')) {
      console.log("inside");
      url = baseUrl + '/'+getUrl.pathname + '/'+ type + "/" + value;
      $.get(url, function (data) {
            for (var i = 0; i < data.length; i++) {

              if (data[i].type == 'point') {
                mymap.addMarker(data[i].points[0].longitude, data[i].points[0].latitude, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images);
              } else if(data[i].type == 'polygon')  {

                mymap.addPolygon(data[i].points, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images);
              }
              else{
                mymap.addLine(data[i].points, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images);
              }
            }
      });
    } else {
      //var  = getUrl .protocol + "/" + getUrl.pathname;
      var url = baseUrl + '/'+getUrl.pathname + '/'+ type + "/" + value;
      $.get(url, function (data) {
        for (var i = 0; i < data.length; i++) {

          if (data[i].type == 'point') {
            mymap.addMarker(data[i].points[0].longitude, data[i].points[0].latitude, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description, data[i].images);
          } else if(data[i].type == 'polygon') {

            mymap.addPolygon(data[i].points, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description,data[i].images)
          }
          else
          {
            mymap.addLine(data[i].points, data[i].category_id, data[i].theme_id, data[i].id, data[i].title, data[i].description,data[i].images)
          }
        }
      });
    }
	} else {
      var layers = mymap.getAllLayers();
      var count  = mymap.getAllLayers().length;
      for (var i = 0; i < count; i++) {
        if (layers[i].R[type] == value) {
          var id = layers[i].getId();
            mymap.deleteMarkerById(id);
        }
      }
      var layers = mymap.getAllLayersP();
      var count  = mymap.getAllLayersP().length;
      for (var i = 0; i < count; i++) {
        if (layers[i].R[type] == value) {
          var id = layers[i].getId();
            mymap.deleteMarkerByIdP(id);
        }
      }
	}

}

function getRootUrl() {
  return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
}

/* Define como debe mostrarse el popup de cada dato sobre el mapa */
function setContent(title, description, images, id) {
  var url = getRootUrl();
  url = url + "content/" + id;

  cimages = '';
  if(images.length > 0)
  {
    images.map((image) => {
      
             let imageUrl = getRootUrl() + 'storage/points/'+image.id+'.'+image.extension;
      
             cimages += "<img style='max-height:100%;max-width:100%;' src='"+imageUrl+"' >"+'<br>';
      
        });
  }
  

  //"<img style='max-height:100%;max-width:100%;' src='data:"+image_ext+";charset=utf-8;base64, "+image_file+"'>"+
  if(description == null) description = " ";

  var content = 
                "<div style='overflow-y:scroll;height: 15vh;'>"+
                "<h4>"+title+"</h4>" +
                "<p>"+description.substr(0, 50)+"</p>"+
                cimages+'<br>'+
                "<a href='"+url+"'>Ver más</a>" +
                "</div>";
  return content;
}

function changeTile(tile)
{
  let route       = window.location.pathname;
  let mapQuery    = '';
  let map         = mymap.getExtent();
  let zoom        = map.getView().getZoom();
  let coordinates = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');

  let url = window.location.protocol + "/" + window.location.host;
  let mid         = ''; 

  if(route.includes("public-map"))
  {
      maqQuery = 'public';
  }
  if(route.includes("private-map"))
  {
      maqQuery = 'private';

      mid = window.location.pathname.split("/").pop();
  }

  var frm = new FormData();
  frm.append('zoom',zoom);
  frm.append('longitude',parseFloat(coordinates[0]));
  frm.append('latitude',parseFloat(coordinates[1]));
  frm.append('type','style');
  frm.append('style',tile);
  frm.append('map',maqQuery);
  frm.append('mid',mid);


  $.ajax({
    url:'/map/setpreferences',
    data:frm,
    cache: false,
    processData: false,
    contentType: false,
    type: 'POST',
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    success:function(response){
      if(response.success)
      {
        swal(
          'Operación exitosa',
          'El estilo fue cambiado de forma exitosa',
          'success'
        );

        mymap.setLayer(tile);
        returnLayer = tile;
      }
    }
  });
}

function exportFile(type)
{
    let map = mymap.getExtent();

    var mapExtent = map.getView().calculateExtent(map.getSize());

    let features = mymap.getAllLayers();
    let markers  = [];

    for(x in features)
    {
      if(ol.extent.containsCoordinate(mapExtent, features[x].getGeometry().getCoordinates()))
      { 
        markers.push(features[x].getId());
      }
    }

    features = mymap.getAllLayersP();

    for(x in features)
    {
                                      
      let points = features[x].getGeometry().getCoordinates();
                                      
      points = points[0];

      for (y in points)
      {
        if(ol.extent.containsXY(mapExtent,points[y][0],points[y][1]))
        { 
          markers.push(features[x].getId());
        }   
      }                         

    }

    if (markers.length > 0)
    {
      let data     = new Object();
      data.markers = markers;
      data.type    = 'public';
      data.file    = type;
      
      HMApi.download(baseUrl+'/map/export?'+$.param(data));
    }
    else
    {
      console.log("aqui deberia ir un sweet alert");
    }


}






