/* Agrega más recompendas */
function addMoreRewards(){

	var textAreaCount = (document.getElementById('added-inputs').getElementsByTagName('textarea').length);
	console.log(textAreaCount);
	if (textAreaCount != 4) {

		var div = document.createElement('div');
		div.id  = 'created_reward_'+textAreaCount;
		div.style = 'border-top: 1px solid grey;padding-top: 1em';

		var input = 
						"<div class='row'>" +
							"<div class='col-6'>" +
								"<label for='name'>Nombre de recompensa(*):</label>" +
							"</div>" +
							"<div class='col-6 align-items-end'>" +
								"<span class='input-group-btn' style='float:right'><button class='btn' style='background: red!important;padding: 2px 6px;' type='button' onclick='removeAddedReward("+textAreaCount+")''> x </button>" +
							"</div>" +
						"</div>" +
						"<div class='input-group'>" +
							"<input type='text' class='input-file' name='reward_name[]'>" +
						"</div>" +

						"<label for='name'>Descripción(*):</label>" +
						"<div class='input-group'>" +
							"<textarea name='reward_description[]''></textarea>" +
						"</div>" +

						"<div class='input-group'>" +
							"<label for='name'>Monto(*):</label>" +
							"<input type='text' class='input-file' name='reward_amount[]'' placeholder='$' style='width: 20%;'>" +
						"</div>" +

						"<div class='input-group'>" +
							"<input type='file' id='reward"+textAreaCount+"' class='input-file' name='file_rewards[]'>" +
						    "<input type='hidden' id='file_name_reward"+textAreaCount+"' name='filenames_rewards[]'>" +
						    "<input type='hidden' id='file_type_reward"+textAreaCount+"' name='filetypes_rewards[]'>" +
						    "<input type='hidden' id='file_btoa_reward"+textAreaCount+"' name='filebs_rewards[]'>" +
						"</div>";

		div.innerHTML = input;

		var addedInputs = document.getElementById('added-inputs');
		addedInputs.appendChild(div);

	}
}

/* Remueve recompendas */
function removeAddedReward(input) {
	var div = document.getElementById('created_reward_'+input);
	document.getElementById('added-inputs').removeChild(div);
}