$(function() {
	 $.datepicker.regional['es'] = {
	 closeText: 'Cerrar',
	 prevText: '< Ant',
	 nextText: 'Sig >',
	 currentText: 'Hoy',
	 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	 weekHeader: 'Sm',
	 dateFormat: 'yy-mm-dd',
	 firstDay: 1,
	 isRTL: false,
	 showMonthAfterYear: false,
	 yearSuffix: ''
	 };

	 $.datepicker.setDefaults($.datepicker.regional['es']);


	$('#datepicker_begin').datepicker({});
	$('#datepicker_end').datepicker({});
})


$(document).on('change', '.input-file', function(){
  var $value = $(this).attr('id');
  $('#file_name_' + $value).val($(this).context.files[0].name);
  $('#file_type_' + $value).val($(this).context.files[0].type);

  var reader = new FileReader();
  reader.readAsDataURL($(this).context.files[0]);
  reader.onload = function(evt){
    baseString = evt.target.result;
    string_btoa = baseString.substr(baseString.indexOf(",") + 1);
    $('#file_btoa_' + $value).val(string_btoa); 
  }
});

/** Agrega recompensas **/
function addReward() {

	var inputCount = (document.getElementById('reward').getElementsByTagName('input').length) / 4;
	if (inputCount != 5) {
		var div = document.createElement('div');
		div.id = 'added-'+inputCount;

		var input = "<div class='input-group' style='margin-top:5px'>" +
						"<input type='file' class='input-file' id='reward"+inputCount+"' name='file_rewards[]'>"    +
						"<input type='hidden' id='file_name_reward"+inputCount+"' name='filenames_rewards[]'>" +
						"<input type='hidden' id='file_type_reward"+inputCount+"' name='filetypes_rewards[]'>" +
						"<input type='hidden' id='file_btoa_reward"+inputCount+"' name='filebs_rewards[]'>"    +
						"<span class='input-group-btn'>"						  +
						"<button class='btn' type='button' onclick='removeReward("+inputCount+")' style='background: red!important;border-radius:5px'> - </button>" +
						"</span>"
					"</div>";

		div.innerHTML = input;


		var addedInputs = document.getElementById('added-inputs');
		addedInputs.appendChild(div);
	}
}

/* remueve recompensas */
function removeReward(input) {
	var div = document.getElementById('added-'+input);
	document.getElementById('added-inputs').removeChild(div);
}

/* Remueve fotos en inputs */
function removeFile(id) {
	$('#'+id).val('');
	$('#trash_' + id).css({'visibility': 'hidden'});
}

/* Agrega un bien en la creción de iniaitivas */
function addGood() {

	var inputCount = (document.getElementById('goods').getElementsByTagName('input').length);
	console.log(inputCount);
	if (inputCount!= 3) {
		var div = document.createElement('div');
		div.id = 'good-' + inputCount;

		var input = 
					"<div class='input-group' style='margin-top:15px;'>" +
					"<input  type='text' class='form-control' id='good' name='good[]'>" +
					"<span class='input-group-btn'><button class='btn' style='background: red!important;' type='button' onclick='removeGood("+inputCount+")''> - </button>" +
					"</span></div>";
		div.innerHTML = input;

		var addedInputs = document.getElementById('added-goods');
		addedInputs.appendChild(div);
	}
}

/* Remuve un bien en la creción de iniaitivas */
function removeGood(input) {
	var div = document.getElementById('good-'+input);
	document.getElementById('added-goods').removeChild(div);
}

/* Agrega un servicio en la creción de iniaitivas */
function addServ() {

	var inputCount = (document.getElementById('servs').getElementsByTagName('input').length);
	console.log(inputCount);
	if (inputCount!= 3) {
		var div = document.createElement('div');
		div.id = 'servs-' + inputCount;

		var input = 
					"<div class='input-group' style='margin-top:15px;'>" +
					"<input  type='text' class='form-control' id='servs' name='servs[]'>" +
					"<span class='input-group-btn'><button class='btn' style='background: red!important;' type='button' onclick='removeServ("+inputCount+")'> - </button>" +
					"</span></div>";
		div.innerHTML = input;

		var addedInputs = document.getElementById('added-servs');
		addedInputs.appendChild(div);
	}
}

/* Remuve un servicio en la creción de iniaitivas */
function removeServ(input) {
	var div = document.getElementById('servs-'+input);
	document.getElementById('added-servs').removeChild(div);
}

/* Agrega una foto en los formularios de ingreso de datos o iniciativas */
function addPhoto(){
	var inputCount = ((document.getElementById('added-photos').getElementsByTagName('input').length) / 4) + 1;

	if (inputCount!= 5) {
		var div = document.createElement('div');
		div.id = 'photo-' + inputCount;

		var input = "<div class='input-group' style='margin-top:10px'>" +
						"<input type='file' class='input-file' id='photos"+inputCount+"' name='file_photos[]'>" +
	                  	"<input type='hidden' id='file_name_photos"+inputCount+"' name='filenames_photos[]'>" +
	                  	"<input type='hidden' id='file_type_photos"+inputCount+"' name='filetypes_photos[]'>" +
	                  	"<input type='hidden' id='file_btoa_photos"+inputCount+"' name='filebs_photos[]'>" +				
     			 	"</div>";
		div.innerHTML = input;

		var addedInputs = document.getElementById('added-photos');
		addedInputs.appendChild(div);
	}
}

/* Elimina una foto en los formularios de ingreso de datos o iniciativas */
function removePhoto() {
	$('#added-photos').children().last().remove();
}